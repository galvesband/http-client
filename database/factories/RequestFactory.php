<?php

namespace Beat\HttpClient\Database\Factories;

use Beat\HttpClient\Client;
use Beat\HttpClient\Models\Request;
use Beat\HttpClient\Tests\FakeUser;
use Beat\HttpClient\Tests\FakeUserFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

class RequestFactory extends Factory
{
    protected $model = Request::class;

    public function definition(): array
    {
        $result = $this->faker->randomElement([0, 200, 201, 401, 403, 404, 409, 422, 500, 502, 503]);
        $duration = $this->faker->randomFloat(3, 0.001, 30.0);
        $response_headers = [];
        switch (true) {
            case $result === 0:
                $response = '';
                $duration = 0;
                break;

            case $result === 200:
            case $result === 201:
                $response = json_encode(['success' => true]);
                $response_headers = ['Content-Type' => 'application/json'];
                break;

            case $result === 401:
            case $result === 403:
            case $result === 404:
            case $result === 409:
            case $result === 422:
                $response = json_encode(['go-f-uself']);
                $response_headers = ['Content-Type' => 'application/json'];
                break;

            case $result === 500:
            case $result === 502:
            case $result === 503:
                $response = 'BOOM!';
                break;

            default:
                $response = "???";
        }

        return [
            'authenticatable_id'   => null,
            'authenticatable_type' => null,
            'timestamp'            => now()->subSeconds($this->faker->numberBetween(1, 3600)),
            'method'               => $this->faker->randomElement(Client::HTTP_METHODS),
            'scheme'               => $this->faker->randomElement(['http', 'https']),
            'host'                 => $this->faker->domainWord . $this->faker->randomElement(['.net', '.com', '.info', '.es', '.bet']),
            'port'                 => $this->faker->randomElement([80, 8080, 443, 8000]),
            'path'                 => $this->faker->randomElement(['/', '/login', '/info?a=b']),
            'parameters'           => $this->faker->boolean
                ? json_encode(['c' => 'd'])
                : '',
            'headers'              => [],
            'response_code'        => $result,
            'response'             => $response,
            'response_headers'     => $response_headers,
            'duration'             => $duration,
        ];
    }

    /**
     * @param Model|FakeUserFactory $user
     * @return void
     */
    public function fromUser($user): RequestFactory
    {
        return $this->afterMaking(function (Request $request) use ($user) {

            switch (true) {
                case $user instanceof FakeUserFactory:
                    $authenticatable = $user->create();
                    break;

                case $user instanceof Model:
                    $authenticatable = $user;
                    break;

                default:
                    throw new \LogicException("Tipo de dato inesperado: " . (
                        is_object($user) ? get_class($user) : gettype($user)
                    ));
            }

            $request->fill([
                'authenticatable_id'   => $authenticatable->id,
                'authenticatable_type' => get_class($authenticatable),
            ]);
        });
    }

    public function toUrl(string $scheme, string $host, int $port, string $path): RequestFactory
    {
        return $this->state(func_get_args());
    }

    public function withResponse(int $status, string $response, ?float $duration = null): RequestFactory
    {
        $state = [
            'response_code' => $status,
            'response'      => $response,
        ];
        if (!is_null($duration)) {
            $state['duration'] = $duration;
        }

        return $this->state($state);
    }
}