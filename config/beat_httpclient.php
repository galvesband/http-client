<?php

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\Sealers\BeatExternalTokenSealer;
use Beat\HttpClient\Sinks\DatabaseSink;
use Beat\HttpClient\Sinks\LogSink;

return [
    /*
     |--------------------------------------------------------------------------
     | Sink configurations
     |--------------------------------------------------------------------------
     |
     | Los Sinks procesan peticiones y respuestas, normalmente con el objetivo de
     | guardar un registro de las peticiones realizadas. Hay que incluirlos en
     | la configuración de los perfiles para que sean utilizados en ellos.
     |
     | Cada Sink incluye secciones para configurar "extractores de contenido".
     | Estos son clases que serán utilizados por el Sink que los contiene
     | para extraer el contenido o un resumen del mismo para registro.
     |
     */
    'sinks' => [
        // Definición de sink con nombre 'database'.
        'database' => [
            // Clase que se utiliza en este Sink.
            'class' => DatabaseSink::class,

            // Parámetros de construcción para la clase. Cada clase tendrá los suyos.
            // Las claves deben ser los nombres de los parámetros del constructor.
            'params' => [
                'queue' => 'default',
                'register_successful_requests' => true,
                'register_failed_requests' => true,
            ],

            // Definición del extractor de contenido de la petición.
            // Mismo sistema: clase y parámetros de construcción.
            'request_content_extractor' => [
                'class' => NoContentExtractor::class,
                'params' => [],
            ],

            // Definición del extractor de contenido de la respuesta.
            'response_content_extractor' => [
                'class' => NoContentExtractor::class,
                'params' => [],
            ],
        ],

        'log' => [
            'class' => LogSink::class,
            'params' => [
                'log_channel' => env('LOG_CHANNEL', 'stack'),
                'register_successful_requests' => true,
                'register_failed_requests' => true,
            ],
            'request_content_extractor' => [
                'class' => NoContentExtractor::class,
                'params' => [],
            ],
            'response_content_extractor' => [
                'class' => NoContentExtractor::class,
                'params' => [],
            ],
        ],
    ],

    /*
     |--------------------------------------------------------------------------
     | SealProvider configurations
     |--------------------------------------------------------------------------
     |
     | Los proveedores de sellado agregan a la petición _cosas_ (cabeceras, por
     | ejemplo) para identificarse ante sistemas externos. Son útiles si los
     | perfiles son definidos con sistema externos concretos por objetivo.
     |
     */
    'seal_providers' => [
        'vx_seal' => [
            // Clase que implementa este proveedor de sellos.
            'class' => BeatExternalTokenSealer::class,
            // Parámetros del constructor.
            'params' => ['passkey' => env('VX_CLAVE')],
        ]
    ],

    /*
     |--------------------------------------------------------------------------
     | Profile defaults
     |--------------------------------------------------------------------------
     |
     | Estos son los parámetros por defecto que se aplican a los distintos
     | perfiles de configuración. Los perfiles definidos más abajo solo
     | tienen que especificar los parámetros que cambien sobre estos.
     |
     */
    /**
     * Esta es la configuración base que se aplica a todos los perfiles.
     * Cada perfil puede cambiar uno o varios de estos valores para su caso de uso específico.
     *
     * Para ver qué opciones hay disponibles y qué hace cada una, ver ProfileConfiguration.
     * @see \Beat\HttpClient\DataTransferObjects\ProfileConfiguration
     */
    'profile_defaults'    => [
        'base_uri'        => '',
        'connect_timeout' => 10,
        'timeout'         => 20,
        'user_agent'      => config('app.name', 'Laravel Project') . ' (Beat/HttpClient 1.0, GuzzleHttp 7)',
        'seal'            => null,

        'sinks'           => [],
    ],

    /*
     |--------------------------------------------------------------------------
     | Profile configurations
     |--------------------------------------------------------------------------
     |
     | Aquí se configuran los perfiles que pueden usarse en la aplicación.
     | Solo es necesario especificar los campos que cambien comparados
     | con la configuración por defecto incluida un poco más arriba.
     |
     */
    'profiles' => [

        'default' => [],

        // Ejemplo de configuración para una API estilo BEAT, con autenticación
        // usando tokens BEAT externos. Todos los parámetros no especificados
        // tendrán el valor que tenían en `profile_defaults`.
//        'vx' => [
//            'base_uri' => config('app.vx_base_url'),
//            'seal'     => 'vx_seal',
//            'sinks'    => ['database', 'log'],
//        ],
    ],

    /*
     |--------------------------------------------------------------------------
     | Intercept Guzzle
     |--------------------------------------------------------------------------
     |
     | Este valor determina si las peticiones realizadas a través del cliente
     | Guzzle (sin usar HttpClient) deben ser procesadas por alguno de los
     | perfiles de HttpClient definido más arriba.
     */
    'intercept_guzzle_requests' => false,

    /*
     |--------------------------------------------------------------------------
     | Intercepted Guzzle requests profile
     |--------------------------------------------------------------------------
     |
     | En caso de que la opción anterior sea true, esta opción indica qué perfil
     | se aplicará a las peticiones realizadas a través de Guzzle sin utilizar
     | HttpClient. La petición será procesada por los sinks del perfil y se
     | emitirán los eventos apropiados.
     */
    'guzzle_requests_profile' => 'default',

    // FIXME Quizás permitir un array donde se establezca una lista de
    //  clases y el perfil que requieren. Creo que al inyector de dependencias se le puede
    //  decir algo así como "Cuando SomeService pide un SomeObject, suministrar resultado de function () { return whateveah(); }
    //  Por ejemplo:
    'profile_assignment' => [
//        VxService::class => 'vx_profile',
    ],
];
