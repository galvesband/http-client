# Configuración #

Si has seguido las instrucciones de instalación, el proyecto debe funcionar con
normalidad de la misma forma que funcionaba antes de añadir el paquete. 


## El archivo de configuración `beat_httpclient.php` #

Es importante haber dado una pasada sobre la documentación que describe el paquete a grandes rasgos,
el _[overview / vistazo](./overview.md)_

El archivo se divide en las siguientes secciones:

 - `sinks`: Define las configuraciones para _Sinks_ que después podrán ser utilizadas en los _Profiles_.
   Se define un nombre como clave y el valor debe ser un array con la configuración del mismo.

 - `seal_providers`: Define las configuraciones para _SealProviders_ que usarán los _Profiles_.
   De forma análoga a _Sinks_, cada configuración debe estar dentro de una clave que representa
   el nombre del _SealProvider_. 

 - `profile_defaults`: Opciones por defecto que se aplicarán a todos los _Profiles_ salvo que la 
   configuración del propio _Profile_ indique opciones particulares en ese sentido.

 - `profiles`: Como _Sinks_ y _SealProviders_, esta parte de la configuración debe ser un mapa entre
   nombres de perfiles y la configuración del mismo.

 - `profile_assignment`: TODO Pendiente de desarrollo. La idea es pre-asignar perfiles a clases
   de forma que no sea necesario definir en un `AppServiceProvider` un _binding_ específico para 
   cada perfil que le dé un perfil determinado.


## Configuración por defecto ##

A continuación se reproduce una versión simplificada del archivo de configuración.

```php
TODO
```


## Sinks ##

TODO


## Seal Providers ##

TODO


## Profile Defaults ##

TODO


## Profiles ##

TODO


## Profile Assignment ##

TODO
