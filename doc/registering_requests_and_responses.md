# Registrando peticiones y respuestas #

Los clientes HTTP aplican a las peticiones hechas a través de ellos los Sinks que tengan configurados en 
su perfil. También es posible añadir o quitar Sinks de una petición concreta de un cliente. Un Sink
es, en resumen, cualquier clase que es capaz de procesar los datos de una petición y su resultado, normalmente
con el objetivo de crear un registro de las peticiones realizadas y su resultado.

Beat/HttpClient incluye dos implementaciones iniciales de Sinks para los dos casos de uso más típicos
(registrar peticiones en base de datos y en logs). Es posible crear nuevos tipos de Sinks y añadirlos a la
configuración. Otras posibles implementaciones podrían registrar métricas de uso (a través de Prometheus, 
por ejemplo) o variaciones de implementaciones existentes (un log de peticiones, pero con un Sink especializado
capaz de escribir en el log información más concreta sobre peticiones específicas, por ejemplo).

TODO Cómo funciona (middleware de Guzzle, sinks de configuración, ...)


## Sinks ##

Un _Sink_ es una implementación capaz de recibir una petición y el resultado de la misma, y procesarla.
Se diseñaron con idea de que registraran esas peticiones externas de alguna forma (en una base de datos, por ejemplo,
o en un log). Se apoyan en _extractores de contenido_ (implementaciones de `MessageContentExtractor`) para 
extraer el cuerpo de la petición y la respuesta.

Los _Sinks_ tienen su propio apartado de configuración, en `sinks` dentro del archivo de configuración
`beat_httpclient.php`. Dentro de `sinks` se pueden especificar en forma de array clave-valor (siendo la
clave el "nombre" del sink y el valor el array de configuración del mismo).

La configuración consta de 4 parámetros obligatorios:

 - `class`: Nombre de la clase que va a servir como implementación del _sink_.
 - `params`: Parámetros del constructor que no pueden ser resueltos por el inyector de dependencias, 
   exceptuando los extractores de contenido.
 - `request_content_extractor`: Configuración del extractor de contenido que se usará para extraer 
   el cuerpo de la petición.
 - `response_content_extractor`: Configuración, análogo al extractor de contenido de peticiones, para el 
   extractor que se aplica a la respuesta.


## Extractores de contenido ##

Los extractores de contenido encapsulan la lógica que se utiliza a la hora de extraer el cuerpo de un mensaje
(petición o respuesta). Pueden ser tan simples como la simple conversión de la respuesta en una cadena sin 
importar el contenido, o más elaborados (resumiendo o limitando la cantidad de información guardada del
mensaje, o extrayendo el contenido solo si el mensaje es de algún tipo concreto).

Su configuración sigue una metodología similar a la de los propios _Sinks_. Requieren de los siguientes parámetros:

 - `class`: Nombre de la clase que va a servir como implementación del extractor de contenido.
 - `params`: Parámetros del constructor que no pueden ser resueltos por el inyector de dependencias.


## Implementaciones ##

Existen varias opciones / implementaciones que pueden ser utilizadas tanto para _Sinks_ como para _extractores
de contenido_. Pueden combinarse libremente.


### Sinks ###

#### Registros en base de datos: `DatabaseSink` ####

`DatabaseSink` es una implementación de _Sink_ que registra peticiones y respuestas en una tabla de la base de 
datos. 

Para que pueda funcionar es necesario que la tabla exista en base de datos. La misma está definida en una migración
que puede publicarse desde el paquete como se describe en el 
[documento de instalación](./installation.md#añadir-migración-para-registro-de-peticiones-en-bd-).

`DatabaseSink` extrae información de la petición y la respuesta y despacha un job en una cola configurable
para guardar esos datos en la base de datos.

Debes especificar estos parámetros dentro de `params` en la configuración:

 - `queue`: Nombre de la cola en la que lanzar los _jobs_ que guardaran información en la base de datos. Es posible 
   utilizar la cola `sync` para que la inserción se produzca en tiempo real, pero en ese caso, si la petición se 
   realiza dentro de una transacción (cosa poco recomendable), entonces la propia petición podría no quedar 
   registrada si la transacción es abortada.
 - `register_successful_requests`: Bool. Indica al _Sink_ si debe registrar las peticiones que tienen éxito. 
   Estas son aquellas que devuelven un código de estado `2xx` y no generan ninguna excepción durante su 
   decodificación dentro del cliente (síncrono) o la promesa (asíncrono). No implica que se registre una petición 
   donde el servicio externo responde con éxito pero la respuesta no pasa las reglas de validación impuestas a la 
   misma, por ejemplo.
 - `register_failed_requests`: Bool. Indica al _Sink_ si debe registrar las peticiones fallidas, entendiéndose 
   como tal cualquier petición que genera una excepción durante el proceso de la misma.


#### Registros en logs: `LogSink` ####

`LogSink` es una implementación de _Sink_ que registra peticiones y respuestas a través de un canal de log. Este
_Sink_ no requiere de ninguna tabla de base de datos, solo de un canal de logging válido.

Es necesario especificar los siguientes parámetros dentro de `params` en la configuración:

 - `log_channel`: Nombre del canal en el que se deben escribir los registros de peticiones y respuestas.
 - `register_successful_requests`: Bool. Mismo comportamiento que en `DatabaseSink`.
 - `register_failed_requests`: Bool. Mismo comportamiento que en `DatabaseSink`.


### Extractores de contenido ###

#### NoContentExtractor ####

Esta implementación de extractor de contenido nunca reproduce el auténtico contenido del mensaje en el registro;
se limita a leer el valor de la cabecera `Content-Type` si puede y devolver como contenido una cadena de 
texto similar a la siguiente: `-- Skipped content of type '%s' weighting %s KiB --`.

No requiere de parámetros de configuración.


#### SimpleContentExtractor ####

Este extractor de contenido permite indicar una lista blanca de tipos de contenido (valores de cabecera
`Content-Type`) para los que se debe registrar el contenido del mensaje. También permite indicar por separado
si se debe extraer el contenido de mensajes sin `Content-Type`. Por último, permite establecer un límite de 
tamaño en bytes para el contenido de los mensajes. Si un determinado mensaje supera este límite, el mensaje
es truncado.

Requiere de los siguientes parámetros:

 - `allowedContentTypes`: Array con una lista de valores de cabecera `Content-Type`. Si el mensaje procesado
   tiene cabecera `Content-Type` y su valor coincide con alguno de los aquí indicados, el contenido del
   mismo será extraído.
 - `allowEmptyContentType`: Bool, indica si el contenido del mensaje debe ser extraído en el caso de que
   no tenga un `Content-Type` definido.
 - `sizeThreshold`: Int, tamaño máximo en bytes de la respuesta. Respuestas con tamaños superiores serán truncadas
   para no superar este límite.


## Ejemplos ##

### `DatabaseSink` típico ###

Suponemos que hay definida una cola en segundo plano para jobs pequeños de baja prioridad con el 
nombre `low_priority`. Indicamos al _sink_ que registre peticiones con éxito o fallidas indistintamente,
y establecemos extractores de contenido que procurarán registrar el contenido de los mensajes cuando este
sea textual, sin superar el medio megabyte de tamaño (truncando contenido si lo supera).

```php
// config/beat_httpclient.php

return [
    'sinks' => [
        'db_sink' => [
            'class' => \Beat\HttpClient\Sinks\DatabaseSink::class,
            'params' => [
                'queue' => 'low_priority',
                'register_successful_requests' => true,
                'register_failed_requests' => true,
            ],
            'request_content_extractor' => [
                'class' => \Beat\HttpClient\ContentExtractors\SimpleContentExtractor::class,
                'params' => [
                    'allowedContentTypes' => ['application/json', 'text/html', 'text'],
                    'allowEmptyContentType' => true,
                    'sizeThreshold' => 1024 * 512, // .5 MiB
                ],
            ],
            'response_content_extractor' => [
                'class' => \Beat\HttpClient\ContentExtractors\SimpleContentExtractor::class,
                'params' => [
                    'allowedContentTypes' => ['application/json', 'text/html', 'text'],
                    'allowEmptyContentType' => true,
                    'sizeThreshold' => 1024 * 512, // .5 MiB
                ],
            ],
        ],
    ],
    
    // ...
];
```


### `LogSink` típico ###

Suponemos que existe un canal de logging llamado `external_http`. En este caso solo nos vamos a interesar por
peticiones fallidas (de más interés para depuración) con extracción de respuestas pero no de peticiones.

```php
// config/beat_httpclient.php

return [
    'sinks' => [
        'log_sink' => [
            'class' => LogSink::class,
            'params' => [
                'log_channel' => 'external_http',
                'register_successful_requests' => false,
                'register_failed_requests' => true,
            ],
            'request_content_extractor' => [
                'class' => \Beat\HttpClient\ContentExtractors\NoContentExtractor::class,
                'params' => [],
            ],
            'response_content_extractor' => [
                'class' => \Beat\HttpClient\ContentExtractors\SimpleContentExtractor::class,
                'params' => [
                    'allowedContentTypes' => ['application/json', 'text/html', 'text'],
                    'allowEmptyContentType' => true,
                    'sizeThreshold' => 0, // Sin límite
                ],
            ],
        ],
    ],
    
    // ...
];
```
