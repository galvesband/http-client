# Vistazo / Overview #

A continuación describo conceptos que maneja el paquete para hacer sus cosas.


## Visión global ##

El paquete proporciona un _Facade_ que da acceso a la factoría de clientes, que es el punto de
entrada principal del paquete. La factoría construye clientes según la configuración con diferentes
componentes ([sinks](#sinks-o-registradores-), [proveedores de sellado](#seal-providers-o-proveedores-de-sellado-),
etcétera).

A vista de pájaro, estas son las piezas más importantes del paquete:

 - `ClientFactory` construye nuevos clientes. Es la clase que está detrás del _Facade_ `HttpClient`.

 - `Client` representa un cliente con una configuración (_profile_) concreto. Permite hacer peticiones
   http usando el constructor de peticiones (`RequestBuilder`) a través del método `newRequest()`.
   Contiene una instancia interna de un cliente HTTP de _GuzzleHttp_ que se utilizará para hacer
   las peticiones a bajo nivel.

 - `RequestBuilder` son objetos devueltos por un `Client` cuando se quiere hacer una nueva petición.
   Permite hacer llamadas de estilo _fluido_ para modificar los parámetros de la petición, y puede
   realizar la petición de forma síncrona o asíncrona. Los `RequestBuilder` utilizan instancias de
   `RequestAddon` para añadir funcionalidades a la petición de forma opcional según las necesidades
   concretas de la petición. 

 - `GuzzleMiddleware` es una clase interna que implementa los _hooks_ y el comportamiento adicional
   necesario sobre un cliente Http de _GuzzleHttp_. Intercepta las llamadas hechas a través del 
   cliente _GuzzleHttp_ interno, transforma respuestas o excepciones en implementaciones propias 
   del paquete con información adicional y da oportunidad a los _Sinks_ configurados en el perfil
   al que pertenece para procesar la petición y el resultado de la misma.

 - Un `Client` tiene configurados 0 o más instancias de `Sink`, que procesarán la petición y 
   su resultado para registrarlo o hacer _algo_ con esos datos. Por ejemplo, `DatabaseSink` persiste
   las peticiones y sus resultados en base de datos, mientras que `LogSink` escribe las respuestas
   en un log.

 - Los `SealProvider` implementan lógica de autenticación. Pueden añadirse a un `Client` a través de 
   la configuración de forma que todas las peticiones hechas a través de ese perfil apliquen ese esquema
   de autenticación. Cada sistema de autenticación que se desee utilizar de esta forma debe tener una  
   implementación de `SealProvider`.

Una vez el paquete este correctamente configurado y los perfiles de cliente http definidos,
el _Facade_ (o `ClientFactory`) deben ser capaces de crear clientes basados en esas configuraciones
de perfiles. A través de esos clientes se pueden realizar peticiones que tengan en cuenta la configuración
definida para el perfil que dio lugar al cliente en `ClientFactory`, de forma que las peticiones
procesadas son automáticamente de forma transparente para el código que hizo la petición. Ese proceso
está asociado a eventos (`SuccessfulHttpRequestEvent` y `FailedHttpRequestEvent`).


## Por partes ##

### Sinks o Registradores ###

Un _Sink_ es un componente que se añade a clientes (según configuración) y tendrá la oportunidad 
de hacer algo con la petición y el resultado de la misma durante el proceso. El paquete trae
dos implementaciones de _Sinks_, una que registra las peticiones en el log y otra que registra
las peticiones en base de datos. Mira la documentación de la configuración para más detalles.


### Seal Providers o Proveedores de sellado ###

Un _SealProvider_ es un componente que añade _algo_ a la petición, normalmente (o fueron diseñados
en principio con idea de) algo relativo a un esquema de autenticación. Por ejemplo, un _SealProvider_
podría añadir una cabecera con un token o cosas así. Mira la documentación de la configuración para 
más detalles.


### Profiles o Perfiles ###

Un _Profile_ engloba la configuración de un tipo de cliente. `ClientFactory` construye instancias
de clientes a partir de la configuración asociada al mismo en la configuración. Esa configuración
indica a `ClientFactory` que _Sinks_ debe añadir a `Client`, qué _SealProvider_, qué opciones
hay que incluir por defecto, etcétera. Mira la documentación de la configuración para más detalles.

