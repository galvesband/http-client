# Eventos #

Durante el procesado de la petición se lanzan 2 eventos con el resultado de la misma:

 - `SuccessfulHttpRequestEvent`: Petición con éxito.
 - `FailedHttprequestEvent`: Petición fallida.
