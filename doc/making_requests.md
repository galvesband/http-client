# Haciendo peticiones #

`ClientFactory` (o la fachada `HttpClient`) pueden generar instancias de `Client` utilizando
configuraciones de perfiles (_profiles_) definidos en la configuración. Esos clientes permiten
realizar peticiones HTTP construyendo las mismas con una API fluida implementada por
`RequestBuilder`. 

A continuación se muestran ejemplos de casos de uso típicos. En todos ellos supondremos que
en la configuración hay definido un perfil `pruebas` que usaremos en las peticiones de ejemplo.


## Petición básica ##

Las peticiones HTTP surgen de una instancia de `Client` (creado por `ClientFactory`). El método
`newRequest()` crea un objeto `RequestBuilder` que tiene métodos que permiten construir la
petición de forma fluida. Para peticiones simples, sin embargo, bastará con definir a dónde
se realiza la petición con `to()` y ejecutar la petición (`execute()`).

```php
use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\ServerException;

try {
    $response = HttpClient::profile('pruebas')->newRequest('Haciendo petición simple')
        ->to('GET', '/api/alive')
        ->execute();
} catch (ServerException $se) {
    // Respuesta del servidor ha sido 5xx
    die("Servidor con problemas: " . $se->getMessage());
} catch (CommunicationException $ce) {
    // Error de comunicación genérico
    die($ce->getMessage());
}

assert($response->getStatusCode() === 204);
```

La petición se realizará con las opciones del perfil `pruebas`. El ejemplo no especifica
protocolo, host o puerto en los parámetros de `to()` porque contamos con que la configuración
del perfil establece un `base_uri` que se utilizará para construir la dirección final
a la que se hace la petición. También se aplicará algún esquema de autenticación si el perfil
tiene configurado un `SealProvider`.

Además, si el perfil `pruebas` tiene configurado algún `Sink`, entonces tanto la petición como 
el resultado de la misma serán procesados por esos _Sinks_.


## Peticiones con datos JSON ##

Dado que uno de los casos principales de uso del paquete es en APIs REST basadas en JSON,
tanto `RequestBuilder` como `Response` ofrecen métodos específicos para facilitar el trabajo
con este tipo de datos.

Para incluir datos JSON en una petición hay que usar el método `withJson()`:

```php
use Beat\HttpClient\Facades\HttpClient;

// Lanzará excepción si respuesta no es 2xx
$response = HttpClient::profile('pruebas')->newRequest('Petición con datos JSON')
    ->to('POST', '/api/login')
    ->withJsonData(['user' => 'Wayne', 'password' => 'BatMetal6969'])
    ->execute();

assert($response->getStatusCode() === 200);
```

El método `withJsonData()` acepta un array que será codificado como JSON e incluido en 
la petición junto con la cabecera HTTP `Content-Type: application/json`.

En este caso no estamos decodificando la respuesta, que presumiblemente también sería un JSON.
El siguiente ejemplo muestra cómo podemos decodificar e incluso validar los datos devueltos.


## Peticiones que esperan un JSON ##

Normalmente, las APIs que esperan JSONs de entrada también responden con JSONs. Las peticiones
realizadas con HttpClient pueden decodificar y aplicar reglas de validación como parte del 
proceso de la petición, y emitirán excepciones apropiadas si cualquiera de estas operaciones 
no puede realizarse con éxito.


### Recibiendo un JSON, decodificándolo y validándolo ###

Cuando se está definiendo una nueva petición, es posible utilizar el método `->expectsJson()`
para indicar al constructor que la respuesta debería ser un JSON. En la práctica, esto 
significa que la petición incluirá la cabecera 'Accept: application/json' y en caso de que
la respuesta sea un `2xx`, se intentará decodificar los datos recibidos en la respuesta 
como un JSON.

```php
use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Exceptions\InvalidJsonException

try {
    $data = HttpClient::profile('pruebas')->newRequest('Petición que envía y recibe JSON')
        ->to('POST', '/api/login')
        ->withJsonData(['user' => 'Wayne', 'password' => 'BatMetal6969'])
        ->expectsJson()
        ->execute()
        ->getJsonData();
} catch (InvalidJsonException $error) {
    die("La respuesta es 2xx, pero el contenido no es un JSON válido: " . $error->getMessage());
}
```

Es importante tener en cuenta que el método `getJsonData()` en la respuesta 
solo está disponible si antes del método `execute()` se hizo una llamada a 
`expectsJson()`. Internamente, el constructor de peticiones añade un `RequestAddon`
que implementa la lógica que añade las cabeceras apropiadas a la petición y 
decodifica y, opcionalmente, valida los datos devueltos.


### Validando los datos recibidos ###

Es posible validar los datos como parte del proceso de comunicación, además
de decodificar un JSON. Las respuestas exitosas que son resultado de peticiones 
realizadas utilizando el método `expectsJson()` tienen disponibles más métodos
relacionados con JSONs en la respuesta. 

```php
use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Exceptions\InvalidJsonException
use Beat\HttpClient\Exceptions\ResponseValidationException;

try {
    $token = HttpClient::profile('pruebas')->newRequest('Petición que envía y recibe JSON')
        ->to('POST', '/api/login')
        ->withJsonData(['user' => 'Wayne', 'password' => 'BatMetal6969'])
        ->expectsJson([
            'data'       => 'required|array',
            'data.token' => 'required|string|regex:^[A-Z0-9]+$|between:16,32'        
        ])
        ->execute()
        ->getJsonData()['data']['token'];
} catch (InvalidJsonException $iae) {
    die("Respuesta no es un JSON válido!");
} catch (ResponseValidationException $rve) {
    die("Respuesta no pasa reglas de validación! " . json_encode($rve->getValidationErrors()));
}
```

No es necesario especificar las reglas de validación junto con `expectsJson()`.
También se pueden aplicar validaciones una vez realizada la petición, con
la respuesta devuelta por `execute()`:

```php
use Beat\HttpClient\Facades\HttpClient;

$response = HttpClient::profile('pruebas')->newRequest('Validando mucho')
    ->to('GET', '/api/secciones')
    ->expectsJson()
    ->execute();

$response->validateJsonData([
    'data'               => 'present|array',
    'data.*'             => 'array',
    'data.*.code'        => 'required|string|between:1,3|unique:sections,code',
    'data.*.description' => 'required|string|max:64',
]);
```

Tanto `expectsJson()` como `validateJsonData()` permiten un segundo parámetro opcional
que acepta un `Closure` similar al que se usa en los `FormRequest` de Laravel.

```php
$response->validateJsonData([
    'data'               => 'present|array',
    'data.*'             => 'array',
    'data.*.code'        => 'required|string|between:1,3|unique:sections,code',
    'data.*.description' => 'required|string|max:64',
], function (Validator $validator) {
    if ($validator->errors->isNotEmpty()) { return; }
    
    // Valida más los datos de entrada...
});
```


## Peticiones asíncronas ##

Internamente, _HttpClient_ utiliza _GuzzleHttp_ para hacer las peticiones, y mantiene la capacidad
de hacer peticiones asíncronas de este. Basta con cambiar `execute()` por `executeAsync()`, cuyo
resultado no es un objeto `Response`, sino la promesa del mismo (`PromiseInterface<Response>`).
Para que la petición se realice _realmente_ hay que llamar al método `wait()` de la promesa.

```php
use Beat\HttpClient\Facades\HttpClient;

$response = HttpClient::profile('pruebas')->newRequest('Validando mucho')
    ->to('GET', '/api/secciones')
    ->expectsJson()
    ->executeAsync()
    ->wait();
```

Normalmente, las peticiones asíncronas son útiles para realizar varias peticiones simultáneas
en lugar de secuencialmente, que sería lo que ocurriría utilizando `execute()`. Para poder
lograr ese efecto hay que definir las peticiones y llamar a `executeAsync()` en todas ellas, para
después usar el método proporcionado por _GuzzleHttp_ para hacer una espera de varias promesas
simultáneamente:

```php
use Beat\HttpClient\Facades\HttpClient;
use GuzzleHttp\Promise\Utils;

$client = HttpClient::profile('pruebas');
$requests_endpoints = [
    ['GET', '/api/secciones', 'secciones'],
    ['GET', '/api/familias', 'familias']
];

$promises = collect($requests_endpoints)
    ->mapWithKeys(function (array $endpoint) use ($client) {
        list($method, $uri, $key) = $endpoint;
        return [
            $key => $client->newRequest(sprintf('Petición a %s %s', $method, $uri))
                ->to($method, $uri)
                ->expectsJson()
                ->executeAsync();
        ];   
    });

// Lanzará cualquier excepción que surja al hacer las peticiones.
$responses = Utils::unwrap($promises);
```

### Aviso sobre peticiones asíncronas ###

Por dentro, el paquete usa las promesas de _GuzzleHttp_. Es importante tener claro que estas promesas
y el propio _GuzzleHttp_ no son capaces de hacer peticiones HTTP y ejecutar código PHP simultáneamente.
Lo que realmente ocurre es que _GuzzleHttp_ llama internamente a CURL para hacer una multi-petición,
una función de CURL que permite de una sola tacada hacer varias peticiones. Mientras CURL hace esas
peticiones, _GuzzleHttp_ y el proceso PHP quedan en espera, como ocurriría con una petición síncrona.

Por tanto, no es posible hacer una petición asíncrona y, mientras esperamos la respuesta, hacer otras
operaciones en PHP.
