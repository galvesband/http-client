# Testing #

El paquete incluye algunas funciones adicionales para facilitar mocks de peticiones HTTP en tests de código
que haga peticiones externas.

## Preparación de los tests ##

La clase de los tests (que normalmente extenderá `TestCase`) debe usar el trait `InteractsWithHttpClient`. 
Este trait se integrará con el proceso de inicialización y destrucción del test (`setUp()` y `tearDown()`), 
señalizando al principio del test a HttpClient que estamos ejecutando pruebas que hacen peticiones http y 
comprobando al terminar el test que todas las expectativas de peticiones definidas en el test han sido satisfechas.

```php
class MyTestClass extends TestCase
{
    use \Beat\HttpClient\Testing\InteractsWithHttpClient;
    
    /** @test */
    public function my_test()
    {
        // Do some testing...
    }
}
```

> **Nota importante**: En Laravel 8 y algunas versiones iniciales de Laravel 9 la integración no es automática.
> Es necesario añadir al `setUp()` de la clase de testeo una línea llamando a `$this->setUpInteractsWithHttpClient()`
> y otra al `tearDown()` llamando a `$this->tearDownInteractsWithHttpClient()`. Esto no es necesario en Laravel 9
> a partir de junio de 2022 y posteriores.
>
> Ejemplo:
>
> ```php
> class MyTestClass extends TestCase
> {
>     use \Beat\HttpClient\Testing\InteractsWithHttpClient;
> 
>     public function setUp()
>     {
>         parent::setUp();
>         $this->setUpInteractsWithHttpClient();
>     }
> 
>     public function tearDown()
>     {
>         $this->tearDownInteractsWithHttpClient();
>         parent::tearDown(); 
>     }
> 
>     /** @test */
>     public function my_test()
>     {
>         // Do some testing...
>     }
> }
> ```

## Definiendo expectativas de peticiones HTTP en los tests ##

En aquellos tests que el código ejecutado utilizaría HttpClient para hacer peticiones HTTP externas, es posible
definir una _expectativa_ de petición Http. Esta expectativa definirá unas condiciones que debe cumplir la petición
Http realizada a través de HttpClient, y la respuesta que debe ser devuelta al código que hizo la petición. 
Dicho de otro modo, permite hacer algo parecido a _mocks_ de peticiones Http y la respuesta de las mismas.

Lo mejor es ver un ejemplo:

```php
class TestExampleWithExpectationsTest extends TestCase
{
    use \Beat\HttpClient\Testing\InteractsWithHttpClient;
    
    /**
     * Ejemplo de test "correcto", que debe pasar. Define una expectativa Http y hace una petición que la satisface. 
     * @test 
     */
    public function expects_to_make_one_external_request()
    {
        // Definimos la expectativa de una petición Http...
        HttpClient::onProfile('default')
            ->expectRequest(function (\Psr\Http\Message\RequestInterface $request) {
                // Esto nos permite examinar la petición que se está realizando y devolver true o false
                // dependiendo de si "reconocemos" la petición o no en esta expectativa.
                return $request->getMethod() === 'GET' && (string) $request->getUri() === 'http://localhost/whatevah';
            })
            // Esperamos esta petición una sola vez
            ->once()
            // La petición debe devolver un 404 con el cuerpo 'NotFound'.
            ->resultingInResponse(404, 'NotFound');
            
        // El test ahora prepararía el código que va a hacer la llamada y lo ejecutaría. 
        // Algo parecido a:
        //$svc = new MyService();
        //$result = $svc->doSomething(); // Hace una petición Http externa...
        //...
        
        // Para el ejemplo, haremos la llamada directamente en lugar de instanciar un servicio...
        // Como la petición realizada coincide con la expectativa definida arriba, $response será
        // un objeto RespondeInterface con código 404 y cuerpo 'NotFound'. La expectativa será marcada
        // como cumplida y el test terminará con éxito. 
        $response = HttpClient::profile('default')->newRequest('Whatevah?')
            ->to('GET', '/whatevah')
            ->execute();
    }
    
    /**
     * Ejemplo de test que hace una petición inesperada. Test fallido.
     * @test 
     */
    public function unexpected_request()
    {
        // En este ejemplo haremos una petición que no coincide con la expectativa. Esto lanzará una excepción
        // dentro de HttpClient porque la petición no ha coincidido con ninguna expectativa definida y, por tanto,
        // es inesperada.
        $response = HttpClient::profile('default')->newRequest('Not whatevah')
            ->to('GET', '/unexpected-path')
            ->execute();
    }
    
    /** 
     * Ejemplo de test que define una expectativa de petición http que nunca se hizo. 
     * @test 
     */
    public function request_not_made_but_expected()
    {
        // Definimos la expectativa de una petición Http...
        HttpClient::onProfile('default')
            ->expectRequest(function (\Psr\Http\Message\RequestInterface $request) {
                return $request->getMethod() === 'GET' && (string) $request->getUri() === 'http://localhost/whatevah';
            })
            ->once()
            ->resultingInResponse(404, 'NotFound');
            
        // Al cerrarse el test, el trait buscará expectativas definidas pero no cumplidas. En caso de encontrar alguna,
        // marcará el test como fallido.
    }
}
```