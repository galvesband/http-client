# Instalación #

El paquete se encuentra disponible en [gitlab](https://gitlab.com/galvesband/http-client) como un paquete público.


## Integrar HttpClient en un proyecto Laravel ##

El paquete está instalado, pero aún no está listo para ser utilizado o configurado. A continuación hay una descripción
de los pasos usuales para configurarlo correctamente en el _framework_.


### Añadir el ServiceProvider a la aplicación ###

Para que el paquete funcione correctamente es necesario registrar el Service Provider del mismo. Edita 
`config/app.php` y busca la entrada `providers`. Añade el Service Provider de Beat/HttpClient:

```php
<?php
// config/app.php

return [
    // ...
    
    'providers' => [
        // ...
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Beat\HttpClient\BeatHttpClientServiceProvider::class, // Línea añadida
        
        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // ...
    ],
];
```


### Añadir migración para registro de peticiones en BD ###

Beat/HttpClient es capaz de registrar las peticiones realizadas en base de datos (configurable
en lo relativo a perfiles). Para que esta funcionalidad esté operativa es necesario registrar
la migración que crea la tabla `requests`:

```bash
php artisan vendor:publish --provider="Beat\\HttpClient\\BeatHttpClientServiceProvider" --tag="migrations" 
```

Esto añadirá una nueva migración al proyecto con la especificación de la nueva tabla `requests`.


### Añadir archivo de configuración ###

El paquete permite configurar y definir diferentes clientes Http (perfiles) que usen distintos
_sinks_, proveedores de sellos, URIs base, ... Toda esa información se especifica en un archivo
de configuración llamado `beat_httpclient.php`. Añade el archivo a tu proyecto ejecutando
el siguiente comando:

```bash
php artisan vendor:publish --provider="Beat\\HttpClient\\BeatHttpClientServiceProvider" --tag="config"
```


### Añadir trait para acceso a peticiones a modelos autenticables ###

Las peticiones registradas quedan asociadas al modelo que está identificado en el momento de 
la petición (de haber alguno). Si quieres que los modelos "autenticatables" tengan definida la relación
con las peticiones que han realizado, añade el trait `HasHttpRequests` a dichos modelos.
Es completamente opcional. Por ejemplo, un modelo `User` acabaría más o menos así:

```php
<?php
class Usuario extends Authenticatable 
{
    use HasFactory,
        HasHttpRequests,    // Añade la relación con las peticiones registradas por HttpClient
        SoftDeletes,
        Notifiable;

    // ...
}
```