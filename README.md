# Cliente HTTP con funciones propias de proyectos BEAT en Laravel #

Beat/HttpClient es un paquete PHP que intenta que sea sencillo hacer peticiones a otras APIs. Está pensada
para los casos de uso más típicos en proyectos Laravel / BEAT. Aun así, es bastante flexible y configurable,
y permite automatizar registros de peticiones realizadas por el sistema en base de datos y logs.

```php
use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Exceptions\ResponseValidationException
use Beat\HttpClient\Exceptions\ServerException;
use Beat\HttpClient\Exceptions\CommunicationException;

// Realizar una petición a google.es de forma asíncrona. La petición realmente se realizará al llamar a wait()
// o similar en la promesa resultante. Es posible hacer peticiones simultáneas agrupando las promesas y
// desempaquetándolas (unwrap()).
$response_promise = HttpClient::newRequest('Using default profile')
   ->to('GET', 'https://google.es')
   ->executeAsync();

// Una petición síncrona que espera un json.
// La respuesta es decodificada y validada automáticamente. Se lanzan excepciones apropiadas según el caso.
try {
    $validated_data = HttpClient::onProfile('lx')->newRequest('Obtener secciones de LX')
        ->to('GET', '/api/externo/secciones')
        ->expectsJson()
        ->withValidationRules([
            'data'               => 'present|array',
            'data.*'             => 'array',
            'data.*.codigo'      => 'required|string|max:5',
            'data.*.descripcion' => 'required|string|max:255',
        ])
        ->withValidatorCallback(function (Validator $v) {
            if ($v->errors()->isNotEmpty()) { return; }
            
            // Comprobar más cosas de los datos de la respuesta
        })
        ->execute();        
} catch (ResponseValidationException $rve) {
    echo "Invalid response...\n";
    echo print_r($rve->getValidationErrors(), true);
    die(1);
} catch (InvalidJsonException $ije) {
    echo "Response is not a JSON...\n";
    die(1);
} catch (ServerException $se) {
    // ...
} catch (CommunicationException $ce) {
    // ...
}
```

Internamente, el paquete utiliza GuzzleHttp (0.7.x).


## Objetivos ##

Este paquete nació para ofrecer una implementación reutilizable, bien testeada y flexible de un cliente HTTP
con funcionalidades para registro de peticiones integradas y facilidades para autenticación y validación 
de respuestas en APIs BEAT.

Desarrollando un poco los objetivos:

- Definición por configuración de perfiles de clientes HTTP. Cada configuración / perfil ya establece
  que registros van a generar las peticiones hechas con el perfil, además de poder especificarse
  opciones comunes para las peticiones del perfil, como una URI base, un proveedor de sellos (autenticación), etc.
 
- API para realizar peticiones fluida y con funciones e integraciones pensadas para los casos de uso de 
  proyectos BEAT. En especial, soporte para peticiones JSON (envío, recepción, decodificación, validación).

- Soporte para perfiles donde se configuren de forma sencilla los parámetros que interesan en
  las comunicaciones que se suelen llevar a cabo en Beat.

- API fluída para hacer peticiones, con soporte para peticiones JSON en particular.

- Registro de peticiones realizadas. Cada perfil puede indicar que _Sinks_ deben registrar las peticiones
  a través del mismo. El paquete incluye 2 implementaciones de Sinks, para Logs y base de datos.

- Descubrir el mundo de los paquetes reutilizables de Laravel.


## Requisitos ##

 - PHP (7.4, 8.0, 8.1 o 8.2)
 - Laravel 8.x
 - `ext-curl`


## Guía de usuario ##

- [Instalación](./doc/installation.md)
- [Vistazo / Overview](./doc/overview.md)
- [Configuración](./doc/configuration.md)
- [Haciendo peticiones](./doc/making_requests.md)
- [Registrar peticiones y respuestas (Sinks)](./doc/registering_requests_and_responses.md)
- [Autenticación en sistemas externos (SealProviders)](./doc/authentication_and_seal_providers.md)
- [Eventos](./doc/events.md)
- [Testing](./doc/testing.md)


## TODO

 - ResponseValidationException: getters para datos decodificados. Maybe validator.

 - profile_assignment: pre-asignar profiles a clases en configuración de forma que el service provider
 configure un perfil de cliente concreto para esas clases cuando se generen via inyección de dependencias.

