<?php

namespace Beat\HttpClient\Tests\Unit\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\Exceptions\RequestException;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class RequestExceptionTest extends TestCase
{
    /** @test */
    public function check_render_and_context_method()
    {
        $subject = new RequestException(
            new Request('POST', 'http://localhost:8000/', [], '{"name":"Miles"}'),
            new Response(200, [], '{"message":"bro, wtf..."}'),
            null,
            new RequestOptions(),
            0.5
        );

        $context = $subject->context();
        $this->assertSame([
            'exception_type' => 'RequestException',
            'code'           => 502,
            'request'        => [
                'endpoint' => 'POST http://localhost:8000/',
                'headers'  => [
                    'Host' => ['localhost:8000'],
                ],
                'body' => '{"name":"Miles"}'
            ],
            'time' => "0.5s",
            'response' => [
                'status' => 200,
                'body'   => '{"message":"bro, wtf..."}',
            ],
        ], $context);

        $response = $subject->render();
        $this->assertEquals(502, $response->status());
        $this->assertSame(
            [
                'message' => 'Una petición a un sistema externo no ha podido ser completada. ' .
                    'Inténtelo más tarde o consulte con un administrador.'
            ],
            json_decode($response->content(), true)
        );
    }
}