<?php

namespace Beat\HttpClient\Tests\Unit\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;

class CommunicationExceptionTest extends TestCase
{
    protected function build_communication_exception(): CommunicationException
    {
        return new CommunicationException(
            new Request('GET', 'http://localhost:9000/'),
            new RequestOptions(),
            0.01,
            null,
            'Lerele'
        );
    }

    /**
     * For the sake of code-coverage!
     *
     * @test
     */
    public function check_some_getters_and_setters()
    {
        $subject = $this->build_communication_exception();
        $this->assertEquals('Lerele', $subject->getPublicMessage());
    }

    /** @test */
    public function check_render_method()
    {
        $subject = $this->build_communication_exception();
        $response = $subject->render();

        $this->assertEquals(502, $response->status());
        $this->assertSame(['message' => 'Lerele'], json_decode($response->getContent(), true));
    }

    /** @test */
    public function check_context_method()
    {
        $subject = $this->build_communication_exception();
        $context = $subject->context();

        $this->assertSame([
            'exception_type' => 'CommunicationException',
            'code' => 502,
            'request' => [
                'endpoint' => 'GET http://localhost:9000/',
                'headers' => [
                    'Host' => [
                        'localhost:9000'
                    ],
                ],
            ],
            'time' => '0.01s',
        ], $context);
    }
}