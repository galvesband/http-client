<?php

namespace Beat\HttpClient\Tests\Unit\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\Exceptions\ServerException;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class ServerExceptionTest extends TestCase
{
    /** @test */
    public function check_generic_internal_server_error()
    {
        $subject = new ServerException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(500, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertTrue($subject->isGenericServerError());
        $this->assertFalse($subject->isBadGateway());
        $this->assertFalse($subject->isTemporaryOutOfService());
        $this->assertFalse($subject->isGatewayTimeout());
    }

    /** @test */
    public function check_bad_gateway()
    {
        $subject = new ServerException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(502, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isGenericServerError());
        $this->assertTrue($subject->isBadGateway());
        $this->assertFalse($subject->isTemporaryOutOfService());
        $this->assertFalse($subject->isGatewayTimeout());
    }

    /** @test */
    public function check_temporary_out_of_service()
    {
        $subject = new ServerException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(503, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isGenericServerError());
        $this->assertFalse($subject->isBadGateway());
        $this->assertTrue($subject->isTemporaryOutOfService());
        $this->assertFalse($subject->isGatewayTimeout());
    }

    /** @test */
    public function check_gateway_timeout()
    {
        $subject = new ServerException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(504, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isGenericServerError());
        $this->assertFalse($subject->isBadGateway());
        $this->assertFalse($subject->isTemporaryOutOfService());
        $this->assertTrue($subject->isGatewayTimeout());
    }
}