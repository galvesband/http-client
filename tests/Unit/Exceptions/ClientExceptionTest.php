<?php

namespace Beat\HttpClient\Tests\Unit\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\Exceptions\ClientException;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class ClientExceptionTest extends TestCase
{
    /** @test */
    public function check_identification_error()
    {
        $subject = new ClientException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(401, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertTrue($subject->isIdentificationError());
        $this->assertFalse($subject->isAuthorizationError());
        $this->assertFalse($subject->isNotFound());
        $this->assertFalse($subject->isValidationError());
        $this->assertFalse($subject->isTooManyRequests());
    }

    /** @test */
    public function check_authorization_error()
    {
        $subject = new ClientException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(403, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isIdentificationError());
        $this->assertTrue($subject->isAuthorizationError());
        $this->assertFalse($subject->isNotFound());
        $this->assertFalse($subject->isValidationError());
        $this->assertFalse($subject->isTooManyRequests());
    }

    /** @test */
    public function check_not_found_error()
    {
        $subject = new ClientException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(404, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isIdentificationError());
        $this->assertFalse($subject->isAuthorizationError());
        $this->assertTrue($subject->isNotFound());
        $this->assertFalse($subject->isValidationError());
        $this->assertFalse($subject->isTooManyRequests());
    }

    /** @test */
    public function check_validation_error()
    {
        $subject = new ClientException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(422, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isIdentificationError());
        $this->assertFalse($subject->isAuthorizationError());
        $this->assertFalse($subject->isNotFound());
        $this->assertTrue($subject->isValidationError());
        $this->assertFalse($subject->isTooManyRequests());
    }

    /** @test */
    public function check_too_many_requests_error()
    {
        $subject = new ClientException(
            new Request('GET', 'http://localhost:9000/'),
            new GuzzleResponse(429, [], ''),
            null,
            new RequestOptions(),
            0.5
        );

        $this->assertFalse($subject->isIdentificationError());
        $this->assertFalse($subject->isAuthorizationError());
        $this->assertFalse($subject->isNotFound());
        $this->assertFalse($subject->isValidationError());
        $this->assertTrue($subject->isTooManyRequests());
    }
}