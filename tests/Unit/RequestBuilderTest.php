<?php

namespace Beat\HttpClient\Tests\Unit;

use BadMethodCallException;
use Beat\HttpClient\BuilderAddons\ExpectsJsonAddon;
use Beat\HttpClient\BuilderAddons\JsonDataAddon;
use Beat\HttpClient\Client;
use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\Tests\Misc\TestAddon;
use Beat\HttpClient\Tests\Misc\TestSink;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;
use Mockery;
use Mockery\MockInterface;

class RequestBuilderTest extends TestCase
{
    /** @test */
    public function has_sane_defaults()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = new RequestBuilder($client_mock, 'My Label');

        $this->assertEquals('My Label', $builder->getLabel());
        $this->assertEquals('GET', $builder->getMethod());
        $this->assertEquals('', $builder->getUri());
        $this->assertEmpty($builder->getHeaders());
        $this->assertEquals(20, $builder->getTimeout());
        $this->assertEquals(10, $builder->getConnectTimeout());
        $this->assertEquals('Beat/HttpClient', $builder->getUserAgent());
        $this->assertTrue($builder->getAddons()->isEmpty());
    }

    /* ************** */
    /* MUTATORS TESTS */
    /* ************** */

    /** @test */
    public function to_method_works_as_expected()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->to('POST', 'http://localhost/api');

        $this->assertEquals('POST', $builder->getMethod());
        $this->assertEquals('http://localhost/api', $builder->getUri());
    }

    /** @test */
    public function user_agent_method_works_as_expected()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->userAgent('LolailoClient');

        $this->assertEquals('LolailoClient', $builder->getUserAgent());
    }

    /** @test */
    public function add_header_method_works_as_expected()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        // Add header when there is none
        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->addHeader('My-Header', 'Sup');
        $this->assertSame(['My-Header' => ['Sup']], $builder->getHeaders());

        // Add a header without replacing
        $builder->addHeader('My-Header', 'Up', false);
        $this->assertSame(['My-Header' => ['Sup', 'Up']], $builder->getHeaders());

        // Add a header replacing previous values
        $builder->addHeader('My-Header', 'What\' up, Buddy');
        $this->assertSame(['My-Header' => ['What\' up, Buddy']], $builder->getHeaders());
    }

    /** @test */
    public function set_timeouts_method_works_as_expected()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->setTimeouts(15, 30);

        $this->assertEquals(15, $builder->getConnectTimeout());
        $this->assertEquals(30, $builder->getTimeout());
    }

    /** @test */
    public function adds_a_single_sink()
    {
        /** @var Client $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->pushSink(new TestSink('My Sink'));

        $this->assertCount(1, $builder->getSInks());
        $this->assertInstanceOf(TestSink::class, $builder->getSinks()->first());
    }

    /** @test */
    public function adds_multiple_sinks()
    {
        /** @var Client $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->pushSinks(collect([
                new TestSink('My Sink 1'), new TestSink('My Sink 2'),
            ]));

        $this->assertCount(2, $builder->getSinks());
    }

    /** @test */
    public function ignore_sinks_pushed_if_already_present()
    {
        /** @var Client $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->pushSink(new TestSink('My Sink'));
        $this->assertCount(1, $builder->getSinks());

        // Intentar añadir un sink con el mismo nombre no hace nada.
        $builder->pushSink(new TestSink('My Sink'));
        $this->assertCount(1, $builder->getSinks());

        // Sigue sin hacer nada aunque usemos pushSinks().
        $builder->pushSinks(collect([new TestSink('My Sink')]));
        $this->assertCount(1, $builder->getSinks());

        // Pero permite añadir un sink con otro nombre.
        $builder->pushSink(new TestSink('My Other Sink'));
        $this->assertCount(2, $builder->getSinks());
    }

    /** @test */
    public function removes_sinks_by_name()
    {
        /** @var Client $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->pushSinks(collect([
                new TestSink('My Sink'),
                new TestSink('My Other Sink'),
            ]));
        $this->assertCount(2, $builder->getSinks());

        $builder->withoutSink('My Sink');
        $this->assertCount(1, $builder->getSinks());
        $this->assertEquals('My Other Sink', $builder->getSinks()->first()->getName());
    }

    /* ****** */
    /* ADDONS */
    /* ****** */

    /** @test */
    public function push_addon_method_adds_a_given_addon()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        /** @var BuilderAddon|MockInterface $mock_addon */
        $mock_addon = $this->createMock(BuilderAddon::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->pushAddon($mock_addon);

        $this->assertCount(1, $builder->getAddons());
        $this->assertEquals($mock_addon, $builder->getAddons()->first());
    }

    /** @test */
    public function unknown_methods_are_forwarded_to_addons()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'));
        $builder->pushAddon(new TestAddon($builder));

        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals(2, $builder->testMethod(1));
    }

    /** @test */
    public function with_json_data_method_adds_json_data_addon()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->withJsonData(['value' => true]);

        // Comprobamos que el Addon ha sido añadido...
        $this->assertCount(1, $builder->getAddons());
        /** @var JsonDataAddon $addon */
        $addon = $builder->getAddons()->first();
        $this->assertInstanceOf(JsonDataAddon::class, $addon);

        // Comprobamos que el addon contiene los datos apropiados.
        $this->assertSame(['value' => true], $addon->getJsonData());
    }

    /** @test */
    public function expects_json_method_adds_expects_json_addon()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->expectsJson(['a' => 'required'], function () {});

        // Comprobamos que el Addon ha sido añadido...
        $this->assertCount(1, $builder->getAddons());
        /** @var ExpectsJsonAddon $addon */
        $addon = $builder->getAddons()->first();
        $this->assertInstanceOf(ExpectsJsonAddon::class, $addon);

        // Comprobamos que el addon contiene los datos apropiados.
        $this->assertSame(['a' => 'required'], $addon->getValidationRules());
        $this->assertTrue($addon->hasValidatorCallback());
    }

    /** @test */
    public function unknown_method_calls_with_no_addon_implementation_throws_bad_method_exception()
    {
        /** @var Client|MockInterface $client_mock */
        $client_mock = $this->createMock(Client::class);

        // Añadimos un addon por darle mas salseo al tema.
        $builder = (new RequestBuilder($client_mock, 'My Label'));
        $builder->pushAddon(new TestAddon($builder));

        try {
            /** @noinspection PhpUndefinedMethodInspection */
            $builder->unknownMethod();
            $this->fail("Did not throw!");
        } catch (BadMethodCallException $bmce) {}

        $this->expectNotToPerformAssertions();
    }

    /* ******** */
    /* REQUESTS */
    /* ******** */

    /** @test */
    public function execute_async_without_extras_returns_promise()
    {
        $promise = new Promise();

        $guzzle_client_mock = $this->mock(GuzzleClient::class, function (MockInterface $mock) use ($promise) {
            $mock
                ->shouldReceive('requestAsync')
                ->withArgs(function (string $in_method, string $in_uri, array $in_options) {
                    if ($in_method !== 'GET') {
                        return false;
                    }

                    if ($in_uri !== '') {
                        return false;
                    }

                    if (count($in_options['headers']) !== 1) {
                        return false;
                    }
                    if (count($in_options['headers']['User-Agent']) !== 1) {
                        return false;
                    }
                    if ($in_options['headers']['User-Agent'][0] !== 'Beat/HttpClient') {
                        return false;
                    }

                    if ($in_options['connect_timeout'] !== 10) {
                        return false;
                    }
                    if ($in_options['timeout'] !== 20) {
                        return false;
                    }

                    if (!($in_options['beat'] instanceof RequestOptions)) {
                        return false;
                    }

                    $requestOptions = $in_options['beat'];

                    return $requestOptions->getLabel() === 'My Label'
                        && $requestOptions->getBuilderAddons()->isEmpty()
                        && $requestOptions->getSinks()->isEmpty();
                })
                ->once()
                ->andReturn($promise);
        });

        /** @var Client $client_mock */
        $client_mock = $this->mock(Client::class, function (MockInterface $mock) use ($guzzle_client_mock, $promise) {
            $mock
                ->shouldReceive('getInternalClient')
                ->once()
                ->andReturn($guzzle_client_mock);
        });

        $builder = new RequestBuilder($client_mock, 'My Label');

        $result = $builder->executeAsync();

        $this->assertEquals($promise, $result);
    }

    /** @test */
    public function execute_async_with_custom_parameters_and_addons_returns_promise()
    {
        $promise = new Promise();

        $guzzle_client_mock = $this->mock(GuzzleClient::class, function (MockInterface $mock) use ($promise) {
            $mock
                ->shouldReceive('requestAsync')
                ->withArgs(function (string $in_method, string $in_uri, array $in_options) {
                    if ($in_method !== 'POST') {
                        return false;
                    }

                    if ($in_uri !== 'http://localhost/') {
                        return false;
                    }

                    if (count($in_options['headers']) !== 1) {
                        return false;
                    }
                    if (count($in_options['headers']['User-Agent']) !== 1) {
                        return false;
                    }
                    if ($in_options['headers']['User-Agent'][0] !== 'My/Client') {
                        return false;
                    }

                    if ($in_options['connect_timeout'] !== 25) {
                        return false;
                    }
                    if ($in_options['timeout'] !== 35) {
                        return false;
                    }

                    if (!($in_options['beat'] instanceof RequestOptions)) {
                        return false;
                    }

                    $requestOptions = $in_options['beat'];

                    return $requestOptions->getLabel() === 'My Label'
                        && $requestOptions->getBuilderAddons()->isNotEmpty()
                        && $requestOptions->getSinks()->isNotEmpty();
                })
                ->once()
                ->andReturn($promise);
        });

        /** @var Client $client_mock */
        $client_mock = $this->mock(Client::class, function (MockInterface $mock) use ($guzzle_client_mock, $promise) {
            $mock
                ->shouldReceive('getInternalClient')
                ->once()
                ->andReturn($guzzle_client_mock);
        });

        $builder = (new RequestBuilder($client_mock, 'My Label'))
            ->to('POST', 'http://localhost/')
            ->userAgent('My/Client')
            ->setTimeouts(25, 35);
        $result = $builder
            ->pushAddon(new TestAddon($builder))
            ->pushSink(new TestSink('MySink'))
            ->executeAsync();

        $this->assertEquals($promise, $result);

        // No veo forma de comprobar las tripas de la promesa para ver si tiene nuevos eslabones.
    }

    /** @test */
    public function execute_calls_execute_async_and_unwraps_the_resulting_promise()
    {
        $promise_mock = Mockery::mock(PromiseInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('wait')
                ->withArgs([])
                ->once()
                ->andReturn(new Response(
                    new RequestOptions(),
                    new Request('POST', 'http://localhost/'),
                    new \GuzzleHttp\Psr7\Response(),
                    0.1
                ));
        });

        /** @var RequestBuilder $mock */
        $mock = $this->partialMock(RequestBuilder::class, function (MockInterface $mock) use ($promise_mock) {
            $mock->shouldAllowMockingProtectedMethods();

            $mock
                ->shouldReceive('transfer')
                ->withArgs(function (array $options) {
                    return empty($options);
                })
                ->once()
                ->andReturn($promise_mock);

            $mock
                ->shouldReceive('build_guzzle_options')
                ->withArgs([[GuzzleRequestOptions::SYNCHRONOUS => true]])
                ->once()
                ->andReturn([]);
        });

        $mock->execute();
    }
}
