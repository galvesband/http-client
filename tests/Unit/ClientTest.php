<?php

namespace Beat\HttpClient\Tests\Unit;

use Beat\HttpClient\Client;
use Beat\HttpClient\Contracts\SealProvider;
use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use Beat\HttpClient\Tests\Misc\TestSink;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Client as GuzzleClient;
use Mockery\MockInterface;

class ClientTest extends TestCase
{
    /** @test */
    public function getters_behave_as_expected()
    {
        /** @var GuzzleClient|MockInterface $internal_client_mock */
        $internal_client_mock  = $this->createMock(GuzzleClient::class);
        /** @var SealProvider|MockInterface $sealer */
        $sealer                = $this->createMock(SealProvider::class);
        $profile_configuration = new ProfileConfiguration();
        $profile_configuration->setSealer($sealer);

        $client = new Client(
            'MyProfile',
            $internal_client_mock,
            $profile_configuration
        );

        $this->assertEquals('MyProfile', $client->getProfileName());
        $this->assertEquals($sealer, $client->getSealer());
        $this->assertEquals($profile_configuration, $client->getConfiguration());
        $this->assertEquals($internal_client_mock, $client->getInternalClient());
    }

    /** @test */
    public function produces_request_builders_correctly_configured()
    {
        /** @var GuzzleClient|MockInterface $internal_client_mock */
        $internal_client_mock  = $this->createMock(GuzzleClient::class);
        $profile_configuration = new ProfileConfiguration();
        $profile_configuration->getSinks()->push(new TestSink('MySink'));
        /** @var SealProvider|MockInterface $sealer */
        $sealer = $this->createMock(SealProvider::class);

        $client = new Client(
            'MyProfile',
            $internal_client_mock,
            $profile_configuration,
            $sealer
        );

        $request_builder = $client->newRequest('TestRequest');

        $this->assertEquals('TestRequest', $request_builder->getLabel());
        $this->assertEquals('GET', $request_builder->getMethod());
        $this->assertEquals('', $request_builder->getUri());
        $this->assertEmpty($request_builder->getHeaders());
        $this->assertEquals($profile_configuration->getConnectTimeout(), $request_builder->getConnectTimeout());
        $this->assertEquals($profile_configuration->getTimeout(), $request_builder->getTimeout());
        $this->assertEquals(ProfileConfiguration::DEFAULT_USER_AGENT, $request_builder->getUserAgent());
        $this->assertEmpty($request_builder->getAddons());
        $this->assertCount(1, $request_builder->getSinks());
    }
}
