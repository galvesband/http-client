<?php

namespace Beat\HttpClient\Tests\Unit\DataTransferObjects;

use Beat\HttpClient\Contracts\SealProvider;
use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use Beat\HttpClient\Tests\TestCase;

/**
 * Estos tests existen, básicamente, para subir el code-coverage de la suite de tests.
 */
class ProfileConfigurationTest extends TestCase
{
    /** @test */
    public function check_some_setters()
    {
        $config = new ProfileConfiguration();

        $config->setBaseUri('http://localhost:8000');
        $this->assertEquals('http://localhost:8000', $config->getBaseUri());

        $config->setConnectTimeout(5);
        $this->assertEquals(5, $config->getConnectTimeout());

        $config->setTimeout(40);
        $this->assertEquals(40, $config->getTimeout());

        $config->setUserAgent('MyThing 1.0');
        $this->assertEquals('MyThing 1.0', $config->getUserAgent());

        $sealMock = \Mockery::mock(SealProvider::class);
        $config->setSealer($sealMock);
        $this->assertEquals($sealMock, $config->getSealer());
    }
}