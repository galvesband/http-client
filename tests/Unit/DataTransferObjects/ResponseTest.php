<?php

namespace Beat\HttpClient\Tests\Unit\DataTransferObjects;

use BadMethodCallException;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\ResponseAddons\JsonDataAddon;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\ResponseInterface;

class ResponseTest extends TestCase
{
    /** @test */
    public function pass_unknown_calls_to_addons_that_implement_the_unknown_method()
    {
        $internal_response = new GuzzleResponse(
            200,
            ['Content-Type' => ['application/json']],
            '{"data":{"name":"Miles"}}'
        );
        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/'),
            $internal_response,
            0.01
        );
        /** @noinspection PhpUnhandledExceptionInspection */
        $response->pushAddon(new JsonDataAddon($response));

        // getJsonData() no existe en Response, pero sí en JsonDataAddon().
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertSame(['data' => ['name' => 'Miles']], $response->getJsonData());
    }

    /** @test */
    public function throws_bad_method_call_exception_if_unknown_call_not_implemented_by_any_addon()
    {
        $internal_response = new GuzzleResponse(
            200,
            ['Content-Type' => ['application/json']],
            '{"data":{"name":"Miles"}}'
        );
        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/'),
            $internal_response,
            0.01
        );
        /** @noinspection PhpUnhandledExceptionInspection */
        $response->pushAddon(new JsonDataAddon($response));

        try {
            /** @noinspection PhpUndefinedMethodInspection */
            $response->whatever();
            $this->fail("Did not throw!");
        } catch (BadMethodCallException $bmce) {
            $this->assertStringContainsString("whatever", $bmce->getMessage());
        }
    }

    /** @test */
    public function extracts_content_type_from_internal_response_headers()
    {
        $internal_response = new GuzzleResponse(
            200,
            ['Content-Type' => ['application/json']],
            '{"data":{"name":"Miles"}}'
        );
        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/'),
            $internal_response,
            0.01
        );

        $this->assertEquals('application/json', $response->getContentType());
        /** @var Response $response2 */
        $response2 = $response->withoutHeader('Content-Type');
        $this->assertEmpty($response2->getContentType());
    }

    /** @test */
    public function implements_response_interface_by_decorating_internal_response()
    {
        $internal_response = new GuzzleResponse(
            200,
            ['Content-Type' => ['application/json']],
            '{"data":{"name":"Miles"}}'
        );
        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/'),
            $internal_response,
            0.01
        );

        $this->assertEquals(1.1, $response->getProtocolVersion());
        $response2 = $response->withProtocolVersion(1.0);
        $this->assertNotEquals($response2, $response);
        $this->assertEquals(1.0, $response2->getProtocolVersion());

        $this->assertEquals(['Content-Type' => ['application/json']], $response2->getHeaders());
        $this->assertTrue($response2->hasHeader('Content-Type'));
        $this->assertSame(['application/json'], $response2->getHeader('Content-Type'));
        $this->assertEquals('application/json', $response2->getHeaderLine('Content-Type'));
        $response3 = $response2->withHeader('Host', 'localhost');
        $this->assertNotEquals($response3, $response2);
        $this->assertEquals('localhost', $response3->getHeaderLine('Host'));
        $response4 = $response3->withAddedHeader('Host', '127.0.0.1');
        $this->assertNotEquals($response4, $response3);
        $this->assertEquals('localhost, 127.0.0.1', $response4->getHeaderLine('Host'));
        $response5 = $response4->withoutHeader('Host');
        $this->assertNotEquals($response5, $response4);
        $this->assertFalse($response5->hasHeader('Host'));

        $this->assertEquals('{"data":{"name":"Miles"}}', (string) $response5->getBody());
        /** @var ResponseInterface $response6 */
        $response6 = $response5->withBody(Utils::streamFor('{"a":"b"}'));
        $this->assertNotEquals($response6, $response5);
        $this->assertEquals('{"a":"b"}', (string) $response6->getBody());

        $this->assertEquals(200, $response6->getStatusCode());
        $response7 = $response6->withStatus(201);
        $this->assertNotEquals($response7, $response6);
        $this->assertEquals(201, $response7->getStatusCode());

        $this->assertEquals('Created', $response7->getReasonPhrase());
    }
}