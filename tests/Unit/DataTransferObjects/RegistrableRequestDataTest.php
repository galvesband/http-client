<?php

namespace Beat\HttpClient\Tests\Unit\DataTransferObjects;

use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\Tests\TestCase;

/**
 * Estos tests existen, básicamente, para subir el code-coverage de la suite de tests.
 */
class RegistrableRequestDataTest extends TestCase
{
    /** @test */
    public function check_some_setters()
    {
        $subject = new RegistrableRequestData(
            'PUT',
            'http://localhost:8000/users/miles',
            ['Content-Type' => ['application/json']],
            '{"data":{"name":"Miles"}}'
        );

        $subject->setMethod('POST');
        $this->assertEquals('POST', $subject->getMethod());

        $subject->setUrl('http://localhost:9000/');
        $this->assertEquals('http://localhost:9000/', $subject->getUrl());

        $timestamp = now()->subSecond();
        $subject->setTimestamp(now()->subSecond());
        $this->assertNotEquals($timestamp, $subject->getTimestamp());
        $this->assertEquals($timestamp->format('Y-m-d H:i:s'), $subject->getTimestamp()->format('Y-m-d H:i:s'));

        $subject->setHeaders(['Some' => ['Header']]);
        $this->assertSame(['Some' => ['Header']], $subject->getHeaders());

        $subject->setContent('Hi there');
        $this->assertEquals('Hi there', $subject->getContent());
    }
}