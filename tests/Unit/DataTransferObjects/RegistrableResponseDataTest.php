<?php

namespace Beat\HttpClient\Tests\Unit\DataTransferObjects;

use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\Tests\TestCase;

/**
 * Estos tests existen, básicamente, para subir el code-coverage de la suite de tests.
 */
class RegistrableResponseDataTest extends TestCase
{
    /** @test */
    public function check_some_setters()
    {
        $subject = new RegistrableResponseData(
            200, 'Duh', ['Content-Type' => ['application/duh']], 0.5
        );

        $subject->setStatus(201);
        $this->assertEquals(201, $subject->getStatus());

        $subject->setTimeInSeconds(0.1);
        $this->assertEquals(0.1, $subject->getTimeInSeconds());
    }

    /** @test */
    public function truncates_time_in_seconds_below_0_to_0()
    {
        $subject = new RegistrableResponseData(
            200, 'Duh', ['Content-Type' => ['application/duh']], -0.5
        );

        $this->assertEquals(0, $subject->getTimeInSeconds());
    }

    /** @test */
    public function truncates_time_in_seconds_above_999999_to_999999()
    {
        $subject = new RegistrableResponseData(
            200, 'Duh', ['Content-Type' => ['application/duh']], 99999999.9
        );

        $this->assertEquals(999999, $subject->getTimeInSeconds());
    }
}