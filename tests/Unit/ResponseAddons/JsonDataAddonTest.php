<?php

namespace Beat\HttpClient\Tests\Unit\ResponseAddons;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\InvalidJsonException;
use Beat\HttpClient\Exceptions\ResponseValidationException;
use Beat\HttpClient\ResponseAddons\JsonDataAddon;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

class JsonDataAddonTest extends TestCase
{
    /** @test */
    public function throws_exception_at_constructor_time_if_response_is_not_a_valid_json()
    {
        $response = new Response(
            new RequestOptions(),
            new Request('POST', 'http://localhost:8000/'),
            new GuzzleResponse(200, [], 'not-a-json'),
            .2
        );

        try {
            new JsonDataAddon($response);
            $this->fail("Did not throw!");
        } catch (InvalidJsonException $e) {
            $this->expectNotToPerformAssertions();
        }
    }

    /** @test */
    public function decodes_response()
    {
        $response = new Response(
            new RequestOptions(),
            new Request('POST', 'http://localhost:8000/'),
            new GuzzleResponse(200, [], '{"data":true}'),
            .2
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $addon = new JsonDataAddon($response);
        $this->assertSame(['data' => true], $addon->getJsonData());
    }

    /** @test */
    public function validates_when_validate_data_method_is_used()
    {
        $response = new Response(
            new RequestOptions(),
            new Request('POST', 'http://localhost:8000/'),
            new GuzzleResponse(200, [], '{"data":true}'),
            .2
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $addon = new JsonDataAddon($response);
        /** @noinspection PhpUnhandledExceptionInspection */
        $addon->validateJsonData(['data' => 'required|boolean']);

        $this->expectNotToPerformAssertions();
    }

    /** @test */
    public function validates_and_throws_if_data_is_not_valid()
    {
        $response = new Response(
            new RequestOptions(),
            new Request('POST', 'http://localhost:8000/'),
            new GuzzleResponse(200, [], '{"data":null}'),
            .2
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $addon = new JsonDataAddon($response);
        try {
            $addon->validateJsonData(['data' => 'required|boolean'], null, ['data.required' => 'Obligatorio']);
            $this->fail("Did not throw!");
        } catch (ResponseValidationException $e) {
            $this->assertSame(['data' => ['Obligatorio']], $e->getValidationErrors());
        }
    }
}