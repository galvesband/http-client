<?php

namespace Beat\HttpClient\Tests\Unit\DataExtractors;

use Beat\HttpClient\ContentExtractors\SimpleContentExtractor;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\MessageInterface;

class SimpleContentExtractorTest extends TestCase
{
    protected function buildMessage(string $contentType = 'application/json'): MessageInterface
    {
        $headers = !empty($contentType)
            ? array_filter(['Content-Type' => [$contentType]])
            : [];

        return new Request(
            'GET', 'http://localhost/', $headers,
            '{"data":[{"name":"Joseph"}]}'
        );
    }

    /** @test */
    public function extracts_whole_message_content_of_known_mimes_below_size_threshold()
    {
        $message = $this->buildMessage();
        $sizeThreshold = $message->getBody()->getSize();

        $extractor = new SimpleContentExtractor([
            'application/json',
        ], false, $sizeThreshold + 1);
        $extracted = $extractor->extractContent($message);

        $this->assertEquals((string) $message->getBody(), $extracted);
    }

    /** @test */
    public function extracts_truncated_message_content_of_known_mimes_over_size_threshold()
    {
        $message = $this->buildMessage();

        $extractor = new SimpleContentExtractor([
            'application/json',
        ], false, 5);
        $extracted = $extractor->extractContent($message);

        $this->assertEquals(substr((string) $message->getBody(), 0, 5), $extracted);
    }

    /** @test */
    public function skips_extraction_of_message_if_content_type_is_not_allowed()
    {
        $message = $this->buildMessage();

        $extractor = new SimpleContentExtractor([], false, 1024);
        $extracted = $extractor->extractContent($message);

        $this->assertEquals('-- Skipped content of type \'application/json\' weighting 0.027 KiB --', $extracted);
    }

    /** @test */
    public function skips_extraction_of_message_content_when_no_content_type_and_property_says_to()
    {
        $message = $this->buildMessage('');

        $extractor = new SimpleContentExtractor(['application/json'], false, 1024);
        $extracted = $extractor->extractContent($message);

        $this->assertEquals('-- Skipped content of type \'\' weighting 0.027 KiB --', $extracted);
    }

    /** @test */
    public function extracts_content_when_no_content_type_and_property_says_to()
    {
        $message = $this->buildMessage('');

        $extractor = new SimpleContentExtractor(['application/json'], true, 1024);
        $extracted = $extractor->extractContent($message);

        $this->assertEquals('{"data":[{"name":"Joseph"}]}', $extracted);
    }

    /* ********************************** */
    /* TESTS CHORRAS DE GETTERS y SETTERS */
    /* ********************************** */

    /** @test */
    public function setter_for_allowed_content_types_works_as_expected()
    {
        $extractor = new SimpleContentExtractor(
            ['application/json'], true, 64
        );
        $this->assertSame(['application/json'], $extractor->getAllowedContentTypes());

        $extractor->setAllowedContentTypes(['text/csv', 'application/json']);
        $this->assertSame(['text/csv', 'application/json'], $extractor->getAllowedContentTypes());
    }

    /** @test */
    public function setter_for_allow_empty_content_type_works_as_expected()
    {
        $extractor = new SimpleContentExtractor(
            ['application/json'], true, 64
        );
        $this->assertTrue($extractor->allowEmptyContentType());

        $extractor->setAllowEmptyContentType(false);
        $this->assertFalse($extractor->allowEmptyContentType());
    }

    /** @test */
    public function setter_for_size_threshold_works_as_expected()
    {
        $extractor = new SimpleContentExtractor(
            ['application/json'], true, 64
        );
        $this->assertEquals(64, $extractor->getSizeThreshold());

        $extractor->setSizeThreshold(1024);
        $this->assertEquals(1024, $extractor->getSizeThreshold());
    }
}
