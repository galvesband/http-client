<?php

namespace Beat\HttpClient\Tests\Unit\DataExtractors;

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;

class NoContentExtractorTest extends TestCase
{
    /** @test */
    public function builds_summary_of_content()
    {
        $message = new Request(
            'GET', 'http://localhost/', ['Content-Type' => ['application/json']],
            '{"data":[{"name":"Joseph"}]}'
        );
        $extractor = new NoContentExtractor();

        $this->assertEquals('-- Skipped content of type \'application/json\' weighting 0.027 KiB --', $extractor->extractContent($message));
    }

    /** @test */
    public function if_content_is_empty_then_extracted_content_is_empty()
    {
        $message = new Request('GET', 'http://localhost/', [], '');
        $extractor = new NoContentExtractor();

        $this->assertEquals('', $extractor->extractContent($message));
    }
}
