<?php

namespace Beat\HttpClient\Tests\Unit\Testing;

use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Tests\TestCase;
use Psr\Http\Message\RequestInterface;

class RequestExpectationBuilderTest extends TestCase
{
    /**
     * El constructor de expectativas de peticiones depende de que se ejecute su destructor automáticamente,
     * cuando ninguna variable apunte a él y tal. Pero es posible que haya referencias internas a sí mismo
     * que hagan que el colector de basura no lo detecte como reciclable. Por ejemplo, un Closure que
     * usa al mismo RequestExpectationBuilder como $this.
     *
     * Este test debe comprobar que el destructor del builder añade su expectativa al ClientFactoryFake
     * si se usa de forma normal, sin guardarlo en variables o cosas así.
     *
     * @test
     */
    public function no_internal_references_prevent_destructor_to_be_called()
    {
        HttpClient::fake();
        HttpClient::onProfile('default')
            ->expectRequest(function (RequestInterface $request, array $options) {
                return $request->getMethod() === 'GET'
                    && ((string) $request->getUri()) === 'http://localhost';
            })
            ->once()
            ->resultingInResponse(202, 'Hi there');

        $response = HttpClient::profile('default')->newRequest('Checking')
            ->to('GET', 'http://localhost')
            ->execute();

        $this->assertEquals(202, $response->getStatusCode());
        $this->assertEquals('Hi there', (string) $response->getBody());
    }
}