<?php

namespace Beat\HttpClient\Tests\Unit\Factories;

use Beat\HttpClient\ContentExtractors\SimpleContentExtractor;
use Beat\HttpClient\Factories\SinkFactory;
use Beat\HttpClient\Tests\Misc\TestSink;
use Beat\HttpClient\Tests\TestCase;
use RuntimeException;

class SinkFactoryTest extends TestCase
{
    protected function buildDataExtractorSimpleConfiguration(
        bool  $enabled               = true,
        array $allowedContentTypes   = ['application/json'],
        bool  $allowEmptyContentType = true,
        int   $sizeThreshold         = 1024 * 1024
    ): array
    {
        return compact(['enabled', 'allowedContentTypes', 'allowEmptyContentType', 'sizeThreshold']);
    }

    /** @test */
    public function throws_if_sink_name_is_not_found_in_configuration()
    {
        $configuration = [];
        $factory = new SinkFactory($configuration);

        $this->expectException(RuntimeException::class);
        $factory->buildSink('lerele');
    }

    /** @test */
    public function builds_sink_with_correctly_configured_content_extractors()
    {
        $configuration = [
            'MySink' => [
                'class' => TestSink::class,
                'params' => [
                    'register_successful_requests' => true,
                    'register_failed_requests'     => true,
                ],

                'request_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => $this->buildDataExtractorSimpleConfiguration(
                        false, [], false, 64
                    ),
                ],
                'response_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => $this->buildDataExtractorSimpleConfiguration(),
                ],
            ],
        ];

        $factory = new SinkFactory($configuration);
        /** @var TestSink $sink */
        $sink = $factory->buildSink('MySink');

        $this->assertEquals('MySink', $sink->getName());
        $this->assertTrue($sink->registersSuccessfulRequests());
        $this->assertTrue($sink->registersFailedRequests());

        $this->assertInstanceOf(SimpleContentExtractor::class, $sink->getRequestContentExtractor());
        /** @var SimpleContentExtractor $request_extractor */
        $request_extractor = $sink->getRequestContentExtractor();
        $this->assertEmpty($request_extractor->getAllowedContentTypes());
        $this->assertFalse($request_extractor->allowEmptyContentType());
        $this->assertEquals(64, $request_extractor->getSizeThreshold());

        $this->assertInstanceOf(SimpleContentExtractor::class, $sink->getResponseContentExtractor());
        /** @var SimpleContentExtractor $response_extractor */
        $response_extractor = $sink->getResponseContentExtractor();
        $this->assertSame(['application/json'], $response_extractor->getAllowedContentTypes());
        $this->assertTrue($response_extractor->allowEmptyContentType());
        $this->assertEquals(1024 * 1024, $response_extractor->getSizeThreshold());
    }
}
