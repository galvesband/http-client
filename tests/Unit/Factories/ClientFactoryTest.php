<?php

namespace Beat\HttpClient\Tests\Unit\Factories;

use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\Factories\SinkFactory;
use Beat\HttpClient\Sealers\BeatExternalTokenSealer;
use Beat\HttpClient\Tests\Misc\TestSink;
use Beat\HttpClient\Tests\TestCase;
use Mockery;
use Mockery\MockInterface;
use RuntimeException;

class ClientFactoryTest extends TestCase
{
    const PROFILE_DEFAULT = [
        'base_uri'          => '',
        'connect_timeout'   => 5,
        'timeout'           => 10,
        'user_agent'        => 'Beat/HttpClient',
        'seal'              => null,
        'sinks'             => [],
    ];

    protected function build_default_configuration(
        ?array  $default_profile_configuration    = null,
        ?string $additional_profile_name          = null,
        ?array  $additional_profile_configuration = null,
        ?string $additional_sink_name             = null,
        ?array  $additional_sink_configuration    = null,
        ?string $additional_sealer_name           = null,
        ?array  $additional_sealer_configuration  = null
    ): array
    {
        $retVal = [
            'profile_defaults' => $default_profile_configuration ?? static::PROFILE_DEFAULT,
            'seal_providers'   => [],
            'profiles'         => [
                'default'      => [],
            ],
            'sinks'            => [],
        ];

        if (!is_null($additional_profile_name)) {
            $retVal['profiles'][$additional_profile_name] = $additional_profile_configuration;
        }

        if (!is_null($additional_sink_name)) {
            $retVal['sinks'][$additional_sink_name] = $additional_sink_configuration;
        }

        if (!is_null($additional_sealer_name)) {
            $retVal['seal_providers'][$additional_sealer_name] = $additional_sealer_configuration;
        }

        return $retVal;
    }

    /** @test */
    public function empty_default_configurations_creates_a_default_profile()
    {
        /** @var SinkFactory $sinkFactoryMock */
        $sinkFactoryMock = Mockery::mock(SinkFactory::class);
        $factory = new ClientFactory($this->build_default_configuration(), $sinkFactoryMock);
        $client  = $factory->profile('default');

        $this->assertEquals('default', $client->getProfileName());
        $this->assertNull($client->getSealer());

        // Comprobar configuración de cliente
        $clientConfiguration = $client->getConfiguration();
        $this->assertEquals("", $clientConfiguration->getBaseUri());
        $this->assertEquals(5, $clientConfiguration->getConnectTimeout());
        $this->assertEquals(10, $clientConfiguration->getTimeout());
        $this->assertNull($clientConfiguration->getSealer());
        $this->assertEquals('Beat/HttpClient', $clientConfiguration->getUserAgent());
        $this->assertEmpty($clientConfiguration->getSinks());

        // No podemos comprobar el cliente interno, Guzzle da pocas opciones al respecto
        // (y las pocas que da van a ser eliminadas en GuzzleHttp 8.0).
    }

    /**
     * Base conform test. The factory is able to create a basic client without a sealer.
     * @test
     */
    public function creates_requested_profile()
    {
        /** @var SinkFactory $sinkFactoryMock */
        $sinkFactoryMock = Mockery::mock(SinkFactory::class);
        $factory = new ClientFactory($this->build_default_configuration(
            null,
            'my_client',
            [
                'base_uri'          => 'http://localhost:8000/project/',
                'connect_timeout'   => 30,
                'timeout'           => 40,
                'register_requests' => true,
                'log_requests'      => true,
                'log_errors'        => true,
                'queue'             => 'my_queue',
                'log_channel'       => 'my_log_channel',
                'user_agent'        => 'my project 1.0',
                'seal'              => null,
            ]
        ), $sinkFactoryMock);
        $client = $factory->profile('my_client');

        $this->assertEquals('my_client', $client->getProfileName());
        $this->assertNull($client->getSealer());

        // Comprobar configuración de cliente
        $clientConfiguration = $client->getConfiguration();
        $this->assertEquals("http://localhost:8000/project/", $clientConfiguration->getBaseUri());
        $this->assertEquals(30, $clientConfiguration->getConnectTimeout());
        $this->assertEquals(40, $clientConfiguration->getTimeout());
        $this->assertNull($clientConfiguration->getSealer());
        $this->assertEquals('my project 1.0', $clientConfiguration->getUserAgent());
        $this->assertEmpty($clientConfiguration->getSinks());

        // No podemos comprobar el cliente interno, Guzzle da pocas opciones al respecto
        // (y las pocas que da van a ser eliminadas en GuzzleHttp 8.0).
    }

    /** @test */
    public function specific_profile_configuration_overrides_default_configuration()
    {
        /** @var SinkFactory $sinkFactory */
        $sinkFactory = Mockery::mock(SinkFactory::class);
        $factory = new ClientFactory($this->build_default_configuration(
            null,
            'my_client',
            ['base_uri' => 'http://localhost:8000/project/']
        ), $sinkFactory);
        $client = $factory->profile('my_client');

        $this->assertEquals('my_client', $client->getProfileName());
        $this->assertNull($client->getSealer());

        // Comprobar configuración de cliente
        $clientConfiguration = $client->getConfiguration();
        $this->assertEquals("http://localhost:8000/project/", $clientConfiguration->getBaseUri());
        $this->assertEquals(5, $clientConfiguration->getConnectTimeout());
        $this->assertEquals(10, $clientConfiguration->getTimeout());
        $this->assertEquals('Beat/HttpClient', $clientConfiguration->getUserAgent());
        $this->assertEmpty($clientConfiguration->getSinks());
        $this->assertNull($clientConfiguration->getSealer());

        // No podemos comprobar el cliente interno, Guzzle da pocas opciones al respecto
        // (y las pocas que da van a ser eliminadas en GuzzleHttp 8.0).
    }

    /** @test */
    public function throws_exception_when_profile_configuration_is_not_found()
    {
        /** @var SinkFactory $sinkFactory */
        $sinkFactory = Mockery::mock(SinkFactory::class);
        $factory = new ClientFactory($this->build_default_configuration(), $sinkFactory);

        $this->expectException(RuntimeException::class);
        $factory->profile('my_client');
    }

    /** @test */
    public function calls_default_profile_when_unknown_methods_are_invoked()
    {
        /** @var SinkFactory $sinkFactory */
        $sinkFactory = Mockery::mock(SinkFactory::class);
        $factory = new ClientFactory($this->build_default_configuration(), $sinkFactory);

        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertEquals('default', $factory->getProfileName());
    }

    /** @test */
    public function created_clients_have_our_guzzle_middleware()
    {
        // Tb habría que comprobar si el cliente creado tiene las opciones correctas (base_uri, ...)
        $this->markTestIncomplete("Pendiente de averiguar cómo testear esto.");
    }

    /** @test */
    public function builds_profile_with_configured_sealer()
    {
        $conf = $this->build_default_configuration(
            null,
            'SealedProfile',
            ['seal' => 'beat'],
            null, null,
            'beat',
            [
                'class' => BeatExternalTokenSealer::class,
                'params' => ['passkey' => 'hi']
            ]
        );

        $sinkFactory = new SinkFactory($conf['sinks']);
        $factory     = new ClientFactory($conf, $sinkFactory);
        $client      = $factory->profile('SealedProfile');

        $this->assertInstanceOf(BeatExternalTokenSealer::class, $client->getConfiguration()->getSealer());
    }

    /** @test */
    public function builds_profiles_using_sink_factory()
    {
        // Preparamos la configuración.
        $sink_config = [
            'class'  => TestSink::class,
            'params' => [],
        ];
        $configuration = $this->build_default_configuration(
            null,
            'MyProfile',
            ['sinks' => ['MySink']],
            'MySink',
            $sink_config
        );

        // Preparamos un mock de la factoría de Sinks.
        $sinkFactoryMock = Mockery::mock(SinkFactory::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('buildSink')
                ->withArgs(['MySink'])
                ->once()
                ->andReturn(new TestSink('MySink'));
        });

        // Creamos una factoría de clientes con nuestra configuración y factoría de sinks.
        $factory = new ClientFactory($configuration, $sinkFactoryMock);

        $client = $factory->profile('MyProfile');

        // El mock de la factoría de sinks comprueba que se recibe la llamada
        // para crear el sink de pruebas y lo devuelve.
        // ¿Estará incluido el sink en la configuración del perfil del cliente devuelto?
        $this->assertCount(1, $client->getConfiguration()->getSinks());
        $this->assertInstanceOf(TestSink::class, $client->getConfiguration()->getSinks()->first());
    }

    /**
     * Demostramos que ClientFactory guarda las instancias de Cliente que genera cuando le piden un perfil por
     * primera vez, y que siguientes peticiones del mismo perfil devuelven el mismo cliente en lugar
     * de construir un cliente nuevo.
     *
     * @test
     */
    public function requesting_same_profile_more_than_once_returns_same_client_instance()
    {
        $conf = $this->build_default_configuration();
        $sinkFactory = new SinkFactory($conf['sinks']);
        $factory     = new ClientFactory($conf, $sinkFactory);
        $client1     = $factory->profile('default');
        $client2     = $factory->profile('default');

        $this->assertEquals($client1, $client2);
    }

    /** @test */
    public function requests_made_through_guzzle_raw_client_use_middleware_and_default_request_options()
    {
        $conf = $this->build_default_configuration();
        $sinkFactory = new SinkFactory($conf['sinks']);
        $factory     = new ClientFactory($conf, $sinkFactory);

        $guzzle_client = $factory->buildGuzzleClient('default');
        $this->markTestIncomplete("Pendiente de averiguar cómo testear esto.");
    }
}
