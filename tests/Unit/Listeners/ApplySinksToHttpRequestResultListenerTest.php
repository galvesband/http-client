<?php

namespace Beat\HttpClient\Tests\Unit\Listeners;

use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Events\FailedHttpRequestEvent;
use Beat\HttpClient\Events\SuccessfulHttpRequestEvent;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Listeners\ApplySinksToHttpRequestResultListener;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Mockery;
use Mockery\MockInterface;
use RuntimeException;

class ApplySinksToHttpRequestResultListenerTest extends TestCase
{
    /** @test */
    public function applies_sinks_to_successful_responses()
    {
        $sink_mock = Mockery::mock(Sink::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('processResult')
                ->withArgs(function (Response $response) {
                    return $response->getRequestOptions()->getLabel() === 'My Label';
                })
                ->once();
        });
        $requestOptions = new RequestOptions('My Label', collect([]), collect([$sink_mock]));

        $response = new Response(
            $requestOptions,
            new Request('POST', 'http://localhost:8000/', [], '{"name":"pepe"}'),
            new GuzzleResponse(200, [], '{"result":true}'),
            .2
        );

        $listener = new ApplySinksToHttpRequestResultListener();
        $listener->handleSuccessfulRequest(new SuccessfulHttpRequestEvent(
            $response, $requestOptions
        ));
    }

    /** @test */
    public function applies_sinks_to_failed_requests()
    {
        $sink_mock = Mockery::mock(Sink::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('processResult')
                ->withArgs(function (CommunicationException $exception) {
                    return $exception->getRequestOptions()->getLabel() === 'My Label';
                })
                ->once();
        });
        $requestOptions = new RequestOptions('My Label', collect([]), collect([$sink_mock]));

        $exception = new CommunicationException(
            new Request('POST', 'http://localhost:8000/', [], '{"name":"pepe"}'),
            $requestOptions, 0.2, new RuntimeException("BOOM")
        );
        $listener = new ApplySinksToHttpRequestResultListener();
        $listener->handleFailedRequest(new FailedHttpRequestEvent($exception, $requestOptions));
    }
}
