<?php

namespace Beat\HttpClient\Tests\Unit\Sinks;

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\ConnectionException;
use Beat\HttpClient\Exceptions\RequestException;
use Beat\HttpClient\Sinks\LogSink;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Support\Facades\Log;
use Mockery;
use Mockery\MockInterface;
use Psr\Http\Message\MessageInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LogSinkTest extends TestCase
{
    /* ******* */
    /* HELPERS */
    /* ******* */

    protected function build_sink(
        string                   $name              = 'Sink:Log',
        bool                     $registerSuccesses = true,
        bool                     $registerFails     = true,
        ?MessageContentExtractor $requestExtractor  = null,
        ?MessageContentExtractor $responseExtractor = null,
        string                   $log_channel       = 'default'
    ): LogSink
    {
        return new LogSink(
            $name, $registerSuccesses, $registerFails,
            $requestExtractor ?? new NoContentExtractor(),
            $responseExtractor ?? new NoContentExtractor(),
            $log_channel
        );
    }

    protected function buildLogFacadeMock(string $loggerName, LoggerInterface $logger): void
    {
        Log::shouldReceive('channel')
            ->withArgs([$loggerName])
            ->once()
            ->andReturn($logger);
    }

    protected function buildSuccessfulResponse(Sink $sink): Response
    {
        $request = new GuzzleRequest(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $response = new GuzzleResponse(
            200, ['Content-Type' => ['application/json']], '{"data":{"name":"Peter"}}'
        );
        $options = new RequestOptions('My Request', collect([]), collect([$sink]));

        return new Response(
            $options, $request, $response, 0.2
        );
    }

    protected function buildCommunicationException(Sink $sink): CommunicationException
    {
        $request = new GuzzleRequest(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $options = new RequestOptions('My Fail', collect([]), collect([$sink]));

        return new ConnectionException(
            $request, $options, 0.5, new ConnectException('Nobody is home?', $request)
        );
    }

    protected function buildRequestException(Sink $sink): RequestException
    {
        $request = new GuzzleRequest(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $options = new RequestOptions('My Fail', collect([]), collect([$sink]));
        $response = new GuzzleResponse(
            500, ['Content-Type' => ['application/json']], '{"message":"Boom"}}'
        );

        return new RequestException(
            $request, $response, null, $options, 0.1
        );
    }

    /* ***** */
    /* TESTS */
    /* ***** */

    /** @test */
    public function successful_requests_are_logged_as_info_on_configured_channel()
    {
        /** @var LoggerInterface $logger_mock */
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('info')
                ->withArgs(function ($message, $context) {
                    return $message === 'OK: My Request (POST http://localhost:8000/) answered 200 code in 0.2 s.'
                        && $context['request']['method']   === 'POST'
                        && $context['request']['uri']      === 'http://localhost:8000/'
                        && $context['request']['content']  === '-- Skipped content of type \'application/json\' weighting 0.016 KiB --'
                        && $context['response']['status']  === 200
                        && $context['response']['time_s']  === 0.2
                        && $context['response']['content'] === '-- Skipped content of type \'application/json\' weighting 0.024 KiB --';
                })
                ->once()
                ->andReturnNull();
        });
        $this->buildLogFacadeMock('MyChannel', $logger_mock);

        $sink = $this->build_sink(
            'Sink:Log', true, true,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildSuccessfulResponse($sink));
    }

    /** @test */
    public function failed_request_with_no_response_are_correctly_logged()
    {
        /** @var LoggerInterface $logger_mock */
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('warning')
                ->withArgs(function ($message, $context) {
                    return $message === 'ER: My Fail (POST http://localhost:8000/) failed with 0 code in 0.5 s.'
                        && $context['request']['method']   === 'POST'
                        && $context['request']['uri']      === 'http://localhost:8000/'
                        && $context['request']['content']  === '-- Skipped content of type \'application/json\' weighting 0.016 KiB --'
                        && $context['response']['status']  === 0
                        && $context['response']['time_s']  === 0.5
                        && $context['response']['content'] === '';
                })
                ->once()
                ->andReturnNull();
        });
        $this->buildLogFacadeMock('MyChannel', $logger_mock);

        $sink = $this->build_sink(
            'Sink:Log', true, true,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildCommunicationException($sink));
    }

    /** @test */
    public function failed_requests_are_logged_as_error_on_configured_channel()
    {
        /** @var LoggerInterface $logger_mock */
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('warning')
                ->withArgs(function ($message, $context) {
                    return $message === 'ER: My Fail (POST http://localhost:8000/) failed with 500 code in 0.1 s.'
                        && $context['request']['method']   === 'POST'
                        && $context['request']['uri']      === 'http://localhost:8000/'
                        && $context['request']['content']  === '-- Skipped content of type \'application/json\' weighting 0.016 KiB --'
                        && $context['response']['status']  === 500
                        && $context['response']['time_s']  === 0.1
                        && $context['response']['content'] === '-- Skipped content of type \'application/json\' weighting 0.019 KiB --';
                })
                ->once()
                ->andReturnNull();
        });
        $this->buildLogFacadeMock('MyChannel', $logger_mock);

        $sink = $this->build_sink(
            'Sink:Log', true, true,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildRequestException($sink));
    }

    /** @test */
    public function uses_given_extractors_to_extract_content_of_requests_and_responses()
    {
        /** @var LoggerInterface $logger_mock */
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('info')
                ->withArgs(function ($message, $context) {
                    return $message === 'OK: My Request (POST http://localhost:8000/) answered 200 code in 0.2 s.'
                        && $context['request']['method']   === 'POST'
                        && $context['request']['uri']      === 'http://localhost:8000/'
                        && $context['request']['content']  === 'Hi from RequestExtractor!'
                        && $context['response']['status']  === 200
                        && $context['response']['time_s']  === 0.2
                        && $context['response']['content'] === 'Hi from ResponseExtractor!';
                })
                ->once()
                ->andReturnNull();
        });
        $this->buildLogFacadeMock('MyChannel', $logger_mock);

        $requestExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $in_msg) {
                    return ((string) $in_msg->getBody()) === '{"name":"Peter"}';
                })
                ->andReturn('Hi from RequestExtractor!');
        });
        $responseExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $in_msg) {
                    return ((string) $in_msg->getBody()) === '{"data":{"name":"Peter"}}';
                })
                ->andReturn('Hi from ResponseExtractor!');
        });
        $sink = $this->build_sink(
            'MySink', true, true,
            $requestExtractor, $responseExtractor, 'MyChannel'
        );

        $sink->processResult($this->buildSuccessfulResponse($sink));
    }

    /** @test */
    public function ignores_successful_requests_when_configured_to_do_so()
    {
        Log::shouldReceive('channel')
            ->withArgs(['MyChannel'])
            ->never();

        $sink = $this->build_sink(
            'Sink:Log', false, true,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildSuccessfulResponse($sink));
    }

    /** @test */
    public function ignores_failed_requests_when_configured_to_not_log_successes_and_fails()
    {
        Log::shouldReceive('channel')
            ->withArgs(['MyChannel'])
            ->never();

        $sink = $this->build_sink(
            'Sink:Log', false, false,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildCommunicationException($sink));
    }

    /** @test */
    public function logs_failed_request_when_configured_to_log_successes_but_not_fails()
    {
        /** @var LoggerInterface $logger_mock */
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('warning')
                ->andReturnNull();
        });
        $this->buildLogFacadeMock('MyChannel', $logger_mock);

        $sink = $this->build_sink(
            'Sink:Log', true, false,
            null, null, 'MyChannel'
        );
        $sink->processResult($this->buildCommunicationException($sink));
    }

    /** @test */
    public function from_configuration_method_builds_sink_correctly()
    {
        $name = 'MySink';
        $params = ['log_channel' => 'some_channel'];
        $request_extractor = new NoContentExtractor();
        $response_extractor = new NoContentExtractor();

        /** @var LogSink $sink */
        $sink = LogSink::fromConfiguration(
            $name, $params, true, false,
            $request_extractor, $response_extractor
        );

        $this->assertEquals($name, $sink->getName());
        $this->assertEquals($params['log_channel'], $sink->getLogChannelName());
        $this->assertTrue($sink->registersSuccessfulRequests());
        $this->assertFalse($sink->registersFailedRequests());
        $this->assertEquals($request_extractor, $sink->getRequestContentExtractor());
        $this->assertEquals($response_extractor, $sink->getResponseContentExtractor());
    }

    /** @test */
    public function from_configuration_method_throws_if_log_channel_key_is_not_defined()
    {
        $name = 'MySink';
        $params = [];
        $request_extractor = new NoContentExtractor();
        $response_extractor = new NoContentExtractor();

        $this->expectException(RuntimeException::class);
        LogSink::fromConfiguration(
            $name, $params, true, false,
            $request_extractor, $response_extractor
        );
    }
}
