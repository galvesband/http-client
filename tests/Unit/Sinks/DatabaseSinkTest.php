<?php

namespace Beat\HttpClient\Tests\Unit\Sinks;

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\RequestException;
use Beat\HttpClient\Jobs\SaveHttpRequestResultInDatabaseJob;
use Beat\HttpClient\Sinks\DatabaseSink;
use Beat\HttpClient\Tests\Misc\FakeUser;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Queue;
use Mockery;
use Mockery\MockInterface;
use Psr\Http\Message\MessageInterface;
use RuntimeException;

class DatabaseSinkTest extends TestCase
{
    /* ******* */
    /* HELPERS */
    /* ******* */

    protected function buildSink(
        string                   $name              = 'Sink:DB',
        bool                     $registerSuccesses = true,
        bool                     $registerFails     = true,
        ?MessageContentExtractor $requestExtractor  = null,
        ?MessageContentExtractor $responseExtractor = null,
        string                   $queue             = 'default'
    ): DatabaseSink
    {
        return new DatabaseSink(
            $name, $registerSuccesses, $registerFails,
            $requestExtractor ?? new NoContentExtractor(),
            $responseExtractor ?? new NoContentExtractor(),
            $queue
        );
    }

    protected function buildSuccessfulResponse(Sink $sink): Response
    {
        $request = new GuzzleRequest(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $response = new GuzzleResponse(
            200, ['Content-Type' => ['application/json']], '{"data":{"name":"Peter"}}'
        );
        $options = new RequestOptions('My Request', collect([]), collect([$sink]));

        return new Response(
            $options, $request, $response, 0.2
        );
    }

    protected function buildFailedResponse(Sink $sink): RequestException
    {
        $request = new GuzzleRequest(
            'POST', 'http://localhost:8000/', [], '{"name":"Peter"}'
        );
        $response = new GuzzleResponse(
            500, [], 'BOOM'
        );
        $options = new RequestOptions('My Request', collect([]), collect([$sink]));

        return new RequestException(
            $request, $response, null, $options, 0.5
        );
    }

    /* ***** */
    /* TESTS */
    /* ***** */

    /** @test */
    public function dispatches_job_to_save_data_in_db()
    {
        Queue::fake();

        $sink = $this->buildSink();
        $sink->processResult($this->buildSuccessfulResponse($sink));

        Queue::assertPushed(function (SaveHttpRequestResultInDatabaseJob $job) {
            return $job->queue === 'default'
                && $job->requestData->getMethod()         === 'POST'
                && $job->requestData->getUrl()            === 'http://localhost:8000/'
                && $job->requestData->getContent()        === '-- Skipped content of type \'application/json\' weighting 0.016 KiB --'
                && $job->responseData->getStatus()        === 200
                && $job->responseData->getContent()       === '-- Skipped content of type \'application/json\' weighting 0.024 KiB --'
                && $job->responseData->getTimeInSeconds() === 0.2;
        });
    }

    /** @test */
    public function dispatches_job_to_save_in_db_with_authenticated_model()
    {
        Queue::fake();

        /** @noinspection PhpParamsInspection */
        Auth::login(FakeUser::newFactory()->create());

        $sink = $this->buildSink();
        $sink->processResult($this->buildSuccessfulResponse($sink));

        Queue::assertPushed(function (SaveHttpRequestResultInDatabaseJob $job) {
            /** @var FakeUser $user */
            $user = Auth::user();

            return $job->queue === 'default'
                && $job->requestData->getMethod() === 'POST'
                && $job->requestData->getUrl() === 'http://localhost:8000/'
                && $job->requestData->getContent() === '-- Skipped content of type \'application/json\' weighting 0.016 KiB --'
                && $job->responseData->getStatus() === 200
                && $job->responseData->getContent() === '-- Skipped content of type \'application/json\' weighting 0.024 KiB --'
                && $job->responseData->getTimeInSeconds() === 0.2
                && $job->authenticatable_class === FakeUser::class
                && $job->authenticatable_id === $user->id;
        });
    }

    /** @test */
    public function uses_given_extractors_to_extract_content_of_request_and_response()
    {
        Queue::fake();

        $mockRequestExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $request) {
                    return (string) $request->getBody() === '{"name":"Peter"}';
                })
                ->once()
                ->andReturn('{"name":"Peter"}');
        });
        $mockResponseExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $response) {
                    return (string) $response->getBody() === '{"data":{"name":"Peter"}}';
                })
                ->once()
                ->andReturn('{"data":{"name":"Peter"}}');
        });

        $sink = $this->buildSink(
            'Sink:DB', true, true,
            $mockRequestExtractor, $mockResponseExtractor, 'http_queue'
        );
        $response = $this->buildSuccessfulResponse($sink);

        $sink->processResult($response);

        Queue::assertPushed(function (SaveHttpRequestResultInDatabaseJob $job) {
            return $job->requestData->getContent() === '{"name":"Peter"}'
                && $job->responseData->getContent() === '{"data":{"name":"Peter"}}';
        });
    }

    /** @test */
    public function ignores_successes_when_configured_to_do_so()
    {
        Queue::fake();

        // No extractor will be called in the sink because we will configure it
        // to ignore successful requests.
        $mockRequestExtractor = Mockery::mock(
            MessageContentExtractor::class, function (MockInterface $mock) {}
        );
        $mockResponseExtractor = Mockery::mock(
            MessageContentExtractor::class, function (MockInterface $mock) {}
        );

        $sink = $this->buildSink(
            'Sink:DB', false, true,
            $mockRequestExtractor, $mockResponseExtractor, 'http_queue'
        );
        $response = $this->buildSuccessfulResponse($sink);

        $sink->processResult($response);
        Queue::assertNothingPushed();
    }

    /** @test */
    public function ignores_fails_when_configured_to_not_log_successes_and_fails()
    {
        Queue::fake();

        // No extractor will be called in the sink because we will configure it
        // to ignore successful requests.
        $mockRequestExtractor = Mockery::mock(
            MessageContentExtractor::class, function (MockInterface $mock) {}
        );
        $mockResponseExtractor = Mockery::mock(
            MessageContentExtractor::class, function (MockInterface $mock) {}
        );

        $sink = $this->buildSink(
            'Sink:DB', false, false,
            $mockRequestExtractor, $mockResponseExtractor, 'http_queue'
        );
        $response = $this->buildFailedResponse($sink);

        $sink->processResult($response);
        Queue::assertNothingPushed();
    }

    /**
     * Rationale: It doesn't make a lot of sense to log successes but not fails. So, if
     * configured to log successes, we will log fails unilaterally disregarding
     * the $register_failed_requests param in the sink.
     *
     * @test
     */
    public function processes_fails_when_configured_to_log_successes_but_not_fails()
    {
        Queue::fake();

        $mockRequestExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $request) {
                    return (string) $request->getBody() === '{"name":"Peter"}';
                })
                ->once()
                ->andReturn('{"name":"Peter"}');
        });
        $mockResponseExtractor = Mockery::mock(MessageContentExtractor::class, function (MockInterface $mock) {
            $mock
                ->shouldReceive('extractContent')
                ->withArgs(function (MessageInterface $response) {
                    return (string) $response->getBody() === 'BOOM';
                })
                ->once()
                ->andReturn('BOOM');
        });

        $sink = $this->buildSink(
            'Sink:DB', true, false,
            $mockRequestExtractor, $mockResponseExtractor, 'http_queue'
        );
        $response = $this->buildFailedResponse($sink);

        $sink->processResult($response);

        Queue::assertPushed(function (SaveHttpRequestResultInDatabaseJob $job) {
            return $job->requestData->getContent() === '{"name":"Peter"}'
                && $job->responseData->getContent() === 'BOOM';
        });
    }

    /** @test */
    public function from_configuration_static_method_builds_the_sink_correctly()
    {
        $name = 'MySink';
        $params = ['queue' => 'some_queue'];
        $request_extractor = new NoContentExtractor();
        $response_extractor = new NoContentExtractor();

        /** @var DatabaseSink $sink */
        $sink = DatabaseSink::fromConfiguration(
            $name, $params, false, true,
            $request_extractor, $response_extractor
        );

        $this->assertEquals($name, $sink->getName());
        $this->assertEquals($params['queue'], $sink->getQueueName());
        $this->assertFalse($sink->registersSuccessfulRequests());
        $this->assertTrue($sink->registersFailedRequests());
        $this->assertEquals($request_extractor, $sink->getRequestContentExtractor());
        $this->assertEquals($response_extractor, $sink->getResponseContentExtractor());
    }

    /** @test */
    public function from_configuration_throws_exception_if_queue_key_is_not_defined()
    {
        $name = 'MySink';
        $params = [];
        $request_extractor = new NoContentExtractor();
        $response_extractor = new NoContentExtractor();

        $this->expectException(RuntimeException::class);
        DatabaseSink::fromConfiguration(
            $name, $params, false, true,
            $request_extractor, $response_extractor
        );
    }
}
