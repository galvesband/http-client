<?php

namespace Beat\HttpClient\Tests\Unit\Models;

use Beat\HttpClient\Tests\Misc\FakeUser;
use Beat\HttpClient\Tests\TestCase;
use Beat\HttpClient\Models\Request;

class RequestTest extends TestCase
{
    /** @test */
    public function relation_to_authenticatable_works_as_expected()
    {
        /** @var Request $request */
        $request = Request::newFactory()->create();
        $this->assertNull($request->authenticatable);

        /** @var FakeUser $fake_user */
        $fake_user = FakeUser::newFactory()->create();

        /** @var Request $other_request */
        $other_request = Request::newFactory()
            ->for($fake_user, 'authenticatable')
            ->create();

        $this->assertEquals($other_request->authenticatable_id, $fake_user->id);
        $this->assertEquals($other_request->authenticatable_type, get_class($fake_user));
        $this->assertEquals($other_request->authenticatable->id, $fake_user->id);

        $this->assertCount(1, $fake_user->requests);
        $this->assertEquals($other_request->id, $fake_user->requests->first()->id);
    }

    /** @test */
    public function url_attribute_works_as_expected()
    {
        /** @var Request $request */
        $request = Request::newFactory()->create([
            'method'     => 'GET',
            'scheme'     => 'https',
            'host'       => 'localhost',
            'port'       => 443,
            'path'       => '/api/things',
            'parameters' => '',
        ]);

        $this->assertEquals('https://localhost:443/api/things', $request->url);
    }
}