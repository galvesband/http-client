<?php

namespace Beat\HttpClient\Tests\Unit\Jobs;

use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\Jobs\SaveHttpRequestResultInDatabaseJob;
use Beat\HttpClient\Tests\Misc\FakeUser;
use Beat\HttpClient\Tests\TestCase;

class SaveHttpRequestResultInDatabaseJobTest extends TestCase
{
    /** @test */
    public function saves_request_and_response_data_in_database_with_no_authenticatable()
    {
        $requestData = new RegistrableRequestData(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $responseData = new RegistrableResponseData(
            200, '{"data":{"name":"Peter"}}', ['Content-Type' => ['application/json']], 0.5
        );
        $timestamp = now()->subSecond();
        $job = new SaveHttpRequestResultInDatabaseJob(
            "My Request", $requestData, $responseData, null, $timestamp
        );

        $job->handle();

        $this->assertDatabaseHas('requests', [
            "authenticatable_type" => null,
            "authenticatable_id"   => null,
            "label"                => "My Request",
            "timestamp"            => $timestamp->format('Y-m-d H:i:s'),
            "method"               => "POST",
            "scheme"               => "http",
            "host"                 => "localhost",
            "port"                 => 8000,
            "path"                 => "/",
            "parameters"           => "{\"name\":\"Peter\"}",
            "headers"              => "{\"Content-Type\":[\"application\/json\"]}",
            "response_code"        => 200,
            "response"             => "{\"data\":{\"name\":\"Peter\"}}",
            "response_headers"     => "{\"Content-Type\":[\"application\/json\"]}",
            "duration"             => 0.5
        ]);
    }

    /** @test */
    public function saves_request_and_response_data_in_database_with_authenticable()
    {
        $requestData = new RegistrableRequestData(
            'POST', 'http://localhost:8000/', ['Content-Type' => ['application/json']], '{"name":"Peter"}'
        );
        $responseData = new RegistrableResponseData(
            200, '{"data":{"name":"Peter"}}', ['Content-Type' => ['application/json']], 0.5
        );
        $timestamp = now()->subSecond();
        /** @var FakeUser $user */
        $user = FakeUser::newFactory()->create();
        $job = new SaveHttpRequestResultInDatabaseJob(
            "My Request", $requestData, $responseData, $user, $timestamp
        );

        $job->handle();

        $this->assertDatabaseHas('requests', [
            "authenticatable_type" => get_class($user),
            "authenticatable_id"   => $user->id,
            "label"                => "My Request",
            "timestamp"            => $timestamp->format('Y-m-d H:i:s'),
            "method"               => "POST",
            "scheme"               => "http",
            "host"                 => "localhost",
            "port"                 => 8000,
            "path"                 => "/",
            "parameters"           => "{\"name\":\"Peter\"}",
            "headers"              => "{\"Content-Type\":[\"application\/json\"]}",
            "response_code"        => 200,
            "response"             => "{\"data\":{\"name\":\"Peter\"}}",
            "response_headers"     => "{\"Content-Type\":[\"application\/json\"]}",
            "duration"             => 0.5
        ]);
    }
}
