<?php

namespace Beat\HttpClient\Tests\Unit\Sealers;

use Beat\HttpClient\Sealers\BeatExternalTokenSealer;
use Beat\HttpClient\Tests\TestCase;
use Mockery;
use Mockery\MockInterface;
use Psr\Http\Message\RequestInterface;

class BeatExternalTokenSealerTest extends TestCase
{
    /** @test */
    public function applies_auth_to_request()
    {
        $request_result = Mockery::mock(RequestInterface::class);
        $request = Mockery::mock(RequestInterface::class, function (MockInterface $mock) use ($request_result) {
            $mock
                ->shouldReceive('withHeader')
                ->withArgs(function (string $header, string $value) {
                    return $header === 'X-Auth'
                        && $value === hash('sha384', 'my_key' . gmdate('Ymd'));
                })
                ->once()
                ->andReturn($request_result);
        });

        $sealer = new BeatExternalTokenSealer('my_key');
        $result = $sealer->addAuthToRequest($request);

        $this->assertEquals($request_result, $result);
    }
}