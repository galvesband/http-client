<?php

namespace Beat\HttpClient\Tests\Unit\BuilderAddons;

use Beat\HttpClient\BuilderAddons\ExpectsJsonAddon;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\InvalidJsonException;
use Beat\HttpClient\Exceptions\ResponseValidationException;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\ResponseAddons\JsonDataAddon;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;
use Illuminate\Validation\Validator;
use Mockery;

class ExpectsJsonAddonTest extends TestCase
{
    /** @test */
    public function works_as_expected_without_rules_or_callback()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new ExpectsJsonAddon($builder);

        // Comprobamos que se construye aparentemente ok.
        $this->assertEquals($builder, $addon->getBuilder());
        $this->assertNull($addon->getValidationRules());
        $this->assertFalse($addon->hasValidatorCallback());

        // Le ponemos reglas y comprobamos que las asume.
        $this->assertEquals($builder, $addon->withValidationRules(['a' => 'required']));
        $this->assertSame(['a' => 'required'], $addon->getValidationRules());

        // Le ponemos un callback y parece que lo asume.
        $this->assertEquals($builder, $addon->withValidatorCallback(function () {}));
        $this->assertTrue($addon->hasValidatorCallback());

        // Le ponemos mensajes de validación y las asume.
        $this->assertEquals($builder, $addon->withValidationMessages(['a.required' => 'Campo obligatorio']));
        $this->assertSame(['a.required' => 'Campo obligatorio'], $addon->getValidationMessages());

        // Eliminamos reglas y callback del addon.
        $addon->withValidationRules(null);
        $addon->withValidatorCallback(null);
        $this->assertNull($addon->getValidationRules());
        $this->assertFalse($addon->hasValidatorCallback());

        // Construimos las opciones sin cabeceras previas ni nada.
        $options = [];
        $addon->buildOptions($options);
        $this->assertSame([GuzzleRequestOptions::HEADERS => ['Accept' => ['application/json']]], $options);

        // Volvemos a construir las opciones, pero esta vez con cabecera previa.
        $options = [GuzzleRequestOptions::HEADERS => ['Accept' => ['application/beat-data']]];
        $addon->buildOptions($options);
        $this->assertSame([GuzzleRequestOptions::HEADERS => ['Accept' => ['application/beat-data', 'application/json']]], $options);

        // Comprobamos que crea y añade un JsonDataAddon (para respuestas) a la respuesta cuando se le da oportunidad.
        $response = Mockery::mock(Response::class, function (Mockery\MockInterface $response_mock) {
            $response_mock
                ->shouldReceive('getBody')
                ->withArgs([])
                ->once()
                ->andReturn(Utils::streamFor('{"hi":"there"}'));

            $response_mock
                ->shouldReceive('pushAddon')
                ->withArgs(function (JsonDataAddon $response_json_addon) {
                    $data = $response_json_addon->getJsonData();

                    return array_key_exists('hi', $data)
                        && $data['hi'] === 'there';
                })
                ->once()
                ->andReturnSelf();
        });

        /** @noinspection PhpUnhandledExceptionInspection */
        $addon->applyToResponse($response);
    }

    /** @test */
    public function throws_invalid_json_exception_when_applied_to_response_with_invalid_json()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new ExpectsJsonAddon($builder);

        $internal_request = new Request('GET', 'http://localhost:8000/');
        $internal_response = new GuzzleResponse(200, [], 'not a json');
        $options = new RequestOptions();
        $response = Mockery::mock(Response::class, function (Mockery\MockInterface $response_mock) use ($options, $internal_response, $internal_request) {
            // Para decodificar la respuesta, necesita el cuerpo del mensaje de la misma.
            $response_mock
                ->shouldReceive('getBody')
                ->withArgs([])
                ->once()
                ->andReturn(Utils::streamFor('not a json'));

            // Al no poder decodificar, va a necesitar la petición y respuestas internas.
            $response_mock
                ->shouldReceive('getRequest')
                ->withArgs([])
                ->once()
                ->andReturn($internal_request);
            $response_mock
                ->shouldReceive('getInternalResponse')
                ->withArgs([])
                ->once()
                ->andReturn($internal_response);

            // También querrá las opciones y el tiempo de ejecución.
            $response_mock
                ->shouldReceive('getRequestOptions')
                ->withArgs([])
                ->once()
                ->andReturn($options);
            $response_mock
                ->shouldReceive('getTimeInSeconds')
                ->withArgs([])
                ->once()
                ->andReturn(0.5);
        });
        try {
            $addon->applyToResponse($response);
            $this->fail("Did not throw!");
        } catch (InvalidJsonException $ije) {
            $this->assertEquals($internal_request, $ije->getRequest());
            $this->assertEquals($internal_response, $ije->getResponse());
            $this->assertEquals($options, $ije->getRequestOptions());
            $this->assertEquals(.5, $ije->getTimeInSeconds());
        }
    }

    /** @test */
    public function uses_response_json_data_addon_to_validate_response_data_if_rules_is_supplied()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new ExpectsJsonAddon($builder, [
            'name' => 'required|string',
        ]);

        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/', []),
            new GuzzleResponse(200, [], '{"name":"Miles"}'),
            .01
        );

        /** @noinspection PhpUnhandledExceptionInspection */
        $addon->applyToResponse($response);
        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertSame(['name' => 'Miles'], $response->getJsonData());
    }

    /** @test */
    public function uses_response_json_data_addon_to_validate_response_data_if_callback_is_supplied()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new ExpectsJsonAddon(
            $builder,
            ['name' => 'required|string'],
            function (Validator $validator) {
                $validator->errors()->add('name', 'Shiiiit');
            },
            ['name.required' => 'Campo obligatorio.']
        );

        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/', []),
            new GuzzleResponse(200, [], '{"name":"Miles"}'),
            .01
        );

        try {
            /** @noinspection PhpUnhandledExceptionInspection */
            $addon->applyToResponse($response);
            $this->fail("Did not throw!");
        } catch (ResponseValidationException $e) {
            $this->assertSame(['name' => ['Shiiiit']], $e->getValidationErrors());

            $this->assertEquals($response->getRequest(), $e->getRequest());
            $this->assertEquals($response->getInternalResponse(), $e->getResponse());
            $this->assertEquals($response->getRequestOptions(), $e->getRequestOptions());
            $this->assertEquals($response->getTimeInSeconds(), $e->getTimeInSeconds());
        }
    }

    /** @test */
    public function throws_appropiate_exception_if_data_is_not_valid()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new ExpectsJsonAddon(
            $builder,
            ['name' => 'required|string'],
            null,
            ['name.required' => 'Campo obligatorio.']
        );

        $response = new Response(
            new RequestOptions(),
            new Request('GET', 'http://localhost:8000/', []),
            new GuzzleResponse(200, [], '{"name":null}'),
            .01
        );

        try {
            /** @noinspection PhpUnhandledExceptionInspection */
            $addon->applyToResponse($response);
            $this->fail("Did not throw!");
        } catch (ResponseValidationException $e) {
            $this->assertSame(['name' => ['Campo obligatorio.']], $e->getValidationErrors());
        }
    }
}