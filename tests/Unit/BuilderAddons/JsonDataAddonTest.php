<?php

namespace Beat\HttpClient\Tests\Unit\BuilderAddons;

use Beat\HttpClient\BuilderAddons\JsonDataAddon;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;
use Mockery;

class JsonDataAddonTest extends TestCase
{
    /** @test */
    public function works_as_expected()
    {
        // Construimos un nuevo addon pasándole un builder mockeado sin expectativas de llamada.
        /** @var RequestBuilder $builder */
        $builder = Mockery::mock(RequestBuilder::class);
        $addon = new JsonDataAddon($builder, ['some' => 'data']);

        // ¿Se ha construido bien?
        $this->assertEquals($builder, $addon->getBuilder());
        $this->assertSame(['some' => 'data'], $addon->getJsonData());

        // ¿setJsonData() funciona como debe?
        $this->assertEquals($builder, $addon->setJsonData(['some_other' => 'data']));
        $this->assertSame(['some_other' => 'data'], $addon->getJsonData());

        // Construir opciones sin HEADERS previos.
        $options = [];
        $addon->buildOptions($options);
        $this->assertSame([
            GuzzleRequestOptions::JSON    => ['some_other' => 'data'],
            GuzzleRequestOptions::HEADERS => ['Content-Type' => ['application/json']],
        ], $options);

        // Construir opciones con HEADERS previos.
        $options = [
            GuzzleRequestOptions::HEADERS => ['Content-Type' => ['application/beat-some-other-data']],
        ];
        $addon->buildOptions($options);
        $this->assertSame([
            GuzzleRequestOptions::HEADERS => ['Content-Type' => ['application/beat-some-other-data', 'application/json']],
            GuzzleRequestOptions::JSON    => ['some_other' => 'data'],
        ], $options);

        // Comprobamos que no se hace ninguna modificación a la respuesta en applyToResponse()
        // pasándole un mock de una respuesta que no espera ninguna llamada
        // (por lo que fallaría si se hiciera algo con ella).
        /** @var Response $mock_response */
        $mock_response = Mockery::mock(Response::class);
        $addon->applyToResponse($mock_response);
    }
}