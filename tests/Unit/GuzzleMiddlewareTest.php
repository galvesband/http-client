<?php

namespace Beat\HttpClient\Tests\Unit;

use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Events\FailedHttpRequestEvent;
use Beat\HttpClient\Events\SuccessfulHttpRequestEvent;
use Beat\HttpClient\Exceptions\ClientException;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\ConnectionException;
use Beat\HttpClient\Exceptions\RequestException;
use Beat\HttpClient\Exceptions\ServerException;
use Beat\HttpClient\GuzzleMiddleware;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\Tests\Misc\TestAddon;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use GuzzleHttp\Promise\Create;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Support\Facades\Event;
use Mockery;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Throwable;

class GuzzleMiddlewareTest extends TestCase
{
    /**
     * @param callable|null $request_checker
     * @param ResponseInterface|Throwable $response
     * @return callable
     */
    protected function buildHandler(
        ?callable $request_checker,
        $response
    ): callable
    {
        return function (RequestInterface $request, array $request_options) use ($response, $request_checker) {
            if (array_key_exists('beat', $request_options)) {
                $this->fail("Request options contains 'beat' key. It should not at this point.");
            }

            if ($request_checker && !$request_checker($request, $request_options)) {
                $this->fail("Request_checker did not validate data received in handler.");
            }

            if ($response instanceof ResponseInterface) {
                return Create::promiseFor($response);
            } else {
                return Create::rejectionFor($response);
            }
        };
    }

    /** @test */
    public function normal_request_is_converted_to_package_version_of_request_and_successful_request_event_is_raised()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        /** @var Response $out_response */
        $out_response = $promise->wait();

        // Primero, debemos haber obtenido una respuesta de las nuestras.
        $this->assertInstanceOf(Response::class, $out_response);

        // Comprobamos que la petición que incluimos en la respuesta es la nuestra.
        $this->assertEquals($request->getMethod(), $out_response->getRequest()->getMethod());
        $this->assertEquals($request->getUri(), $out_response->getRequest()->getUri());
        $this->assertEquals((string) $request->getBody(), (string) $out_response->getRequest()->getBody());

        // Comprobamos que la respuesta es la esperada.
        $this->assertEquals(200, $out_response->getStatusCode());
        $this->assertEquals($response, $out_response->getInternalResponse());
        $this->assertEquals((string) $response->getBody(), (string) $out_response->getInternalResponse()->getBody());

        // Comprobamos que se lanzó el evento de petición exitosa, con las opciones incluidas en la petición.
        Event::assertDispatched(function (SuccessfulHttpRequestEvent $event) use ($out_response) {
            return $event->response === $out_response
                && $event->options->getLabel() === 'Mi Petición';
        });
        // Y que no se lanzó el evento de petición chunga.
        Event::assertNotDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function failed_request_by_unknown_reason_is_converted_to_package_version_of_communication_exception_and_failed_request_event_is_raised()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $error = new RuntimeException("OMG!");

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Error que debe lanzar el handler
            $error
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw an exception!");
        } catch (Throwable $t) {
            $this->assertInstanceOf(CommunicationException::class, $t);
            $this->assertEquals('Error de comunicación durante operación \'Mi Petición\': \'OMG!\'.', $t->getMessage());
            $this->assertEquals($error, $t->getPrevious());
            $this->assertEquals($request, $t->getRequest());
        }

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(function (FailedHttpRequestEvent $event) use ($error, $t) {
            return $event->exception === $t
                && $event->exception->getRequestOptions()->getLabel() === 'Mi Petición'
                && $event->exception->getPrevious() === $error;
        });
    }

    /** @test */
    public function failed_request_by_connection_error_is_converted_to_package_version_of_connection_exception()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $error = new ConnectException('Damn', $request);

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Error que debe lanzar el handler
            $error
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw an exception!");
        } catch (ConnectionException $t) {
            $this->assertInstanceOf(ConnectionException::class, $t);
            $this->assertEquals($error, $t->getPrevious());
            $this->assertEquals($request, $t->getRequest());
        }

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(function (FailedHttpRequestEvent $event) use ($error, $t) {
            return $event->exception === $t
                && $event->exception->getPrevious() === $error
                && $event->exception->getRequestOptions()->getLabel() === 'Mi Petición';
        });
    }

    /** @test */
    public function failed_request_by_client_error_is_converted_to_package_version_of_client_exception()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $response = new GuzzleResponse(404, ['Content-Type' => ['application/json']], '{"result":"WTF"}');
        $error = new GuzzleClientException('Damn', $request, $response);

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Error que debe lanzar el handler
            $error
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw an exception!");
        } catch (ClientException $t) {
            $this->assertEquals($error, $t->getPrevious());
            $this->assertEquals($request, $t->getRequest());
            $this->assertEquals($response, $t->getResponse());
        }

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(function (FailedHttpRequestEvent $event) use ($error, $t) {
            return $event->exception === $t
                && $event->exception->getPrevious() === $error;
        });
    }

    /** @test */
    public function failed_request_by_server_error_is_converted_to_package_version_of_server_exception()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $response = new GuzzleResponse(404, ['Content-Type' => ['application/json']], '{"result":"WTF"}');
        $error = new GuzzleServerException('Damn', $request, $response);

        $handler = $this->buildHandler(
        // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Error que debe lanzar el handler
            $error
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw an exception!");
        } catch (ServerException $t) {
            $this->assertEquals($error, $t->getPrevious());
            $this->assertEquals($request, $t->getRequest());
            $this->assertEquals($response, $t->getResponse());
        }

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(function (FailedHttpRequestEvent $event) use ($error, $t) {
            return $event->exception === $t
                && $event->exception->getPrevious() === $error;
        });
    }

    /** @test */
    public function addons_are_applied_to_successful_response()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $addon_applied = false;
        $builder = Mockery::mock(RequestBuilder::class);
        $request_options->getBuilderAddons()->push(new TestAddon($builder, null, function () use (&$addon_applied) {
            $addon_applied = true;
        }));
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        $promise->wait();
        $this->assertTrue($addon_applied);

        Event::assertDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertNotDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function fail_in_addon_by_unknown_reason_gets_converted_to_communication_exception()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $builder = Mockery::mock(RequestBuilder::class);
        $request_options->getBuilderAddons()->push(new TestAddon($builder, null, new RuntimeException("BOOM!")));
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw!");
        } catch (RequestException $exception) {}

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function fail_in_addon_by_communication_exception_derived_exceptions_goes_through()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $request_options = new RequestOptions();
        $request_options->setLabel('Mi Petición');
        $error = new CommunicationException($request, $request_options, 0.1, new RuntimeException("OMG"));
        $builder = Mockery::mock(RequestBuilder::class);
        $request_options->getBuilderAddons()->push(new TestAddon($builder, null, $error));
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, [
            'beat' => $request_options,
        ]);

        try {
            $promise->wait();
            $this->fail("Did not throw!");
        } catch (CommunicationException $exception) {
            $this->assertEquals($error, $exception);
        }

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function throws_runtime_exception_if_request_does_not_have_beat_options()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);

        try {
            $inner_middleware_closure($request, []);
            $this->fail("Did not throw!");
        } catch (RuntimeException $re) {}

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertNotDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function use_default_request_options_if_request_does_not_have_beat_options_but_middleware_has_default_options()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration(), new RequestOptions(
            "Raw request", collect([]), collect([])
        ));
        $inner_middleware_closure = $middleware($handler);
        /** @var PromiseInterface $promise */
        $promise = $inner_middleware_closure($request, []);

        /** @var Response $out_response */
        $out_response = $promise->wait();

        // Primero, debemos haber obtenido una respuesta de las nuestras.
        $this->assertInstanceOf(Response::class, $out_response);

        // Comprobamos que se lanzó el evento de petición exitosa, con las opciones incluidas en la petición.
        Event::assertDispatched(function (SuccessfulHttpRequestEvent $event) use ($out_response) {
            return $event->response === $out_response
                && $event->options->getLabel() === 'Raw request';
        });
        // Y que no se lanzó el evento de petición chunga.
        Event::assertNotDispatched(FailedHttpRequestEvent::class);
    }

    /** @test */
    public function throws_runtime_exception_if_request_has_an_unexpected_type_of_beat_options()
    {
        Event::fake([SuccessfulHttpRequestEvent::class, FailedHttpRequestEvent::class]);

        $request = new Request(
            'POST', 'http://localhost:8000/',
            [
                'Content-Type' => ['application/json'],
                'Accept'       => ['application/json'],
                'User-Agent'   => ['Beat/HttpClient']],
            '{"name":"Destroyer of Worlds"}'
        );
        $response = new GuzzleResponse(200, ['Content-Type' => ['application/json']], '{"result":true}');

        $handler = $this->buildHandler(
            // Función que comprueba que al handler llega lo que debe de llegar...
            function (RequestInterface $in_request, array $in_options) use ($request): bool {
                return $in_request === $request
                    && empty($in_options);
            },
            // Respuesta que debe devolver el handler
            $response
        );

        $middleware = new GuzzleMiddleware(new ProfileConfiguration());
        $inner_middleware_closure = $middleware($handler);

        try {
            $inner_middleware_closure($request, [
                'beat' => [],
            ]);
            $this->fail("Did not throw!");
        } catch (RuntimeException $re) {}

        Event::assertNotDispatched(SuccessfulHttpRequestEvent::class);
        Event::assertNotDispatched(FailedHttpRequestEvent::class);
    }
}
