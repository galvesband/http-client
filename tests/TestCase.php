<?php

namespace Beat\HttpClient\Tests;

use Beat\HttpClient\BeatHttpClientServiceProvider;
use Beat\HttpClient\Testing\InteractsWithHttpClient;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use RefreshDatabase;

    protected function getPackageProviders($app): array
    {
        return [
            BeatHttpClientServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetup($app)
    {
        // Ejecutamos las migraciones de base de datos necesarias:
        include_once __DIR__ . '/Misc/fake_user_migration.php.stub';
        include_once __DIR__ . '/../database/migrations/create_requests_table.php.stub';

        /** @noinspection PhpUndefinedClassInspection */
        (new \CreateFakeUsersTable())->up();
        /** @noinspection PhpUndefinedClassInspection */
        (new \CreateRequestsTable())->up();
    }
}
