<?php /** @noinspection PhpUnhandledExceptionInspection */

namespace Beat\HttpClient\Tests\Feature;

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\ContentExtractors\SimpleContentExtractor;
use Beat\HttpClient\Exceptions\ClientException;
use Beat\HttpClient\Exceptions\ConnectionException;
use Beat\HttpClient\Exceptions\InvalidJsonException;
use Beat\HttpClient\Exceptions\ResponseValidationException;
use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\Factories\SinkFactory;
use Beat\HttpClient\Sealers\BeatExternalTokenSealer;
use Beat\HttpClient\Sinks\DatabaseSink;
use Beat\HttpClient\Sinks\LogSink;
use Beat\HttpClient\Tests\Misc\TestClientFactory;
use Beat\HttpClient\Tests\Misc\TransactionTestData;
use Beat\HttpClient\Tests\TestCase;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Illuminate\Support\Facades\Log;
use Mockery;
use Mockery\MockInterface;
use Psr\Log\LoggerInterface;

/**
 * Test de integración, donde solo mockeamos lo mínimo y dejamos que el resto funcione solo.
 *
 * Los tests representan casos de uso típicos.
 */
class RequestTest extends TestCase
{
    const SIMPLE_CONFIG = [
        'sinks' => [],
        'seal_providers' => [],
        'profile_defaults' => [
            'base_uri'        => '',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
        ],
    ];

    const LOG_SINK_CONFIG = [
        'sinks' => [
            'log' => [
                'class' => LogSink::class,
                'params' => [
                    'log_channel' => 'stack',
                    'register_successful_requests' => true,
                    'register_failed_requests' => true,
                ],
                'request_content_extractor' => [
                    'class' => NoContentExtractor::class,
                    'params' => [],
                ],
                'response_content_extractor' => [
                    'class' => NoContentExtractor::class,
                    'params' => [],
                ],
            ],
        ],
        'seal_providers' => [],
        'profile_defaults' => [
            'base_uri'        => '',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
            'with_log' => ['sinks' => ['log']],
        ],
    ];

    const DATABASE_SINK_CONFIG = [
        'sinks' => [
            'db' => [
                'class' => DatabaseSink::class,
                'params' => [
                    'queue' => 'sync',
                    'register_successful_requests' => true,
                    'register_failed_requests' => true,
                ],
                'request_content_extractor' => [
                    'class' => NoContentExtractor::class,
                    'params' => [],
                ],
                'response_content_extractor' => [
                    'class' => NoContentExtractor::class,
                    'params' => [],
                ],
            ],
        ],
        'seal_providers' => [],
        'profile_defaults' => [
            'base_uri'        => '',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
            'with_db' => ['sinks' => ['db']],
        ],
    ];

    const LOG_AND_DATABASE_SINK_CONFIG = [
        'sinks' => [
            'log' => [
                'class' => LogSink::class,
                'params' => [
                    'log_channel' => 'stack',
                    'register_successful_requests' => true,
                    'register_failed_requests' => true,
                ],
                'request_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => [
                        'allowedContentTypes' => [
                            'application/json',
                        ],
                        'allowEmptyContentType' => true,
                        'sizeThreshold' => 1024 * 1024 * 4, // 4 MiBs
                    ],
                ],
                'response_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => [
                        'allowedContentTypes' => [
                            'application/json',
                        ],
                        'allowEmptyContentType' => true,
                        'sizeThreshold' => 1024 * 1024 * 4, // 4 MiBs
                    ],
                ],
            ],
            'database' => [
                'class' => DatabaseSink::class,
                'params' => [
                    'queue' => 'default',
                    'register_successful_requests' => true,
                    'register_failed_requests' => true,
                ],
                'request_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => [
                        'allowedContentTypes' => [
                            'application/json',
                        ],
                        'allowEmptyContentType' => true,
                        'sizeThreshold' => 1024 * 1024 * 4, // 4 MiBs
                    ],
                ],
                'response_content_extractor' => [
                    'class' => SimpleContentExtractor::class,
                    'params' => [
                        'allowedContentTypes' => [
                            'application/json',
                        ],
                        'allowEmptyContentType' => true,
                        'sizeThreshold' => 1024 * 1024 * 4, // 4 MiBs
                    ],
                ],
            ],
        ],
        'seal_providers' => [],
        'profile_defaults' => [
            'base_uri'        => '',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
            'with_log_and_db' => ['sinks' => ['log', 'database']],
        ],
    ];

    const BASE_URI_CONFIG = [
        'sinks' => [],
        'seal_providers' => [],
        'profile_defaults' => [
            'base_uri'        => 'http://localhost:8000/',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
        ],
    ];

    const SEALER_CONFIG = [
        'sinks' => [],
        'seal_providers' => [
            'beat' => [
                'class' => BeatExternalTokenSealer::class,
                'params' => [
                    'passkey' => 'my_key',
                ],
            ],
        ],
        'profile_defaults' => [
            'base_uri'        => '',
            'connect_timeout' => 10,
            'timeout'         => 20,
            'user_agent'      => 'Beat/HttpClient/GuzzleHttp 7',
            'seal'            => null,
            'sinks'           => [],
        ],
        'profiles' => [
            'default' => [],
            'beat'  => [
                'seal' => 'beat',
            ],
        ],
    ];

    /* ******* */
    /* HELPERS */
    /* ******* */

    protected function build_simple_request_with_json_response(
        ?string $request_body   = null,
        ?string $response_body  = null,
        string  $base_uri       = 'http://localhost:8000/users/miles'
    ): TransactionTestData
    {
        return new TransactionTestData(
            new Request('POST', $base_uri, [], $request_body),
            new GuzzleResponse(200, ['Content-Type' => ['application/json']], $response_body ?? '{"data":{"name":"Miles"}}')
        );
    }

    protected function build_simple_request_with_connection_exception(
        string $base_uri = 'http://localhost:8000/users/miles'
    ): TransactionTestData
    {
        $request = new Request('GET', $base_uri);

        return new TransactionTestData(
            $request,
            new ConnectException('No route to server!', $request, null)
        );
    }

    /* ***** */
    /* TESTS */
    /* ***** */

    /**
     * Test mínimo, donde hacemos un get y obtenemos una respuesta, sin más líos.
     *
     * @test
     */
    public function does_simple_request_with_successful_response()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::SIMPLE_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $response = $client->newRequest('Simple test')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->execute();

        $this->assertEquals($transaction_data->response, $response->getInternalResponse());
    }

    /**
     * Igual que el anterior, pero con respuesta negativa.
     *
     * @test
     */
    public function does_simple_request_with_failed_response()
    {
        $transaction_data = $this->build_simple_request_with_connection_exception();
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::SIMPLE_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        try {
            /** @noinspection PhpUnhandledExceptionInspection */
            $client->newRequest('Simple test')
                ->to('GET', 'http://localhost:8000/users/miles')
                ->execute();
            $this->fail("Did not throw!");
        }
        catch (ConnectionException $ce) {
            $this->assertEquals($ce->getPrevious(), $transaction_data->response);
        }
    }

    /**
     * Metemos el sink que escribe logs en la mezcla.
     *
     * @test
     */
    public function logs_requests_when_profile_has_log_sink()
    {
        // Definimos un canal de logging mockeado porque parece que
        // el TestBench de Laravel no produce canales por sí solo.
        $logger_mock = Mockery::mock(LoggerInterface::class, function (MockInterface $mock) {
            // Los detalles no nos importan mucho porque los tests unitarios ya los tienen cubiertos.
            $mock
                ->shouldReceive('info')
                ->once()
                ->andReturnNull();
        });

        // Definimos la expectativa de que se va a usar el logger 'stack'.
        // El resto de detalles está bastante probado en los tests unitarios.
        Log::shouldReceive('channel')
            ->withArgs(['stack'])
            ->once()
            ->andReturn($logger_mock);

        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::LOG_SINK_CONFIG,
            new SinkFactory(static::LOG_SINK_CONFIG['sinks']),
            collect(['with_log' => [$transaction_data]])
        );
        $client = $clientFactory->profile('with_log');

        $client->newRequest('Logged request')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->execute();
    }

    /**
     * Metemos el sink que escribe en base de datos en la prueba.
     *
     * @test
     */
    public function registers_requests_in_database_when_profile_has_database_sink()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::DATABASE_SINK_CONFIG,
            new SinkFactory(static::DATABASE_SINK_CONFIG['sinks']),
            collect(['with_db' => [$transaction_data]])
        );
        $client = $clientFactory->profile('with_db');

        $client->newRequest('Logged request')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->execute();

        $this->assertDatabaseHas('requests', [
            'method'        => 'POST',
            'scheme'        => 'http',
            'host'          => 'localhost',
            'port'          => 8000,
            'response_code' => 200,
            'response'      => '-- Skipped content of type \'application/json\' weighting 0.024 KiB --'
        ]);
    }

    /**
     * Incidencia: beat/HttpClient/-/issues/2
     * @test
     */
    public function registers_request_in_database_when_profile_has_database_sink_and_no_port_in_uri()
    {
        $transaction_data = $this->build_simple_request_with_json_response(null, null, 'http://localhost/users/miles');
        $clientFactory = new TestClientFactory(
            static::DATABASE_SINK_CONFIG,
            new SinkFactory(static::DATABASE_SINK_CONFIG['sinks']),
            collect(['with_db' => [$transaction_data]])
        );
        $client = $clientFactory->profile('with_db');

        $client->newRequest('Logged request')
            ->to('POST', 'http://localhost/users/miles')
            ->execute();

        $this->assertDatabaseHas('requests', [
            'method' => 'POST',
            'scheme' => 'http',
            'host'   => 'localhost',
            'port'   => 80,
        ]);
    }

    /**
     * Teoría: Dos sinks consecutivos no funcionan bien porque el primero extrae el contenido,
     * y el segundo no extrae nada porque no rebobina el contenido (Stream).
     * @test
     */
    public function registers_request_in_log_and_database_when_both_sinks_are_in_profile()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::LOG_AND_DATABASE_SINK_CONFIG,
            new SinkFactory(static::LOG_AND_DATABASE_SINK_CONFIG['sinks']),
            collect(['with_log_and_db' => [$transaction_data]])
        );
        $client = $clientFactory->profile('with_log_and_db');

        $client->newRequest('Logged request')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->execute();

        $this->assertDatabaseHas('requests', [
            'method'        => 'POST',
            'scheme'        => 'http',
            'host'          => 'localhost',
            'port'          => 8000,
            'response_code' => 200,
            'response'      => '{"data":{"name":"Miles"}}'
        ]);
    }

    /**
     * Usamos nuestro addon para enviar datos en formato JSON.
     *
     * @test
     */
    public function sends_jsons_with_json_addon()
    {
        $json = ['name' => 'Miles'];
        $transaction_data = $this->build_simple_request_with_json_response(json_encode($json));
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::DATABASE_SINK_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $response = $client->newRequest('Json Request')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->withJsonData($json)
            ->execute();

        $this->assertSame($json, json_decode((string) $response->getRequest()->getBody(), true));
    }

    /**
     * Usamos nuestro addon para indicar que esperamos un JSON y decodificarlo en respuestas con éxito.
     *
     * @test
     */
    public function expects_jsons_with_expects_json_addon_and_response_is_decoded()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::SIMPLE_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $response = $client->newRequest('Json Response')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->expectsJson()
            ->execute();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertSame(['data' => ['name' => 'Miles']], $response->getJsonData());
    }

    /**
     * Usamos nuestro addon para indicar que esperamos un JSON y lanzamos una excepción si la respuesta no contiene un JSON.
     *
     * @test
     */
    public function expects_jsons_with_expects_json_addon_and_throws_json_exception_if_response_content_is_not_a_json()
    {
        $transaction_data = $this->build_simple_request_with_json_response(null, 'not-a-json');
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::SIMPLE_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $this->expectException(InvalidJsonException::class);
        $client->newRequest('Invalid Json Response')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->expectsJson()
            ->execute();
    }

    /** @test */
    public function expects_json_with_validation_rules_and_throws_if_data_is_not_valid()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::SIMPLE_CONFIG,
            new SinkFactory(static::SIMPLE_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $response = $client->newRequest('Invalid Json Response')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->expectsJson([
                'data'      => 'required|array',
                'data.name' => 'required|string'
            ])
            ->execute();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->assertSame(json_decode((string) $response->getbody(), true), $response->getJsonData());

        try {
            /** @noinspection PhpUndefinedMethodInspection */
            $response->validateJsonData([
                'data'      => 'required|array',
                'data.name' => 'required|string|max:2',
            ]);
        } catch (ResponseValidationException $t) {
            $this->assertSame(['data.name' => ['The data.name must not be greater than 2 characters.']], $t->getValidationErrors());
        }
    }

    /** @test */
    public function base_uri_check()
    {
        $transaction_data = $this->build_simple_request_with_json_response(
            null, '{"data":{"name":"Miles"}}', '/users/miles'
        );
        $clientFactory = new TestClientFactory(
            static::BASE_URI_CONFIG,
            new SinkFactory(static::BASE_URI_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        $response = $client->newRequest('Base Uri Check')
            ->to('POST', '/users/miles')
            ->execute();

        $this->assertEquals('http://localhost:8000/users/miles', (string) $response->getRequest()->getUri());
    }

    /**
     * Este test es importante. Aquí nos aseguramos de que el middleware propio de Guzzle
     * convierte las respuestas con código de estado >= 400 en excepciones.
     *
     * @see ClientFactory::build_guzzle_stack()
     * @test
     */
    public function base_uri_check_on_failed_exception()
    {
        $transaction_data = new TransactionTestData(
            new Request('POST', "/users/miles"),
            new GuzzleResponse(404)
        );

        $clientFactory = new TestClientFactory(
            static::BASE_URI_CONFIG,
            new SinkFactory(static::BASE_URI_CONFIG['sinks']),
            collect(['default' => [$transaction_data]])
        );
        $client = $clientFactory->profile('default');

        try {
            /** @noinspection PhpUnhandledExceptionInspection */
            $client->newRequest('Base Uri Check')
                ->to('POST', '/users/miles')
                ->execute();
            $this->fail("Did not throw!");
        }
        catch (ClientException $ce) {
            $this->assertEquals('http://localhost:8000/users/miles', (string) $ce->getRequest()->getUri());
        }
    }

    /** @test */
    public function check_request_with_beat_sealer()
    {
        $transaction_data = $this->build_simple_request_with_json_response();
        $clientFactory = new TestClientFactory(
            static::SEALER_CONFIG,
            new SinkFactory(static::SEALER_CONFIG['sinks']),
            collect(['beat' => [$transaction_data]])
        );
        $client = $clientFactory->profile('beat');

        $response = $client->newRequest('Simple test')
            ->to('POST', 'http://localhost:8000/users/miles')
            ->execute();

        $this->assertEquals($transaction_data->response, $response->getInternalResponse());

        $this->assertEquals(hash('sha384', 'my_key' . gmdate('Ymd')), $response->getRequest()->getHeader('X-Auth')[0]);
    }
}