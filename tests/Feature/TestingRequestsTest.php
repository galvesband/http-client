<?php

namespace Beat\HttpClient\Tests\Feature;

use Beat\HttpClient\Facades\HttpClient;
use Beat\HttpClient\Testing\InteractsWithHttpClient;
use Beat\HttpClient\Tests\TestCase;
use Exception;
use Psr\Http\Message\RequestInterface;

class TestingRequestsTest extends TestCase
{
    use InteractsWithHttpClient;

    /** @test */
    public function noop_if_no_pending_expectations_on_closing()
    {
        // Definimos una expectativa una petición a localhost que devuelven 200.
        HttpClient::onProfile('default')
            ->expectRequest(function (RequestInterface $request) {
                return $request->getMethod() === 'GET' && $request->getUri()->getHost() === 'localhost';
            })
            ->once()
            ->resultingInResponse(200, 'Hi');

        // Hacemos una única petición.
        /** @noinspection PhpUnhandledExceptionInspection */
        $response = HttpClient::newRequest('psst')
            ->to('GET', 'http://localhost/tururu')
            ->execute();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Hi', (string) $response->getBody());
    }

    /** @test */
    public function throws_if_pending_expectations_on_closing()
    {
        // Definimos una expectativa para 2 peticiones a localhost que devuelven 404.
        HttpClient::onProfile('default')
            ->expectRequest(function (RequestInterface $request) {
                return $request->getMethod() === 'GET' && $request->getUri()->getHost() === 'localhost';
            })
            ->twice()
            ->resultingInResponse(200, 'Hi');

        // Hacemos una única petición.
        /** @noinspection PhpUnhandledExceptionInspection */
        $response = HttpClient::newRequest('psst')
            ->to('GET', 'http://localhost/tururu')
            ->execute();

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('Hi', (string) $response->getBody());

        try {
            HttpClient::closeExpectations();
            $this->fail("Did not throw!");
        } catch (Exception $e) {
            $this->assertEquals('Quedan por realizar 1 peticiones de al menos una expectativa en el perfil default.', $e->getMessage());
        }
    }

    /** @test */
    public function trait_helper_method_checks_pending_expectations_on_faked_factory()
    {
        HttpClient::onProfile('default')
            ->expectRequest(function (RequestInterface $request) {
                return $request->getMethod() === 'GET' && $request->getUri()->getHost() === 'localhost';
            })
            ->once()
            ->resultingInResponse(200, 'Hi');

        try {
            $this->checkPendingHttpExpectations();
            $this->fail("Did not throw!");
        } catch (Exception $e) {}
    }

    /**
     * Si no llamamos a HttpClient::fake(), entonces la factoría de clientes es una factoría normal
     * (en lugar de una falsa para tests). En ese caso, el método checkPendingHttpExpectations() debe no hacer nada.
     *
     * @test
     */
    public function trait_helper_method_does_nothing_on_normal_factory()
    {
        $this->checkPendingHttpExpectations();
        $this->expectNotToPerformAssertions();
    }

    /**
     * Demostramos que se puede enviar un JSON en una petición y que los helpers de testeo
     * permiten el acceso al cuerpo de la petición de prueba.
     * @test
     */
    public function expectation_has_access_to_requests_body()
    {
        HttpClient::fake();

        HttpClient::onProfile('default')
            ->expectRequest(function (RequestInterface $request) {
                $body = json_decode((string) $request->getBody(), true);

                return array_key_exists('hello', $body)
                    && $body['hello'] === 'there';
            })
            ->once()
            ->resultingInResponse(204, '');

        /** @noinspection PhpUnhandledExceptionInspection */
        $response = HttpClient::newRequest('Probatura con JSON')
            ->to('GET', 'https://localhost')
            ->withJsonData(['hello' => 'there'])
            ->execute();

        $this->assertEquals(204, $response->getStatusCode());
        $this->assertEmpty((string) $response->getBody());

        HttpClient::closeExpectations();
    }
}