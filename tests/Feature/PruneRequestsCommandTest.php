<?php

namespace Beat\HttpClient\Tests\Feature;

use Beat\HttpClient\Models\Request;
use Beat\HttpClient\Tests\TestCase;
use RuntimeException;

class PruneRequestsCommandTest extends TestCase
{
    /** @test */
    public function rejects_execution_without_by_date_or_by_days_or_by_count()
    {
        try {
            $this->artisan('beat:http-client:prune');
            $this->fail("Did not throw!");
        } catch (RuntimeException $e) {
            $this->assertEquals('Debes especificar un criterio (--by-date, --by-days o --by-count).', $e->getMessage());
        }
    }

    /** @test */
    public function prune_by_date_with_prunable_requests()
    {
        $date = now()->subDays(10);

        // Registro que será eliminado.
        /** @var Request $to_be_deleted */
        $to_be_deleted = Request::newFactory()->create(['timestamp' => now()->subDays(11)]);
        // Registros que permanecerán.
        Request::newFactory()->create(['timestamp' => now()->subDays(10)]);
        Request::newFactory()->create(['timestamp' => now()->subDays(9)]);

        $this->artisan('beat:http-client:prune --by-date=' . $date->format('Y-m-d'))
            ->expectsOutput('Iniciando pruning en modo by-date...')
            ->expectsOutput('Se han eliminado 1 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', 2);
        $this->assertDatabaseMissing('requests', ['id' => $to_be_deleted->id]);
    }

    /** @test */
    public function prune_by_date_without_prunable_requests()
    {
        $date = now()->subDays(10);

        // Registros que permanecerán.
        Request::newFactory()->create(['timestamp' => now()->subDays(10)]);
        Request::newFactory()->create(['timestamp' => now()->subDays(9)]);

        $this->artisan('beat:http-client:prune --by-date=' . $date->format('Y-m-d'))
            ->expectsOutput('Iniciando pruning en modo by-date...')
            ->expectsOutput('Se han eliminado 0 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', 2);
    }

    /** @test */
    public function detects_invalid_dates_when_by_date_mode()
    {
        try {
            $this->artisan('beat:http-client:prune --by-date=' . '2023-13-');
            $this->fail("Did not throw!");
        } catch (RuntimeException $e) {
            $this->assertStringContainsString('YYYY-MM-DD', $e->getMessage());
        }
    }

    /** @test */
    public function prune_by_days_with_prunable_requests()
    {
        $days = 8;

        // Creamos un Request por día en los últimos $days + 2 días, incluida una petición _hoy_.
        // Por tanto, se crean $days + 3 peticiones en BD, y al hacer prune by-days=$days,
        // deben quedar $days + 1 peticiones en BD.
        for ($i = 0; $i <= $days + 2; $i++) {
            Request::newFactory()->create(['timestamp' => now()->subDays($i)]);
        }

        $this->artisan('beat:http-client:prune --by-days=' . $days)
            ->expectsOutput('Iniciando pruning en modo by-days...')
            ->expectsOutput('Se han eliminado 2 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', $days + 1);
    }

    /** @test */
    public function prune_by_days_without_prunable_requests()
    {
        Request::newFactory()->create(['timestamp' => now()->subDay()]);

        $this->artisan('beat:http-client:prune --by-days=2')
            ->expectsOutput('Iniciando pruning en modo by-days...')
            ->expectsOutput('Se han eliminado 0 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', 1);
    }

    /** @test */
    public function prune_by_count_with_prunable_requests()
    {
        Request::newFactory()->count(12)->create([
            'timestamp' => now()->subDay(),
        ]);
        Request::newFactory()->count(2)->create([
            'timestamp' => now()->subDays(10),
        ]);

        $this->artisan('beat:http-client:prune --by-count=12')
            ->expectsOutput('Iniciando pruning en modo by-count...')
            ->expectsOutput('Se han eliminado 2 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', 12);
    }

    /** @test */
    public function prune_by_count_without_prunable_requests()
    {
        Request::newFactory()->count(12)->create([
            'timestamp' => now()->subDay(),
        ]);

        $this->artisan('beat:http-client:prune --by-count=12')
            ->expectsOutput('Se han eliminado 0 registros.')
            ->assertSuccessful();

        $this->assertDatabaseCount('requests', 12);
    }

    /** @test */
    public function detects_invalid_number_when_in_by_days_or_by_count_mode()
    {
        try {
            $this->artisan('beat:http-client:prune --by-days=a');
            $this->fail("Did not throw!");
        } catch (RuntimeException $e) {
            $this->assertStringContainsString('Número inválido (debe ser un número entero mayor o igual a 1): a', $e->getMessage());
        }

        try {
            $this->artisan('beat:http-client:prune --by-count=a');
            $this->fail("Did not throw!");
        } catch (RuntimeException $e) {
            $this->assertStringContainsString('Número inválido (debe ser un número entero mayor o igual a 1): a', $e->getMessage());
        }
    }
}