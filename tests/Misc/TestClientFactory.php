<?php

namespace Beat\HttpClient\Tests\Misc;

use Beat\HttpClient\Client;
use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\Factories\SinkFactory;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use Illuminate\Support\Collection;
use Mockery;
use Mockery\MockInterface;

class TestClientFactory extends ClientFactory
{
    protected Collection $transactions;

    public function __construct(array $configuration, SinkFactory $sinkFactory, Collection $transactions)
    {
        parent::__construct($configuration, $sinkFactory);
        $this->transactions = $transactions;
    }

    public function profile(string $profile_name): Client
    {
        // Si no tenemos transacciones definidas para el perfil que nos piden,
        // devolvemos un cliente normal y corriente.
        if (!$this->transactions->offsetExists($profile_name)) {
            return parent::profile($profile_name);
        }

        // En otro caso, prepararemos un mock parcial que esperará tantas llamadas a requestAsync()
        // como transacciones tengamos definidas para el perfil dado. En el caso de que el mock
        // reconozca los datos de entrada de una transacción concreta, entonces configurará el cliente interno
        // con handler mockeado para que la siguiente petición devuelva o lance el resultado de la transacción
        // y devolveremos lo que devuelva el cliente interno.

        $mockHandler             = new MockHandler();
        $effective_configuration = $this->build_configuration_for_profile($profile_name);
        $guzzleStack             = $this->build_guzzle_stack($effective_configuration);
        $guzzleStack->setHandler($mockHandler);
        $guzzleConfig            = [
            'handler'  => $guzzleStack,
            'base_uri' => $effective_configuration->getBaseUri(),
        ];
        $internal_client = new GuzzleClient($guzzleConfig);

        /** @var GuzzleClient $mocked_internal_client */
        $mocked_internal_client = Mockery::mock($internal_client, function (MockInterface $mock) use ($mockHandler, $internal_client, $profile_name) {
            // Para cada transacción que nos den...
            collect($this->transactions[$profile_name])
                ->each(function (TransactionTestData $test_data) use ($mock, $mockHandler, $internal_client) {
                    $options = [];

                    // Definimos una expectativa que tendrá en cuenta el método y la URI de la petición.
                    $mock
                        ->shouldReceive('requestAsync')
                        ->withArgs(function (string $in_method, string $in_uri, array $in_options) use ($test_data, &$options) {
                            $options = $in_options;
                            return $in_method === $test_data->request->getMethod()
                                && $in_uri    === (string) $test_data->request->getUri();
                        })
                        ->once()
                        ->andReturnUsing(function () use (&$options, $internal_client, $test_data, $mockHandler) {
                            // Para generar la respuesta interfiriendo lo mínimo en el resultado,
                            // usamos el cliente interno con el MockHandler:
                            //  - Limpiamos el mock handler de peticiones previamente configuradas.
                            //  - Añadimos a la cola de respuestas la que se espera para esta transacción.
                            $mockHandler->reset();
                            $mockHandler->append($test_data->response);

                            // Y dejamos que el cliente interno haga lo suyo, pasando por GuzzleMiddleware, sinks
                            // y toda la pesca que hemos añadido al cliente, menos la petición externa, que
                            // la tomará directamente de la cola del MockHandler.
                            return $internal_client->requestAsync(
                                $test_data->request->getMethod(),
                                $test_data->request->getUri(),
                                $options
                            );
                        });
                });
        });

        return new Client(
            $profile_name,
            $mocked_internal_client,
            $effective_configuration
        );
    }
}