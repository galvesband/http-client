<?php

namespace Beat\HttpClient\Tests\Misc;

use Beat\HttpClient\Models\HasHttpRequests;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

/**
 * @property int $id
 */
class FakeUser extends Model implements AuthorizableContract, AuthenticatableContract
{
    use HasHttpRequests, Authorizable, Authenticatable, HasFactory;

    protected $guarded = [];

    protected $table = 'users';

    public static function newFactory(): FakeUserFactory
    {
        return FakeUserFactory::new();
    }
}
