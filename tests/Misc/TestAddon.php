<?php

namespace Beat\HttpClient\Tests\Misc;

use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\RequestBuilder;
use Throwable;

class TestAddon implements BuilderAddon
{
    /**
     * @var callable|Throwable|null
     */
    protected $build_options;

    /** @var callable|Throwable|null */
    protected $apply_to_response = null;
    protected RequestBuilder $builder;

    /**
     * @param callable|Throwable|null $on_build_options
     * @param callable|Throwable|null $on_apply
     */
    public function __construct(RequestBuilder $builder, $on_build_options = null, $on_apply = null)
    {
        $this->build_options     = $on_build_options;
        $this->apply_to_response = $on_apply;
        $this->builder = $builder;
    }

    /** @noinspection PhpUnused */
    public function getBuilder(): RequestBuilder
    {
        return $this->builder;
    }

    /**
     * @throws Throwable
     */
    public function buildOptions(array &$options): void
    {
        if (!is_null($this->build_options)) {
            if (is_callable($this->build_options)) {
                ($this->build_options)($options);
            } else {
                throw $this->build_options;
            }
        }
    }

    /**
     * @throws Throwable
     */
    public function applyToResponse(Response $response): void
    {
        if (!is_null($this->apply_to_response)) {
            if (is_callable($this->apply_to_response)) {
                ($this->apply_to_response)($response);
            } else {
                throw $this->apply_to_response;
            }
        }
    }

    public function testMethod(int $a): int
    {
        return $a + 1;
    }
}
