<?php

namespace Beat\HttpClient\Tests\Misc;

use Beat\HttpClient\ContentExtractors\NoContentExtractor;
use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\Sinks\BaseSink;
use Illuminate\Support\Collection;

class TestSink extends BaseSink
{
    public static function fromConfiguration(
        string                  $name,
        array                   $parameters,
        bool                    $registerSuccessfulRequest,
        bool                    $registerFailedRequest,
        MessageContentExtractor $requestExtractor,
        MessageContentExtractor $responseExtractor
    ): Sink
    {
        return new TestSink(
            $name, $registerSuccessfulRequest, $registerFailedRequest, $requestExtractor, $responseExtractor
        );
    }

    protected Collection $results;

    public function __construct(
        string                  $name,
        bool                    $register_successful_requests = false,
        bool                    $register_failed_requests = false,
        ?MessageContentExtractor $request_content_extractor = null,
        ?MessageContentExtractor $response_content_extractor = null
    ) {
        parent::__construct(
            $name,
            $register_successful_requests,
            $register_failed_requests,
            $request_content_extractor   ?? new NoContentExtractor(),
            $response_content_extractor ?? new NoContentExtractor()
        );

        $this->results = collect([]);
    }

    protected function sink_registrable_data(
        string $label,
        RegistrableRequestData $request_data,
        RegistrableResponseData $response_data
    ): void
    {
        $this->results->push(['label' => $label, 'request' => $request_data, 'response' => $response_data]);
    }

    public function getResults(): Collection
    {
        return $this->results;
    }
}
