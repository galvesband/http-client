<?php

namespace Beat\HttpClient\Tests\Misc;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FakeUserFactory extends Factory
{
    protected $model = FakeUser::class;

    public function definition(): array
    {
        return [
            'name'              => $this->faker->name,
            'email'             => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
        ];
    }
}
