<?php

namespace Beat\HttpClient\Tests\Misc;

use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Psr\Http\Message\RequestInterface;
use Throwable;

class TransactionTestData
{
    public RequestInterface $request;

    /** @var GuzzleResponse|Throwable */
    public                  $response;

    /**
     * @param RequestInterface $request
     * @param GuzzleResponse|Throwable $response
     */
    public function __construct(RequestInterface $request,  $response)
    {
        $this->request  = $request;
        $this->response = $response;
    }
}