<?php

namespace Beat\HttpClient\Jobs;

use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\Models\Request;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveHttpRequestResultInDatabaseJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var bool No eliminar job si algún modelo serializado no existe. */
    public bool            $deleteWhenMissingModels = false;

    /** @var int Intentos (con reintento controlado o no) */
    public int             $tries                   = 10;

    /** @var int Segundos entre intentos por defecto */
    public int             $backoff                 = 30;

    /** @var int Número máximo de intentos que acaban en excepción antes de dar el job por fallido. */
    public int             $maxExceptions           = 3;

    /** @var string Descripción (breve) del objetivo de la petición o de lo que se iba a hacer con los datos obtenidos. */
    public string          $label;

    /** @var string|null Nombre de la clase que implementa el modelo. */
    public ?string         $authenticatable_class;

    /** @var mixed|null Identificador (PK) del modelo. */
    public                 $authenticatable_id;

    /** @var Carbon Momento en el que se hizo la petición. */
    public Carbon          $timestamp;

    /** @var RegistrableRequestData Datos extraídos de la petición. */
    public RegistrableRequestData   $requestData;

    /** @var RegistrableResponseData Datos extraídos de la respuesta */
    public RegistrableResponseData $responseData;

    public function __construct(
        string                  $label,
        RegistrableRequestData  $requestData,
        RegistrableResponseData $responseData,
        ?Model                  $authenticated_model,
        ?Carbon                 $timestamp
    ) {
        $this->label                 = $label;
        $this->requestData           = $requestData;
        $this->responseData          = $responseData;
        $this->authenticatable_class = $authenticated_model ? get_class($authenticated_model) : null;
        $this->authenticatable_id    = optional($authenticated_model)->getQueueableId();
        $this->timestamp             = optional($timestamp)->clone() ?? now();
    }

    public function handle()
    {
        $database_request = Request::fromJobData($this);
        $database_request->save();
    }
}
