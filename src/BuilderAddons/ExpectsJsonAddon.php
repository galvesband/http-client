<?php

namespace Beat\HttpClient\BuilderAddons;

use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\InvalidJsonException;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\ResponseAddons\JsonDataAddon as ResponseJsonDataAddon;
use Closure;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;

/**
 * Añade a la petición la cabecera 'Accept: application/json' y agrega a la respuesta un addon
 * que valida que la respuesta es un JSON y lo decodifica.
 *
 * @see ResponseJsonDataAddon
 */
class ExpectsJsonAddon implements BuilderAddon
{
    protected RequestBuilder $builder;

    protected ?array $rules;
    protected ?Closure $withValidatorCallback;
    protected array $validationMessages;

    public function __construct(
        RequestBuilder $builder,
        ?array         $rules                 = null,
        ?Closure       $withValidatorCallback = null,
        array         $validationMessages     = []
    ) {
        $this->builder               = $builder;
        $this->rules                 = $rules;
        $this->withValidatorCallback = $withValidatorCallback;
        $this->validationMessages    = $validationMessages;
    }

    public function getBuilder(): RequestBuilder
    {
        return $this->builder;
    }

    public function getValidationRules(): ?array
    {
        return $this->rules;
    }

    public function withValidationRules(?array $rules): RequestBuilder
    {
        $this->rules = $rules;
        return $this->builder;
    }

    public function hasValidatorCallback(): bool
    {
        return !is_null($this->withValidatorCallback);
    }

    public function withValidatorCallback(?Closure $closure): RequestBuilder
    {
        $this->withValidatorCallback = $closure;
        return $this->builder;
    }

    public function getValidationMessages(): ?array
    {
        return $this->validationMessages;
    }

    public function withValidationMessages(array $messages): RequestBuilder
    {
        $this->validationMessages = $messages;
        return $this->builder;
    }

    public function buildOptions(array &$options): void
    {
        if (!array_key_exists(GuzzleRequestOptions::HEADERS, $options)) {
            $options[GuzzleRequestOptions::HEADERS] = [];
        }

        if (!array_key_exists('Accept', $options[GuzzleRequestOptions::HEADERS])) {
            $options[GuzzleRequestOptions::HEADERS]['Accept'] = [];
        }

        $options[GuzzleRequestOptions::HEADERS]['Accept'][] = 'application/json';
    }

    /**
     * @param Response $response
     * @return void
     * @throws InvalidJsonException
     */
    public function applyToResponse(Response $response): void
    {
        // Esto indirectamente decodifica la respuesta o lanza una InvalidJsonException.
        $response->pushAddon(new ResponseJsonDataAddon($response));

        // Ahora podemos construir nuestro validador, aplicarlo
        // y lanzar una excepción apropiada dado el caso.
        if (!empty($this->rules) || !is_null($this->withValidatorCallback)) {
            /** @noinspection PhpUndefinedMethodInspection */
            $response->validateJsonData(
                $this->rules, $this->withValidatorCallback, $this->validationMessages
            );
        }
    }
}