<?php

namespace Beat\HttpClient\BuilderAddons;

use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\RequestBuilder;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;

/**
 * Añade un cuerpo en formato JSON a la petición, además de una cabecera Content-Type apropiada.
 */
class JsonDataAddon implements BuilderAddon
{
    protected RequestBuilder $builder;
    protected array          $data;

    public function __construct(RequestBuilder $builder, array $data)
    {
        $this->builder = $builder;
        $this->data    = $data;
    }

    public function getBuilder(): RequestBuilder
    {
        return $this->builder;
    }

    public function setJsonData(array $data): RequestBuilder
    {
        $this->data = $data;
        return $this->builder;
    }

    public function getJsonData(): array
    {
        return $this->data;
    }

    public function buildOptions(array &$options): void
    {
        $options[GuzzleRequestOptions::JSON] = $this->data;

        if (!array_key_exists(GuzzleRequestOptions::HEADERS, $options)) {
            $options[GuzzleRequestOptions::HEADERS] = [];
        }

        if (!array_key_exists('Content-Type', $options[GuzzleRequestOptions::HEADERS])) {
            $options[GuzzleRequestOptions::HEADERS]['Content-Type'] = [];
        }

        $options[GuzzleRequestOptions::HEADERS]['Content-Type'][] = 'application/json';
    }

    public function applyToResponse(Response $response): void
    {}
}