<?php

namespace Beat\HttpClient\Facades;

use Beat\HttpClient\Client;
use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\RequestBuilder;
use Beat\HttpClient\Testing\ClientFactoryFake;
use Beat\HttpClient\Testing\RequestExpectationBuilder;
use Illuminate\Support\Facades\Facade;

/**
 * @method static Client                    profile(string $profileName)
 * @method static RequestBuilder            newRequest(string $label)
 * @method static RequestExpectationBuilder onProfile(string $profileName)
 * @method static void                      closeExpectations()
 */
class HttpClient extends Facade
{
    /**
     * @return ClientFactoryFake
     */
    public static function fake(): ClientFactoryFake
    {
        static::swap($fake = new ClientFactoryFake(app(ClientFactory::class)));

        return $fake;
    }

    protected static function getFacadeAccessor(): string
    {
        return ClientFactory::class;
    }
}