<?php

namespace Beat\HttpClient;

use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\Events\FailedHttpRequestEvent;
use Beat\HttpClient\Events\SuccessfulHttpRequestEvent;
use Beat\HttpClient\Exceptions\ClientException;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\ConnectionException;
use Beat\HttpClient\Exceptions\RequestException;
use Beat\HttpClient\Exceptions\ServerException;
use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Closure;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\ConnectException as GuzzleConnectException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\RejectedPromise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Throwable;

/**
 * Middleware for Guzzle that adds the functionality provided by HttpClient package.
 *
 * The middleware is designed to be placed before the normal guzzle middleware set. It will
 * add bits to the request before it goes through the rest of the middlewares, and transform
 * the result in responses or exceptions defined by the HttpClient package.
 */
class GuzzleMiddleware
{
    /**
     * Encapsulates $error in one of HttpClient exceptions.
     */
    public static function throwableToCommunicationException(
        Throwable $error,
        RequestInterface $request,
        RequestOptions $beatOptions,
        float $elapsedSeconds
    ): CommunicationException
    {
        switch (true) {
            case $error instanceof GuzzleServerException:
                return new ServerException(
                    $request, $error->getResponse(), $error, $beatOptions, $elapsedSeconds
                );

            case $error instanceof GuzzleClientException:
                return new ClientException(
                    $request, $error->getResponse(), $error, $beatOptions, $elapsedSeconds
                );

            case $error instanceof GuzzleConnectException:
                return new ConnectionException(
                    $request, $beatOptions, $elapsedSeconds, $error
                );

            case $error instanceof CommunicationException:
                return $error;

            default:
                return new CommunicationException(
                    $request, $beatOptions, $elapsedSeconds, $error
                );
        }
    }

    protected ProfileConfiguration $profile_configuration;
    protected ?RequestOptions $default_request_options = null;

    public function __construct(ProfileConfiguration $profileConfiguration, ?RequestOptions $defaultRequestOptions = null)
    {
        $this->profile_configuration = $profileConfiguration;
        $this->default_request_options = $defaultRequestOptions;
    }

    public function __invoke(callable $handler): Closure
    {
        return function (RequestInterface $request, array $request_options) use ($handler) {

            // Apply sealer, if present.
            if ($this->profile_configuration->getSealer()) {
                $request = $this->profile_configuration->getSealer()->addAuthToRequest($request);
            }

            // Prepare options for the request.
            $beat_options = $this->get_beat_options_from_request_options($request, $request_options);

            // Let the request pass through the chain.
            $timestamp = microtime(true);
            try {
                /** @var PromiseInterface $result */
                $result = $handler($request, $request_options);
            } catch (\Throwable $error) {
                $result = new RejectedPromise($error);
            }

            return $this->attach_result_postprocess($result, $request, $timestamp, $beat_options);
        };
    }

    protected function attach_result_postprocess(
        PromiseInterface $promise,
        RequestInterface $request,
        float $timestamp,
        RequestOptions $beatOptions
    ): PromiseInterface
    {
        return $promise
            ->then(function (ResponseInterface $guzzle_response) use ($request, $beatOptions, $timestamp) {
                $elapsed_seconds = round(microtime(true) - $timestamp, 3);

                $response = new Response(
                    $beatOptions, $request, $guzzle_response, $elapsed_seconds
                );

                try {
                    foreach ($beatOptions->getBuilderAddons() as $addon) {
                        /** @var BuilderAddon $addon */
                        $addon->applyToResponse($response);
                    }
                } catch (CommunicationException $ce) {
                    throw $ce;
                } catch (Throwable $error) {
                    throw new RequestException(
                        $response->getRequest(), $response->getInternalResponse(),
                        $error, $beatOptions, $response->getTimeInSeconds()
                    );
                }

                event(new SuccessfulHttpRequestEvent($response, $beatOptions));
                return $response;
            })
            ->otherwise(function (Throwable $error) use ($request, $beatOptions, $timestamp) {
                $elapsed_seconds = round(microtime(true) - $timestamp, 3);

                $transformed_error = static::throwableToCommunicationException(
                    $error, $request, $beatOptions, $elapsed_seconds
                );
                event(new FailedHttpRequestEvent($transformed_error, $beatOptions));

                throw $beatOptions->shouldDisableExceptionHandling()
                    ? $error
                    : $transformed_error;
            }
        );
    }

    /**
     * Extracts Beat's options object from the request, or throws an exception otherwise.
     *
     * Requests made through this middleware should have a 'beat' key in its options array,
     * with an instance of RequestOptions.
     *
     * @param RequestInterface $request
     * @param array $requestOptions
     * @return RequestOptions
     * @See Client
     * @see RequestBuilder
     */
    protected function get_beat_options_from_request_options(RequestInterface $request, array &$requestOptions): RequestOptions
    {
        if (!array_key_exists('beat', $requestOptions) && !$this->default_request_options) {
            throw new RuntimeException(sprintf(
                "Http request trough '%s' without 'beat' options detected: '%s'.",
                get_class($this),
                $request->getMethod() . ' ' . $request->getUri()
            ));
        }

        /** @var RequestOptions|mixed $beat_options */
        $beat_options = $requestOptions['beat'] ?? $this->default_request_options;
        if (!($beat_options instanceof RequestOptions)) {
            throw new RuntimeException(sprintf(
                "Http request through '%s' with 'beat' options of unexpected type ('%s'): %s.",
                get_class($this),
                is_object($beat_options) ? get_class($beat_options) : gettype($beat_options),
                $request->getMethod() . ' ' . $request->getUri()
            ));
        }

        unset($requestOptions['beat']);
        return $beat_options;
    }
}