<?php

namespace Beat\HttpClient;

use Beat\HttpClient\Contracts\SealProvider;
use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use GuzzleHttp\Client as GuzzleClient;

class Client
{
    public const HTTP_METHODS = ['GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE', 'PATCH'];

    protected string               $profile_name;
    protected GuzzleClient         $client;
    protected ProfileConfiguration $configuration;

    public function __construct(
        string               $profile_name,
        GuzzleClient         $client,
        ProfileConfiguration $configuration
    ) {
        $this->profile_name  = $profile_name;
        $this->client        = $client;
        $this->configuration = $configuration;
    }

    public function getProfileName(): string
    {
        return $this->profile_name;
    }

    public function getSealer(): ?SealProvider
    {
        return $this->configuration->getSealer();
    }

    public function getConfiguration(): ProfileConfiguration
    {
        return $this->configuration;
    }

    public function getInternalClient(): GuzzleClient
    {
        return $this->client;
    }

    public function newRequest(string $label): RequestBuilder
    {
        return (new RequestBuilder($this, $label))
            ->setTimeouts($this->configuration->getConnectTimeout(), $this->configuration->getTimeout())
            ->userAgent($this->configuration->getUserAgent())
            ->pushSinks($this->configuration->getSinks());
    }
}
