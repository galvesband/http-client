<?php

namespace Beat\HttpClient\Sinks;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\Jobs\SaveHttpRequestResultInDatabaseJob;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use RuntimeException;

/**
 * Despacha un job en la cola configurada para guardar los datos de peticiones en una table en BD.
 */
class DatabaseSink extends BaseSink
{
    /** @inheritDoc */
    public static function fromConfiguration(
        string                  $name,
        array                   $parameters,
        bool                    $registerSuccessfulRequest,
        bool                    $registerFailedRequest,
        MessageContentExtractor $requestExtractor,
        MessageContentExtractor $responseExtractor
    ): Sink
    {
        if (!array_key_exists('queue', $parameters)) {
            throw new RuntimeException(sprintf(
                "La configuración del Sink '%s' no contiene parámetro 'queue'.",
                $name
            ));
        }

        return new static(
            $name, $registerSuccessfulRequest, $registerFailedRequest,
            $requestExtractor, $responseExtractor, $parameters['queue']
        );
    }

    protected string $queue;

    public function __construct(
        string                  $name,
        bool                    $register_successful_requests,
        bool                    $register_failed_requests,
        MessageContentExtractor $request_content_extractor,
        MessageContentExtractor $response_content_extractor,
        string                  $queue
    ) {
        parent::__construct(
            $name, $register_successful_requests, $register_failed_requests,
            $request_content_extractor, $response_content_extractor
        );
        $this->queue = $queue;
    }

    public function getQueueName(): string
    {
        return $this->queue;
    }

    protected function sink_registrable_data(
        string                  $label,
        RegistrableRequestData  $request_data,
        RegistrableResponseData $response_data
    ): void
    {
        /** @var Authenticatable|Model|null $authenticated_user */
        $authenticated_user = Auth::user();

        SaveHttpRequestResultInDatabaseJob::dispatch(
            $label,
            $request_data,
            $response_data,
            $authenticated_user,
            $request_data->getTimestamp()
        )->onQueue($this->queue);
    }
}
