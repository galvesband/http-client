<?php

namespace Beat\HttpClient\Sinks;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\RequestException;
use LogicException;

abstract class BaseSink implements Sink
{
    protected string $name;

    protected bool $register_successful_requests;
    protected MessageContentExtractor $request_content_extractor;

    protected bool $register_failed_requests;
    protected MessageContentExtractor $response_content_extractor;

    protected function __construct(
        string                  $name,
        bool                    $register_successful_requests,
        bool                    $register_failed_requests,
        MessageContentExtractor $request_content_extractor,
        MessageContentExtractor $response_content_extractor
    ) {
        $this->name                         = $name;
        $this->register_successful_requests = $register_successful_requests;
        $this->register_failed_requests     = $register_failed_requests;
        $this->request_content_extractor    = $request_content_extractor;
        $this->response_content_extractor   = $response_content_extractor;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function registersSuccessfulRequests(): bool
    {
        return $this->register_successful_requests;
    }

    public function registersFailedRequests(): bool
    {
        return $this->register_failed_requests;
    }

    public function getRequestContentExtractor(): MessageContentExtractor
    {
        return $this->request_content_extractor;
    }

    public function getResponseContentExtractor(): MessageContentExtractor
    {
        return $this->response_content_extractor;
    }

    /**
     * @inheritDoc
     */
    public function processResult($requestResult): void
    {
        switch (true) {
            case $requestResult instanceof Response:
                if (!$this->should_register_successful_request()) {
                    return;
                }

                $label = $requestResult->getRequestOptions()->getLabel();
                $request_data = new RegistrableRequestData(
                    $requestResult->getRequest()->getMethod(),
                    $requestResult->getRequest()->getUri(),
                    $requestResult->getRequest()->getHeaders(),
                    $this->request_content_extractor->extractContent($requestResult->getRequest())
                );
                $response_data = new RegistrableResponseData(
                    $requestResult->getStatusCode(),
                    $this->response_content_extractor->extractContent($requestResult),
                    $requestResult->getHeaders(),
                    $requestResult->getTimeInSeconds()
                );
                break;

            case $requestResult instanceof CommunicationException:
                if (!$this->should_register_failed_request()) {
                    return;
                }

                $label = $requestResult->getRequestOptions()->getLabel();
                $request_data = new RegistrableRequestData(
                    $requestResult->getRequest()->getMethod(),
                    $requestResult->getRequest()->getUri(),
                    $requestResult->getRequest()->getHeaders(),
                    $this->request_content_extractor->extractContent($requestResult->getRequest())
                );
                if ($requestResult instanceof RequestException) {
                    $response_data = new RegistrableResponseData(
                        $requestResult->getResponse()->getStatusCode(),
                        $this->response_content_extractor->extractContent($requestResult->getResponse()),
                        $requestResult->getResponse()->getHeaders(),
                        $requestResult->getTimeInSeconds()
                    );
                } else {
                    $response_data = RegistrableResponseData::noResponseData($requestResult->getTimeInSeconds());
                }
                break;

            default:
                throw new LogicException(sprintf(
                    "RequestResult ('%s') no es ni una respuesta ni una CommunicationException!",
                    is_object($requestResult) ? get_class($requestResult) : gettype($requestResult)
                ));
        }

        $this->sink_registrable_data($label, $request_data, $response_data);
    }

    /**
     * Indica si una respuesta correcta debe ser registrada por el sink.
     *
     * Normalmente, registraremos las respuestas exitosas según la opción directa.
     *
     * @return bool
     */
    protected function should_register_successful_request(): bool
    {
        return $this->register_successful_requests;
    }

    /**
     * Indica si una respuesta fallida debe ser registrada por el sink.
     *
     * Registraremos respuestas fallidas si hay que registrar respuestas correctas
     * o nos dicen explícitamente que debemos registrar respuestas fallidas. Lo hacemos
     * así porque no parece tener mucho sentido registrar las respuestas correctas pero
     * no las fallidas.
     *
     * @return bool
     */
    protected function should_register_failed_request(): bool
    {
        return $this->register_failed_requests || $this->should_register_successful_request();
    }

    abstract protected function sink_registrable_data(
        string                  $label,
        RegistrableRequestData  $request_data,
        RegistrableResponseData $response_data
    ): void;
}
