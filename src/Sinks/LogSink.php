<?php

namespace Beat\HttpClient\Sinks;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RegistrableRequestData;
use Beat\HttpClient\DataTransferObjects\RegistrableResponseData;
use Illuminate\Support\Facades\Log;
use RuntimeException;

/**
 * Escribe, en el canal de log configurado, datos de las peticiones y respuestas.
 *
 * TODO: Plantillas de mensaje de log configurables?
 * TODO: Más opciones de qué se mete en el log en cada caso (success y fail)?
 *  Por ejemplo, al logear success, no incluir contenido.
 */
class LogSink extends BaseSink
{
    /** @inheritDoc */
    public static function fromConfiguration(
        string                  $name,
        array                   $parameters,
        bool                    $registerSuccessfulRequest,
        bool                    $registerFailedRequest,
        MessageContentExtractor $requestExtractor,
        MessageContentExtractor $responseExtractor
    ): Sink
    {
        if (!array_key_exists('log_channel', $parameters)) {
            throw new RuntimeException(sprintf(
                "La configuración del Sink '%s' no contiene parámetro 'log_channel'.",
                $name
            ));
        }

        return new static(
            $name, $registerSuccessfulRequest,$registerFailedRequest,
            $requestExtractor, $responseExtractor, $parameters['log_channel']
        );
    }

    protected string $log_channel;

    public function __construct(
        string                  $name,
        bool                    $register_successful_requests,
        bool                    $register_failed_requests,
        MessageContentExtractor $request_content_extractor,
        MessageContentExtractor $response_content_extractor,
        string                  $log_channel
    ) {
        parent::__construct(
            $name, $register_successful_requests, $register_failed_requests,
            $request_content_extractor, $response_content_extractor
        );
        $this->log_channel = $log_channel;
    }

    public function getLogChannelName(): string
    {
        return $this->log_channel;
    }

    protected function sink_registrable_data(
        string                  $label,
        RegistrableRequestData  $request_data,
        RegistrableResponseData $response_data
    ): void
    {
        $successful_response = $response_data->getStatus() >= 200 && $response_data->getStatus() < 300;
        $method = $successful_response ? 'info' : 'warning';
        $message_template = $successful_response
            ? 'OK: _LABEL_ (_METHOD_ _URI_) answered _STATUS_ code in _TIME_S_ s.'
            : 'ER: _LABEL_ (_METHOD_ _URI_) failed with _STATUS_ code in _TIME_S_ s.';

        Log::channel($this->log_channel)->$method(
            str_replace([
                '_LABEL_', '_METHOD_', '_URI_', '_STATUS_', '_TIME_S_',
            ], [
                $label, $request_data->getMethod(), $request_data->getUrl(),
                $response_data->getStatus(), $response_data->getTimeInSeconds()
            ], $message_template),
            [
                'request'  => $request_data->toArray(),
                'response' => $response_data->toArray(),
            ]
        );
    }
}
