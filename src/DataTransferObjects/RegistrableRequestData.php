<?php

namespace Beat\HttpClient\DataTransferObjects;

use Carbon\Carbon;

class RegistrableRequestData extends BaseRegistrableData
{
    protected string $method;
    protected string $url;
    protected Carbon $timestamp;

    public function __construct(
        string $method,
        string $uri,
        array  $headers,
        string $content,
        ?Carbon $timestamp = null
    )
    {
        parent::__construct($headers, $content);
        $this->method    = $method;
        $this->url       = $uri;
        $this->timestamp = $timestamp ?? now();
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): RegistrableRequestData
    {
        $this->method = $method;
        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): RegistrableRequestData
    {
        $this->url = $url;
        return $this;
    }

    public function getTimestamp(): Carbon
    {
        return $this->timestamp;
    }

    public function setTimestamp(Carbon $timestamp): void
    {
        $this->timestamp = $timestamp->clone();
    }

    public function toArray(): array
    {
        return array_merge([
            'method'    => $this->method,
            'uri'       => $this->url,
            'timestamp' => $this->timestamp->format('Y-m-d H:i:s'),
        ], parent::toArray());
    }
}
