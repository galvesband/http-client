<?php

namespace Beat\HttpClient\DataTransferObjects;

use BadMethodCallException;
use Beat\HttpClient\Client;
use Beat\HttpClient\Contracts\ResponseAddon;
use Illuminate\Support\Collection;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class Response implements ResponseInterface
{
    protected RequestInterface  $request;
    protected ResponseInterface $internal_response;
    protected float             $time_in_seconds;
    protected Client            $client;
    protected RequestOptions    $beat_options;
    protected Collection        $add_ons;

    public function __construct(
        RequestOptions    $beat_options,
        RequestInterface  $request,
        ResponseInterface $internalResponse,
        float             $time_in_seconds
    ) {
        $this->beat_options      = $beat_options;
        $this->request           = $request;
        $this->internal_response = $internalResponse;
        $this->time_in_seconds   = $time_in_seconds;
        $this->add_ons           = collect([]);
    }

    public function pushAddon(ResponseAddon $addon): Response
    {
        $this->add_ons->push($addon);
        return $this;
    }

    public function getTimeInSeconds(): float
    {
        return $this->time_in_seconds;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    public function getInternalResponse(): ResponseInterface
    {
        return $this->internal_response;
    }

    public function getContentType(): string
    {
        return $this->internal_response->getHeader('Content-Type')[0] ?? '';
    }

    public function getRequestOptions(): RequestOptions
    {
        return $this->beat_options;
    }

    public function __call($name, $arguments)
    {
        foreach ($this->add_ons as $add_on) {
            /** @var ResponseAddon $add_on */
            if (method_exists($add_on, $name)) {
                return $add_on->$name(...$arguments);
            }
        }

        throw new BadMethodCallException(sprintf(
            "Método %s no encontrado en %s ni en ninguno de sus add-ons.",
            $name, get_class($this)
        ));
    }

    /* ********************************* */
    /* RESPONSE INTERFACE IMPLEMENTATION */
    /* ********************************* */

    public function getProtocolVersion(): string
    {
        return $this->internal_response->getProtocolVersion();
    }

    public function withProtocolVersion(string $version): MessageInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withProtocolVersion($version),
            $this->time_in_seconds
        );
    }

    public function getHeaders(): array
    {
        return $this->internal_response->getHeaders();
    }

    public function hasHeader(string $name): bool
    {
        return $this->internal_response->hasHeader($name);
    }

    public function getHeader(string $name): array
    {
        return $this->internal_response->getHeader($name);
    }

    public function getHeaderLine(string $name): string
    {
        return $this->internal_response->getHeaderLine($name);
    }

    public function withHeader(string $name, $value): MessageInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withHeader($name, $value),
            $this->time_in_seconds
        );
    }

    public function withAddedHeader(string $name, $value): MessageInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withAddedHeader($name, $value),
            $this->time_in_seconds
        );
    }

    public function withoutHeader(string $name): MessageInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withoutHeader($name),
            $this->time_in_seconds
        );
    }

    public function getBody(): StreamInterface
    {
        return $this->internal_response->getBody();
    }

    public function withBody(StreamInterface $body): MessageInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withBody($body),
            $this->time_in_seconds
        );
    }

    public function getStatusCode(): int
    {
        return $this->internal_response->getStatusCode();
    }

    public function withStatus(int $code, string $reasonPhrase = ''): ResponseInterface
    {
        return new Response(
            $this->beat_options,
            $this->request,
            $this->internal_response->withStatus($code, $reasonPhrase),
            $this->time_in_seconds
        );
    }

    public function getReasonPhrase(): string
    {
        return $this->internal_response->getReasonPhrase();
    }
}