<?php

namespace Beat\HttpClient\DataTransferObjects;

use Illuminate\Support\Collection;

/**
 * Stores data that should go troughs Guzzle with the request.
 *
 * It contains necessary information for GuzzleMiddleware to do his job.
 */
class RequestOptions
{
    protected string     $label;
    protected Collection $builder_add_ons;
    protected Collection $sinks;
    protected bool       $disable_exception_handling = false;

    public function __construct(
        string      $label                      = 'Unknown label',
        ?Collection $builder_addons             = null,
        ?Collection $sinks                      = null,
        bool        $disable_exception_handling = false
    ) {
        $this->label                      = $label;
        $this->builder_add_ons            = $builder_addons ?? collect([]);
        $this->sinks                      = $sinks          ?? collect([]);
        $this->disable_exception_handling = $disable_exception_handling;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): RequestOptions
    {
        $this->label = $label;
        return $this;
    }

    public function withBuilderAddons(Collection $collection): RequestOptions
    {
        $this->builder_add_ons = collect($collection);
        return $this;
    }

    public function getBuilderAddons(): Collection
    {
        return $this->builder_add_ons;
    }

    public function withSinks(Collection $sinks): RequestOptions
    {
        $this->sinks = $sinks;
        return $this;
    }

    public function getSinks(): Collection
    {
        return $this->sinks;
    }

    public function shouldDisableExceptionHandling(): bool
    {
        return $this->disable_exception_handling;
    }
}
