<?php

namespace Beat\HttpClient\DataTransferObjects;

class RegistrableResponseData extends BaseRegistrableData
{
    public static function noResponseData(float $timeInSeconds): RegistrableResponseData
    {
        return new static(
            0, '', [], $timeInSeconds
        );
    }

    protected int    $status;
    protected float  $time_in_seconds;

    public function __construct(
        int    $status,
        string $content,
        array  $headers,
        float  $time_in_seconds
    ) {
        parent::__construct($headers, $content);

        $this->status          = $status;
        $this->setTimeInSeconds($time_in_seconds);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): RegistrableResponseData
    {
        $this->status = $status;
        return $this;
    }

    public function getTimeInSeconds(): float
    {
        return $this->time_in_seconds;
    }

    public function setTimeInSeconds(float $time_in_seconds): RegistrableResponseData
    {
        // We try to keep time_in_seconds always inside database range for 'duration' field.
        // 'duration' is defined as an unsigned float (10,3).
        $this->time_in_seconds = min(max($time_in_seconds, 0), 999999);
        return $this;
    }

    public function toArray(): array
    {
        return array_merge([
            'status' => $this->status,
            'time_s' => $this->time_in_seconds,
        ], parent::toArray());
    }
}
