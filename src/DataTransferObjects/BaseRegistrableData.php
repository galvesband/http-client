<?php

namespace Beat\HttpClient\DataTransferObjects;

use Illuminate\Contracts\Support\Arrayable;

abstract class BaseRegistrableData implements Arrayable
{
    protected array  $headers = [];
    protected string $content = '';

    public function __construct(array $headers, string $content)
    {
        $this->headers = $headers;
        $this->content = $content;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeaders(array $headers): BaseRegistrableData
    {
        $this->headers = $headers;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): BaseRegistrableData
    {
        $this->content = $content;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'headers' => $this->headers,
            'content' => $this->content,
        ];
    }
}