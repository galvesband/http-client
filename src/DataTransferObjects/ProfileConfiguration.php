<?php

namespace Beat\HttpClient\DataTransferObjects;

use Beat\HttpClient\Contracts\SealProvider;
use Illuminate\Support\Collection;

/**
 * Encapsula la configuración de un perfil Http.
 */
class ProfileConfiguration
{
    public const DEFAULT_USER_AGENT = 'Beat/HttpClient 0.1 (GuzzleHttp 7)';

    public static function fromArray(array $configuration, ?SealProvider $sealer, ?Collection $sinks): ProfileConfiguration
    {
        $retVal = new static;

        $retVal->base_uri          = $configuration['base_uri']          ?? $retVal->base_uri;
        $retVal->connect_timeout   = $configuration['connect_timeout']   ?? $retVal->connect_timeout;
        $retVal->timeout           = $configuration['timeout']           ?? $retVal->timeout;
        $retVal->user_agent        = $configuration['user_agent']        ?? $retVal->user_agent;
        $retVal->sealer            = $sealer                             ?? $retVal->sealer;
        $retVal->sinks             = $sinks                              ?? collect([]);

        return $retVal;
    }

    /**
     * URL base. Ante la duda, ver RFC 3986 donde se especifica el comportamiento del
     * cliente HTTP cuando se combina una url base y un URI en la propia petición.
     * Como leerse el RFC es un coñazo, sirva esto, sacado de la documentación de Guzzle:
     *
     * | base_uri            | URI            | Result                  |
     * | ---                 | ---            | ---                     |
     * | http://foo.com      | /bar           | http://foo.com/bar      |
     * | http://foo.com/foo  | /bar           | http://foo.com/bar      |
     * | http://foo.com/foo  | bar            | http://foo.com/bar      |
     * | http://foo.com/foo/ | bar            | http://foo.com/foo/bar  |
     * | http://foo.com      | http://baz.com | http://baz.com          |
     * | http://foo.com/?bar | bar            | http://foo.com/bar      |
     *
     * @var string
     */
    protected string        $base_uri          = '';

    /**
     * Timeout de conexión, en segundos.
     *
     * @var int
     */
    protected int           $connect_timeout   = 10;

    /**
     * Timeout para recibir respuesta, en segundos.
     *
     * @var int
     */
    protected int           $timeout           = 20;

    /**
     * UserAgent utilizado en las peticiones.
     *
     * @var string
     */
    protected string        $user_agent        = 'Beat/HttpClient 0.1 (GuzzleHttp 7)';

    /**
     * Proveedor de autenticación para peticiones externas.
     *
     * La factoría de clientes construirá el SealProvider apropiado y lo añadirá a la configuración
     * cuando necesite generar un perfil, basándose en la configuración 'sealer' del array de configuración.
     * Dicho 'sealer' debe ser el nombre de un SealProvider definido en 'seal_providers' en beat_httpclient.php.
     *
     * @var SealProvider|null
     */
    protected ?SealProvider $sealer            = null;

    protected Collection    $sinks;

    public function __construct()
    {
        $this->sinks = collect([]);
    }

    public function getBaseUri(): string
    {
        return $this->base_uri;
    }

    public function setBaseUri(string $base_uri): ProfileConfiguration
    {
        $this->base_uri = $base_uri;
        return $this;
    }

    public function getConnectTimeout(): int
    {
        return $this->connect_timeout;
    }

    public function setConnectTimeout(int $connect_timeout): ProfileConfiguration
    {
        $this->connect_timeout = $connect_timeout;
        return $this;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function setTimeout(int $timeout): ProfileConfiguration
    {
        $this->timeout = $timeout;
        return $this;
    }

    public function getSealer(): ?SealProvider
    {
        return $this->sealer;
    }

    public function setSealer(?SealProvider $sealer): ProfileConfiguration
    {
        $this->sealer = $sealer;
        return $this;
    }

    public function getUserAgent(): string
    {
        return $this->user_agent;
    }

    public function setUserAgent(string $user_agent): ProfileConfiguration
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    public function getSinks(): Collection
    {
        return $this->sinks;
    }
}
