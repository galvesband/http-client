<?php

namespace Beat\HttpClient\ResponseAddons;

use Beat\HttpClient\Contracts\ResponseAddon;
use Beat\HttpClient\Exceptions\InvalidJsonException;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\ResponseValidationException;
use Closure;
use Illuminate\Support\Facades\Validator;

class JsonDataAddon implements ResponseAddon
{
    protected array $data;
    protected Response $response;

    /**
     * @param Response $response
     * @throws InvalidJsonException
     */
    public function __construct(Response $response)
    {
        $this->response = $response;

        $data = json_decode((string) $response->getBody(), true);
        if (is_null($data)) {
            throw new InvalidJsonException(
                $response->getRequest(),
                $response->getInternalResponse(),
                null,
                $response->getRequestOptions(),
                $response->getTimeInSeconds(),
                null,
                502,
                "La respuesta no es un JSON válido."
            );
        }

        $this->data = $data;
    }

    public function getJsonData(): array
    {
        return $this->data;
    }

    /**
     * @param array|null $rules
     * @param Closure|null $validatorCallback
     * @param array|null $messages
     * @return Response
     * @throws ResponseValidationException
     */
    public function validateJsonData(?array $rules = null, ?Closure $validatorCallback = null, array $messages = []): Response
    {
        $validator = Validator::make($this->data, $rules, $messages);
        if (!is_null($validatorCallback)) {
            $validator->after($validatorCallback);
        }

        if ($validator->fails()) {
            throw new ResponseValidationException(
                $this->response->getRequest(),
                $this->response->getInternalResponse(),
                $this->response->getRequestOptions(),
                $validator->errors()->toArray(),
                $this->response->getTimeInSeconds(),
                null,
                502,
                "Los datos de la respuesta no son válidos."
            );
        }

        return $this->response;
    }
}