<?php

namespace Beat\HttpClient\Commands;

use Beat\HttpClient\Models\Request;
use Carbon\Carbon;
use Illuminate\Console\Command;
use LogicException;
use RuntimeException;

class PruneRequests extends Command
{
    protected $signature = 'beat:http-client:prune ' .
        '{--by-date= : Elimina peticiones anteriores a una fecha dada (YYYY-MM-DD).} ' .
        '{--by-days= : Elimina peticiones con más de los días dados de antigüedad.}' .
        '{--by-count= : Elimina peticiones empezando por las más antiguas hasta dejar el número de peticiones dado.} ';

    protected $description = 'Elimina peticiones registradas por antigüedad o número.';

    protected $help = <<< EOT
Permite mantener un control sobre el número de peticiones almacenadas en base de datos.
 
Aplicaciones con gran volúmen de peticiones pueden acumular una cantidad de datos peligrosa para los límites
del hardware en el que se ejecuta la aplicación. Este comando elimina peticiones antiguas basándose en uno de 
los siguientes criterios:

 - Por fecha (parámetro --by-date (YYYY-MM-DD): Peticiones con fecha anterior a la fecha dada serán eliminadas.
   Ejemplo que elimina peticiones anteriores al 21 de Agosto de 2023: 
     
     beat:http-client:prune --by-date 2023-08-21
 
 - Por días de antigüedad (parámetro --by-days): Las peticiones que tengan más de los días dados de antigüedad
   serán eliminadas. 
   Ejemplo que conserva solo peticiones ocurridas en los 14 últimos días:
   
     beat:http-client:prune --by-days=14
   
 - Por número de peticiones (parámetro --by-count): Se conservarán el número de peticiones dado más recientes. 
   El resto serán eliminadas. No es exacto, solo aproximado; si la petición bisagra se realizó en el mismo 
   segundo que muchas otras peticiones, el número de peticiones conservada puede variar del esperado. 
   Ejemplo que conserva solo las 15000 peticiones más recientes:
   
     beat:http-client:prune --by-count=15000
     
Se recomienda programar la ejecución de este comando periódicamente (diaría o semanalmente, por ejemplo) con
un criterio apropiado para el caso de uso de la aplicación y la trazabilidad deseada. 

También es posible reducir la cantidad de datos almacenada en BD modificando la configuración de los 
extractores de contenido de los perfiles utilizados al hacer peticiones externas. Consulte documentación
y configuración de Beat/HttpClient para más detalles. 
EOT;

    /**
     * Pruning por fecha. Se eliminarán peticiones con fecha anterior al parámetro.
     */
    const MODE_DATE = 'by-date';
    /**
     * Pruning por días de antigüedad. Se conservarán peticiones hechas en los últimos $param días.
     */
    const MODE_DAYS = 'by-days';
    /**
     * Pruning por número de peticiones. Se conservará un número de peticiones cercano a $param.
     */
    const MODE_COUNT = 'by-count';
    const MODES = [
        self::MODE_DATE, self::MODE_DAYS, self::MODE_COUNT,
    ];

    public function handle(): int
    {
        /**
         * @var string $mode Modo de funcionamiento.
         * @see self::MODES
         * @var Carbon|int $param
         */
        list($mode, $param) = $this->parse_mode();
        $this->line("Iniciando pruning en modo $mode...");

        $deletedRows = $this->pruneBy($mode, $param);

        $this->info("Se han eliminado $deletedRows registros.");
        return 0;
    }

    protected function parse_mode(): array
    {
        switch (true) {
            case $this->option(self::MODE_DATE) !== null:
                $raw_date = $this->option(self::MODE_DATE);
                $valid_date = Carbon::hasFormatWithModifiers($raw_date, 'Y-m-d')
                    && Carbon::canBeCreatedFromFormat($raw_date, 'Y-m-d');
                if (!$valid_date) {
                    throw new RuntimeException("Fecha con formato desconocido (debe ser YYYY-MM-DD): " . $raw_date);
                }
                return [self::MODE_DATE, Carbon::createFromFormat('Y-m-d', $raw_date)];

            case $this->option(self::MODE_DAYS) !== null:
            case $this->option(self::MODE_COUNT) !== null:
                $mode = $this->option(self::MODE_DAYS) !== null ? self::MODE_DAYS : self::MODE_COUNT;
                $raw_number = $this->option($mode);
                if (!is_numeric($raw_number) || ($days = intval($raw_number)) <= 0) {
                    throw new RuntimeException("Número inválido (debe ser un número entero mayor o igual a 1): " . $raw_number);
                }
                return [$mode, $days];

            default:
                throw new RuntimeException("Debes especificar un criterio (--by-date, --by-days o --by-count).");
        }
    }

    protected function pruneBy(string $mode, $param): int
    {
        $query = Request::query();

        switch ($mode) {
            case self::MODE_DATE:
                $query->where('timestamp', '<', $param);
                break;

            case self::MODE_DAYS:
                $query->where('timestamp', '<', now()->subDays($param));
                break;

            case self::MODE_COUNT:
                // Hay que determinar la fecha de la petición $param ordenando por fecha descendente.
                // Borraremos peticiones con fecha menor a la fecha encontrada. Podríamos usar ID
                // con mayor exactitud, pero a medio plazo probablemente sustituya la PK numérica
                // por una PK basada en UUID, y ahí ya no nos valdría.
                $date = $this->get_nth_request_date_ordered_by_date_desc($param);
                if ($date === false) {
                    // Si devuelve false es que no debe haber $param peticiones. Nada que borrar.
                    return 0;
                }

                $query->where('timestamp', '<', $date);
                break;

            default:
                throw new LogicException("Modo de funcionamiento desconocido! '$mode'.");
        }

        return $query->delete();
    }

    /**
     * @return Carbon|false
     */
    protected function get_nth_request_date_ordered_by_date_desc(int $count)
    {
        /** @var Request|null $threshold_request */
        $threshold_request = Request::query()
            ->orderByDesc('timestamp')
            ->offset($count - 1)
            ->first();

        return optional($threshold_request)->timestamp ?? false;
    }
}