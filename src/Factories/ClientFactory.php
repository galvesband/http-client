<?php

namespace Beat\HttpClient\Factories;

use Beat\HttpClient\Client;
use Beat\HttpClient\Contracts\SealProvider;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\GuzzleMiddleware;
use Beat\HttpClient\DataTransferObjects\ProfileConfiguration;
use Beat\HttpClient\RequestBuilder;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Collection;
use RuntimeException;

/**
 * @method RequestBuilder newRequest(string $label)
 */
class ClientFactory
{
    protected array       $configuration;
    protected SinkFactory $sink_factory;

    protected Collection  $instances;

    public function __construct(array $configuration, SinkFactory $sinkFactory)
    {
        $this->configuration = $configuration;
        $this->sink_factory  = $sinkFactory;
        $this->instances     = collect([]);
    }

    public function profile(string $profile_name): Client
    {
        if (!$this->instances->offsetExists($profile_name)) {
            $this->instances->offsetSet(
                $profile_name,
                $this->build_client($profile_name)
            );
        }

        return $this->instances->get($profile_name);
    }

    public function buildGuzzleClient(string $profileName): GuzzleClient
    {
        return $this->profile($profileName)->getInternalClient();
    }

    protected function build_client(string $profileName): Client
    {
        $effective_configuration = $this->build_configuration_for_profile($profileName);
        $internal_client = $this->build_guzzle_client($effective_configuration);

        return new Client($profileName, $internal_client, $effective_configuration);
    }

    protected function build_guzzle_client(ProfileConfiguration $configuration): GuzzleClient
    {
        return new GuzzleClient([
            'handler' => $this->build_guzzle_stack(
                $configuration,
                // These are the request options applied to request made directly
                // to the internal client, without passing through HttpClient.
                new RequestOptions(
                    'Unlabeled request', collect([]), $configuration->getSinks(), true
                )
            ),
            'base_uri' => $configuration->getBaseUri(),
        ]);
    }

    protected function build_guzzle_stack(
        ProfileConfiguration $profile_options,
        ?RequestOptions $default_request_options = null
    ): HandlerStack
    {
        $stack = HandlerStack::create();

        /*
         * El orden de los middlewares dentro del cliente interno es MUY IMPORTANTE.
         *
         * Lo normal y lo que sugieren en la documentación de Guzzle es usar el método push(), pero eso
         * significa que nos ejecutamos después de la preparación de los otros middlewares y antes del
         * post-procesado de la respuesta de los mismos. No es lo que queremos, entre otras cosas porque
         * hay un middleware de guzzle que convierte las respuestas con código >= 400 en excepciones y
         * nuestro GuzzleMiddleware cuenta con ello.
         *
         * Así que usamos el método before(), y plantamos nuestro middleware el primero. Eso significa
         * que nuestra preparación es anterior a la de los middleware, y nuestro post-procesado es
         * el último (tras los middlewares de Guzzle).
         */
        $stack->before(
            'http_errors',
            new GuzzleMiddleware($profile_options, $default_request_options),
            "Beat/HttpClient"
        );

        return $stack;
    }

    public function __call($name, $arguments)
    {
        return $this->profile('default')->$name(...$arguments);
    }

    protected function build_configuration_for_profile(string $profile_name): ProfileConfiguration
    {
        if (!array_key_exists($profile_name, $this->configuration['profiles'])) {
            throw new RuntimeException("Perfil de HttpClient desconocido: '$profile_name'.");
        }

        $default_configuration   = $this->configuration['profile_defaults'];
        $profile_configuration   = $this->configuration['profiles'][$profile_name];
        $effective_configuration = array_merge($default_configuration, $profile_configuration);
        $sealer                  = $this->build_sealer($effective_configuration['seal']);
        $sinks                   = collect($effective_configuration['sinks'])
            ->map(function ($sink_name) {
                return $this->sink_factory->buildSink($sink_name);
            });

        return ProfileConfiguration::fromArray($effective_configuration, $sealer, $sinks);
    }

    protected function build_sealer(?string $sealer_name): ?SealProvider
    {
        if (is_null($sealer_name)) {
            return null;
        }

        if (!array_key_exists($sealer_name, $this->configuration['seal_providers'])) {
            throw new RuntimeException("Sealer desconocido: $sealer_name.");
        }

        return app(
            $this->configuration['seal_providers'][$sealer_name]['class'],
            $this->configuration['seal_providers'][$sealer_name]['params']
        );
    }
}
