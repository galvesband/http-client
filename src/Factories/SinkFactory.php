<?php

namespace Beat\HttpClient\Factories;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Beat\HttpClient\Contracts\Sink;
use RuntimeException;
use Throwable;

class SinkFactory
{
    protected array $configuration;

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    public function buildSink(string $sinkName): Sink
    {
        if (!array_key_exists($sinkName, $this->configuration)) {
            throw new RuntimeException(sprintf(
                'Configuración de sink con nombre \'%s\' no encontrada.',
                $sinkName
            ));
        }
        $sink_configuration = $this->configuration[$sinkName];
        $sink_class         = $sink_configuration['class'];
        $sink_params        = $sink_configuration['params'] ?? [];

        $request_extractor  = $this->build_content_extractor($sinkName, $sink_configuration['request_content_extractor']);
        $response_extractor = $this->build_content_extractor($sinkName, $sink_configuration['response_content_extractor']);

        return app($sink_class, array_merge($sink_params, [
            'name'                       => $sinkName,
            'request_content_extractor'  => $request_extractor,
            'response_content_extractor' => $response_extractor,
        ]));
    }

    protected function build_content_extractor(string $sink_name, array $configuration): MessageContentExtractor
    {
        try {
            return app($configuration['class'], $configuration['params'] ?? []);
        } catch (Throwable $t) {
            throw new RuntimeException(sprintf(
                "No es posible construir un extractor de contenido para el Sink '%s': '%s'.",
                $sink_name, $t->getMessage()
            ));
        }
    }
}
