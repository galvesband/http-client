<?php

namespace Beat\HttpClient\Events;

use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FailedHttpRequestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public CommunicationException $exception;
    public RequestOptions $options;

    public function __construct(CommunicationException $exception, RequestOptions $options)
    {
        $this->exception = $exception;
        $this->options   = $options;
    }
}