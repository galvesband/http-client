<?php

namespace Beat\HttpClient\Events;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SuccessfulHttpRequestEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Response       $response;
    public RequestOptions $options;

    public function __construct(Response $response, RequestOptions $options)
    {
        $this->response = $response;
        $this->options  = $options;
    }
}
