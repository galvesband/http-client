<?php

namespace Beat\HttpClient\Testing;

use Closure;
use GuzzleHttp\Handler\MockHandler;
use Psr\Http\Message\RequestInterface;
use RuntimeException;

class GuzzleRequestMockHelperMiddleware
{
    protected ClientFactoryFake $fake_factory;
    protected string            $profile;
    protected MockHandler       $mockHandler;

    public function __construct(ClientFactoryFake $fakeFactory, string $profile, MockHandler $mockHandler)
    {
        $this->fake_factory = $fakeFactory;
        $this->profile      = $profile;
        $this->mockHandler  = $mockHandler;
    }

    public function __invoke(callable $handler): Closure
    {
        return function (RequestInterface $request, array $request_options) use ($handler) {

            // Limpiar el mock handler de cualquier expectativa o respuesta previamente salvada.
            $this->mockHandler->reset();

            if ($expectation = $this->fake_factory->getExpectationForRequest($this->profile, $request, $request_options)) {
                // Si hemos encontrado una expectativa que cuadra,
                // preparar la respuesta apropiada en el mock handler.
                $request_result = ($expectation->resultGenerator)($request, $request_options);
            } else {
                $request_result = new RuntimeException("Request did not match any expectations: \n" . json_encode([
                    'method' => $request->getMethod(),
                    'uri'    => $request->getUri(),
                ], JSON_PRETTY_PRINT));
            }

            // Y le decimos al handler que responda con el resultado.
            $this->mockHandler->append($request_result);

            return $handler($request, $request_options);

        };
    }
}