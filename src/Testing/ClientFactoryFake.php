<?php

namespace Beat\HttpClient\Testing;

use Beat\HttpClient\Client;
use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\GuzzleMiddleware;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Collection;
use Psr\Http\Message\RequestInterface;

class ClientFactoryFake extends ClientFactory
{
    protected Collection $expectations;

    public function __construct(ClientFactory $normalFactory)
    {
        parent::__construct($normalFactory->configuration, $normalFactory->sink_factory);
        $this->expectations = collect([]);
    }

    public function onProfile(string $profileName): RequestExpectationBuilder
    {
        return new RequestExpectationBuilder($profileName, $this);
    }

    public function addExpectation(RequestExpectation $expectation)
    {
        if (!$this->expectations->offsetExists($expectation->profile)) {
            $this->expectations->offsetSet($expectation->profile, collect([]));
        }

        $this->expectations[$expectation->profile]->push($expectation);
    }

    protected function build_client(string $profile_name): Client
    {
        // Si no tenemos expectativas para el perfil pedido, devolvemos un cliente normal.
        if (!$this->expectations->offsetExists($profile_name)) {
            return parent::build_client($profile_name);
        }

        $effective_configuration = $this->build_configuration_for_profile($profile_name);

        // Construir GuzzleClient con MockHandler sin respuestas esperadas.
        $mockHandler = new MockHandler();
        // Montamos la pila de middleware normales de Guzzle alrededor del MockHandler.
        $stack = HandlerStack::create($mockHandler);
        // Añadimos nuestro middleware estrella en el exterior de la cadena, como en un cliente BeatHttp normal.
        $stack->before('http_errors', new GuzzleMiddleware($effective_configuration), 'beat');
        // Como extra, añadimos un middleware en el lado interno de la cadena especial para testing.
        // Este middleware mirará entre las expectativas definidas una que encaje con la petición que se realiza,
        // y preparará o generará la respuesta apropiada en caso de que encuentre una expectativa adecuada.
        $stack->push(new GuzzleRequestMockHelperMiddleware($this, $profile_name, $mockHandler), 'beat_testing');

        // Construimos finalmente el cliente Guzzle.
        $internal_client = new GuzzleClient([
            'handler'  => $stack,
            'base_uri' => $effective_configuration->getBaseUri(),
        ]);

        return new Client($profile_name, $internal_client, $effective_configuration);
    }

    /**
     * Busca entre las expectativas definidas dentro de un perfil una que encaje con la petición que se está realizando.
     *
     * Si encuentra una expectativa, decrementa el número de veces que se espera dicha expectativa.
     *
     * @param string           $profileName
     * @param RequestInterface $request
     * @param array            $options
     * @return RequestExpectation|null
     */
    public function getExpectationForRequest(
        string           $profileName,
        RequestInterface $request,
        array            $options
    ): ?RequestExpectation
    {
        /** @var Collection $expectations */
        $expectations = $this->expectations->get($profileName, collect([]));

        $expectation = null;
        $expectations
            ->where('times', '>', 0)
            ->each(function (RequestExpectation $in_expectation) use ($options, $request, &$expectation) {
                if (($in_expectation->requestChecker)($request, $options)) {
                    // Ya hemos confirmado que una petición que esperábamos se está realizando.
                    // Decrementamos el número de veces que nos queda por recibirla.
                    $in_expectation->times -= 1;
                    // Nos quedamos con esta expectativa.
                    $expectation = $in_expectation;
                    // Y dejamos de buscar.
                    return false;
                }

                return true;
            });

        return $expectation;
    }

    /**
     * Revisa las expectativas que quedan. Si alguna queda pendiente por llamar, lanza excepción.
     *
     * @return void
     * @throws Exception
     */
    public function closeExpectations(): void
    {
        try {
            $this->expectations->each(function (Collection $expectations, string $profileName) {
                $expectations->each(function (RequestExpectation $expectation) use ($profileName) {
                    if ($expectation->times > 0) {
                        throw new Exception(sprintf(
                            "Quedan por realizar %d peticiones de al menos una expectativa en el perfil %s.",
                            $expectation->times, $profileName
                        ));
                    }

                    if ($expectation->times < 0) {
                        throw new Exception(sprintf(
                            "Se han realizado %d peticiones más de las esperadas en el perfil %s.",
                            abs($expectation->times), $profileName
                        ));
                    }
                });
            });
        } finally {
            $this->expectations = collect([]);
        }
    }
}