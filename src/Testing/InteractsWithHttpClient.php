<?php

namespace Beat\HttpClient\Testing;

use Beat\HttpClient\Facades\HttpClient;

trait InteractsWithHttpClient
{
    public function setUpInteractsWithHttpClient()
    {
        HttpClient::fake();

        $this->beforeApplicationDestroyed(function () {
            $this->checkPendingHttpExpectations();
        });
    }

    public function checkPendingHttpExpectations()
    {
        if (HttpClient::getFacadeRoot() instanceof ClientFactoryFake) {
            try {
                HttpClient::closeExpectations();
            } catch (\Throwable $error) {
                $this->fail(sprintf(
                    'Error closing Http expectations: (%s) \'%s\'.',
                    get_class($error), $error->getMessage()
                ));
            }
        }
    }
}