<?php

namespace Beat\HttpClient\Testing;

use Closure;

class RequestExpectation
{
    public string   $profile;
    public ?Closure $requestChecker;
    public int      $times;
    public ?Closure $resultGenerator;
    public float    $timeInSeconds;

    public function __construct(
        string  $profile,
        Closure $requestChecker,
        int     $times,
        Closure $resultGenerator,
        float   $timeInSeconds
    )
    {
        $this->profile         = $profile;
        $this->requestChecker  = $requestChecker;
        $this->times           = $times;
        $this->resultGenerator = $resultGenerator;
        $this->timeInSeconds   = $timeInSeconds;
    }
}