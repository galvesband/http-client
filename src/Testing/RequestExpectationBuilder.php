<?php

namespace Beat\HttpClient\Testing;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\GuzzleMiddleware;
use Closure;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use LogicException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

class RequestExpectationBuilder
{
    protected ClientFactoryFake $fake_factory;
    protected string            $profile;

    protected ?Closure          $requestChecker = null;
    protected int               $times = 1;
    protected ?Closure          $resultGenerator = null;
    protected float             $reportedTimeInSeconds = 0.0;

    public function __construct(string $profile, ClientFactoryFake $fakeFactory)
    {
        $this->profile      = $profile;
        $this->fake_factory = $fakeFactory;
    }

    public function expectRequest(Closure $requestChecker): RequestExpectationBuilder
    {
        $this->requestChecker = $requestChecker;
        return $this;
    }

    /**
     * Se espera que la petición se realice el número de veces dado.
     *
     * @param int $times
     * @return $this
     */
    public function times(int $times): RequestExpectationBuilder
    {
        if ($times <= 0) {
            throw new LogicException("Should be at least 1.");
        }

        $this->times = $times;
        return $this;
    }

    /**
     * Se espera que la petición se realice una única vez.
     *
     * @return $this
     */
    public function once(): RequestExpectationBuilder
    {
        return $this->times(1);
    }

    /**
     * Se espera que la petición se realice 2 veces.
     *
     * @return $this
     */
    public function twice(): RequestExpectationBuilder
    {
        return $this->times(2);
    }

    public function resultingInResponse(int $status, string $body, array $headers = []): RequestExpectationBuilder
    {
        return $this->setResultGenerator(function () use ($headers, $body, $status) {
            return new GuzzleResponse($status, $headers, $body);
        });
    }

    public function resultingInResponseInterface(ResponseInterface $response): RequestExpectationBuilder
    {
        return $this->setResultGenerator(function () use ($response) {
            return $response;
        });
    }

    public function resultingInError(Throwable $t): RequestExpectationBuilder
    {
        return $this->setResultGenerator(function () use ($t) {
            return $t;
        });
    }

    public function resultUsing(Closure $resultGenerator): RequestExpectationBuilder
    {
        return $this->setResultGenerator($resultGenerator);
    }

    protected function setResultGenerator(Closure $resultGenerator): RequestExpectationBuilder
    {
        $this->resultGenerator = $resultGenerator->bindTo(null);
        return $this;
    }

    public function __destruct()
    {
        $this->fake_factory->addExpectation(new RequestExpectation(
            $this->profile, $this->requestChecker, $this->times, $this->resultGenerator, $this->reportedTimeInSeconds
        ));
    }
}