<?php

namespace Beat\HttpClient\Sealers;

use Beat\HttpClient\Contracts\SealProvider;
use Carbon\Carbon;
use Psr\Http\Message\RequestInterface;

class BeatExternalTokenSealer implements SealProvider
{
    protected string $passkey;

    public function __construct(string $passkey)
    {
        $this->passkey = $passkey;
    }

    public function addAuthToRequest(RequestInterface $requestSpec): RequestInterface
    {
        return $requestSpec->withHeader('X-Auth', $this->build_token());
    }

    protected function build_token(?Carbon $date = null): string
    {
        // Usamos la función "gmdate()" para evitar problemas de zonas horarias.
        $strFecha = is_null($date) ? gmdate('Ymd') : $date->format('Ymd');

        $cadenaClaveFecha = $this->passkey . $strFecha;
        return hash('sha384', $cadenaClaveFecha);
    }
}