<?php

namespace Beat\HttpClient\Listeners;

use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\Events\FailedHttpRequestEvent;
use Beat\HttpClient\Events\SuccessfulHttpRequestEvent;

class ApplySinksToHttpRequestResultListener
{
    public function handleSuccessfulRequest(SuccessfulHttpRequestEvent $event): void
    {
        $event->response->getRequestOptions()->getSinks()->each(function (Sink $sink) use ($event) {
            $sink->processResult($event->response);
        });
    }

    public function handleFailedRequest(FailedHttpRequestEvent $event): void
    {
        $event->options->getSinks()->each(function (Sink $sink) use ($event) {
            $sink->processResult($event->exception);
        });
    }
}
