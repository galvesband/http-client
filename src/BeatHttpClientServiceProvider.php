<?php

namespace Beat\HttpClient;

use Beat\HttpClient\Commands\PruneRequests;
use Beat\HttpClient\Events\FailedHttpRequestEvent;
use Beat\HttpClient\Events\SuccessfulHttpRequestEvent;
use Beat\HttpClient\Factories\ClientFactory;
use Beat\HttpClient\Factories\SinkFactory;
use Beat\HttpClient\Listeners\ApplySinksToHttpRequestResultListener;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class BeatHttpClientServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        parent::register();

        // Registramos configuración del paquete...
        $this->mergeConfigFrom(
            __DIR__ . '/../config/beat_httpclient.php', 'beat_httpclient'
        );

        $this->app->bind(SinkFactory::class, function () {
            return new SinkFactory(config('beat_httpclient.sinks'));
        });

        $this->app->singleton(ClientFactory::class, function () {
            return new ClientFactory(config('beat_httpclient'), app(SinkFactory::class));
        });

        if (config('beat_httpclient.guzzle_requests_profile', false)) {
            $this->prepareGuzzleInterception(config('beat_httpclient.guzzle_requests_profile', 'default'));
        }

        $this->booting(function () {
            Event::listen(
                SuccessfulHttpRequestEvent::class,
                ApplySinksToHttpRequestResultListener::class . '@handleSuccessfulRequest'
            );

            Event::listen(
                FailedHttpRequestEvent::class,
                ApplySinksToHttpRequestResultListener::class . '@handleFailedRequest'
            );
        });
    }

    public function boot()
    {
        // En el caso de que nos ejecutemos en modo consola...
        if ($this->app->runningInConsole()) {
            // Publicamos los archivos del paquete (migraciones, configuración, ...).
            $this->configurePublishedFiles();

            // Registramos los comandos de consola del paquete.
            $this->commands([
                PruneRequests::class,
            ]);
        }
    }

    /**
     * Registra en Laravel los archivos publicables por el paquete.
     *
     * @return void
     */
    protected function configurePublishedFiles(): void
    {
        // Si el usuario aún no ha añadido la migración de la tabla de peticiones, la configuramos como publicable.
        if (! class_exists('CreateRequestsTable')) {
            $this->publishes([
                __DIR__ . '/../database/migrations/create_requests_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_requests_table.php'),
            ], 'migrations');
        }

        // Añadimos el archivo de configuración por defecto como publicable.
        $this->publishes([
            __DIR__ . '/../config/beat_httpclient.php' => config_path('beat_httpclient.php'),
        ], 'config');
    }

    protected function prepareGuzzleInterception(string $profile)
    {
        $this->app->bind(ClientInterface::class, \GuzzleHttp\Client::class);
        $this->app->bind(\GuzzleHttp\Client::class, function () use ($profile) {
            /** @var ClientFactory $client_factory */
            $client_factory = $this->app->get(ClientFactory::class);
            return $client_factory->buildGuzzleClient($profile);
        });
    }
}
