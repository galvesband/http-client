<?php

namespace Beat\HttpClient\ContentExtractors;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Psr\Http\Message\MessageInterface;

class SimpleContentExtractor implements MessageContentExtractor
{
    /** @var array Contenidos de Content-Types no incluidos aquí no serán logeados. */
    protected array $allowed_content_types;

    /** @var bool Si no hay cabecera Content-Type, ¿debe incluirse el contenido? */
    protected bool  $allow_empty_content_type;

    /** @var int Si el contenido de la petición supera este tamaño (en bytes), será truncado. 0 es ilimitado. */
    protected int   $size_threshold;

    public function __construct(
        array $allowedContentTypes,
        bool  $allowEmptyContentType,
        int   $sizeThreshold
    )
    {
        $this->allowed_content_types    = $allowedContentTypes;
        $this->allow_empty_content_type = $allowEmptyContentType;
        $this->size_threshold           = $sizeThreshold;
    }

    public function getAllowedContentTypes(): array
    {
        return $this->allowed_content_types;
    }

    public function setAllowedContentTypes(array $allowed_content_types): SimpleContentExtractor
    {
        $this->allowed_content_types = $allowed_content_types;
        return $this;
    }

    public function allowEmptyContentType(): bool
    {
        return $this->allow_empty_content_type;
    }

    public function setAllowEmptyContentType(bool $allow_empty_content_type): SimpleContentExtractor
    {
        $this->allow_empty_content_type = $allow_empty_content_type;
        return $this;
    }

    public function getSizeThreshold(): int
    {
        return $this->size_threshold;
    }

    public function setSizeThreshold(int $size_threshold): SimpleContentExtractor
    {
        $this->size_threshold = $size_threshold;
        return $this;
    }

    public function extractContent(MessageInterface $message): string
    {
        $content_type = NoContentExtractor::getContentTypeOfMessage($message);

        return $this->contentShouldBeExtracted($message, $content_type)
            ? $this->doExtractContentFromMessage($message)
            : NoContentExtractor::buildSummaryContentFromMessage($message, $content_type);
    }

    protected function contentShouldBeExtracted(MessageInterface $message, string $contentType): bool
    {
        // Si es alguno de los Content-Type permitidos, hay que extraer el contenido.
        $content_type = mb_strtolower($contentType);
        foreach ($this->allowed_content_types as $allowed_type) {
            if (strpos($content_type, mb_strtolower($allowed_type)) !== false) {
                return true;
            }
        }

        // Si no es una cabecera conocida porque el contenido está vacío y permitimos contenido vacío,
        // entonces hay que extraer el contenido. En otro caso, no.
        return (empty($content_type) && $this->allow_empty_content_type);
    }

    protected function doExtractContentFromMessage(MessageInterface $message): string
    {
        $size = $message->getBody()->getSize();
        $message->getBody()->rewind();
        return $this->size_threshold > 0 && $size > $this->size_threshold
            ? substr($message->getBody()->getContents(), 0, $this->size_threshold)
            : $message->getBody()->getContents();
    }
}
