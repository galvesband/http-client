<?php

namespace Beat\HttpClient\ContentExtractors;

use Beat\HttpClient\Contracts\MessageContentExtractor;
use Psr\Http\Message\MessageInterface;

class NoContentExtractor implements MessageContentExtractor
{
    public static function buildSummaryContentFromMessage(MessageInterface $message, string $contentType): string
    {
        $size = $message->getBody()->getSize();
        if ($size === 0) {
            return "";
        }

        return sprintf(
            '-- Skipped content of type \'%s\' weighting %s KiB --',
            $contentType, round($size / 1024, 3)
        );
    }

    public static function getContentTypeOfMessage(MessageInterface $message): string
    {
        return $message->getHeader('Content-Type')[0] ?? '';
    }

    public function extractContent(MessageInterface $message): string
    {
        return static::buildSummaryContentFromMessage(
            $message, static::getContentTypeOfMessage($message)
        );
    }
}