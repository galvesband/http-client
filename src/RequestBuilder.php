<?php

namespace Beat\HttpClient;

use BadMethodCallException;
use Beat\HttpClient\BuilderAddons\ExpectsJsonAddon;
use Beat\HttpClient\BuilderAddons\JsonDataAddon;
use Beat\HttpClient\Contracts\BuilderAddon;
use Beat\HttpClient\Contracts\Sink;
use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\ClientException;
use Beat\HttpClient\Exceptions\CommunicationException;
use Beat\HttpClient\Exceptions\ConnectionException;
use Beat\HttpClient\Exceptions\ServerException;
use Closure;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\RequestOptions as GuzzleRequestOptions;
use Illuminate\Support\Collection;

/**
 * Permite definir una petición HTTP externa a través de una API fluída a través de un cliente Beat/HttpClient.
 *
 * @see Client
 */
class RequestBuilder
{
    /** @var Client Cliente que creó este RequestBuilder. */
    protected Client $client;

    /** @var string Cadena corta que describe la operación o algo así. Informativo. */
    protected string $label;
    /** @var string Verbo HTTP de la petición a realizar. */
    protected string $method           = 'GET';
    /** @var string URI. Se combinará con 'base_uri' de las opciones del cliente. */
    protected string $uri              = '';
    /** @var array Cabeceras de la petición a realizar. */
    protected array  $headers          = [];
    /** @var int Segundos de timeout para la petición. */
    protected int    $timeout          = 20;
    /** @var int Segundos para el timeout de conexión de la petición. */
    protected int    $connect_timeout  = 10;
    /** @var string Valor de cabecera User-Agent. */
    protected string $user_agent       = 'Beat/HttpClient';
    /**
     * Sinks que procesarán la petición.
     *
     * @var Collection<Sink>
     * @see Sink
     */
    protected Collection $sinks;

    /**
     * Addons que modifican el comportamiento de la petición.
     *
     * @var Collection<BuilderAddon>
     * @see BuilderAddon
     */
    protected Collection $add_ons;

    /**
     * @param Client $client Instancia de Cliente Beat/HttpClient que creó este RequestBuilder.
     * @param string $label Cadena corta que describe la petición.
     */
    public function __construct(Client $client, string $label)
    {
        $this->client  = $client;
        $this->label   = $label;
        $this->add_ons = collect([]);
        $this->sinks   = collect([]);
    }

    /**
     * Establece el endpoint al que se hará la petición HTTP.
     *
     * El parámetro `$uri` se combinará con `base_uri` de las opciones del cliente. A continuación
     * unos cuantos ejemplos de cómo se comporta este parámetro junto con `base_uri` de las opciones
     * del cliente, directamente sacado de la documentación de
     * [GuzzleHttp](https://docs.guzzlephp.org/en/stable/quickstart.html).
     *
     * | `base_uri`             | `URI`             | Resultado                 |
     * | ---                    | ---               | ---                       |
     * | http://foo.com         | /bar              | http://foo.com/bar        |
     * | http://foo.com/foo     | /bar              | http://foo.com/bar        |
     * | http://foo.com/foo     | bar               | http://foo.com/bar        |
     * | http://foo.com/foo/    | bar               | http://foo.com/foo/bar    |
     * | http://foo.com	        | http://baz.com    | http://baz.com            |
     * | http://foo.com/?bar    | bar               | http://foo.com/bar        |
     *
     * @param string $method Verbo HTTP ('GET', 'POST', ...).
     * @param string $uri URI al que se hará la petición.
     * @return $this
     */
    public function to(string $method, string $uri): RequestBuilder
    {
        $this->method = $method;
        $this->uri = $uri;

        return $this;
    }

    /**
     * Establece el valor de la cabecera "User-Agent" de la petición.
     *
     * @param string $userAgent
     * @return $this
     */
    public function userAgent(string $userAgent): RequestBuilder
    {
        $this->user_agent = $userAgent;
        return $this;
    }

    /**
     * Añade una cabecera personalizada a la petición.
     *
     * @param string $headerName
     * @param string $headerValue
     * @param bool $replace ¿Debe reemplazar la cabecera existente, si existe?
     * @return $this
     */
    public function addHeader(string $headerName, string $headerValue, bool $replace = true): RequestBuilder
    {
        if (!array_key_exists($headerName, $this->headers) || $replace) {
            $this->headers[$headerName] = [];
        }

        $this->headers[$headerName][] = $headerValue;

        return $this;
    }

    /**
     * Establece los valores de timeout de la petición.
     *
     * @param int $connect_timeout Timeout para realizar la conexión inicial.
     * @param int $timeout Timeout de la petición.
     * @return $this
     */
    public function setTimeouts(int $connect_timeout = 10, int $timeout = 20): RequestBuilder
    {
        $this->connect_timeout = $connect_timeout;
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * Establece un array con los datos que deben ser incluidos en la petición en forma de JSON como cuerpo.
     *
     * Además de añadir el array de datos como JSON en el cuerpo, añadirá la cabecera
     * `Content-Type: application/json`.
     *
     * Internamente, llamar a este método es equivalente a añadir a la petición una instancia de `JsonDataAddon`
     * que añadirá a la petición algunos métodos específicos para manejar los datos JSON incluidos en la misma.
     *
     * AVISO: No se debe llamar a este método 2 veces, porque ello hará que se agreguen a la petición 2 instancias
     * de `JsonDataAddon`. Tras la primera llamada a `withJsonData()` es posible hacer llamadas sobre el objeto
     * RequestBuilder existentes en `JsonDataAddon` como `JsonDataAddon::setJsonData()`.
     *
     * @param array $data
     * @return $this
     * @see JsonDataAddon
     */
    public function withJsonData(array $data): RequestBuilder
    {
        $this->add_ons->push(new JsonDataAddon($this, $data));
        return $this;
    }

    /**
     * Indica que se espera una respuesta con cuerpo codificado como JSON y, opcionalmente, validación de los datos.
     *
     * Si la respuesta tiene un código de estado 2xx, será decodificada automáticamente (y se emitirá una excepción
     * InvalidJsonException).
     *
     * Internamente, llamar a este método es equivalente a añadir a la petición una instancia de `ExpectsJsonAddon`
     * que añadirá a la respuesta lógica para decodificar y, opcionalmente, validar los datos recibidos.
     *
     * AVISO: Llamar a este método varias veces añadirá la lógica de decodificación JSON varias veces, con sus
     * validaciones opcionales. Normalmente, no es lo que se pretende. Tras la primera llamada a `expecstsJson()`
     * es posible hacer llamadas sobre el objeto RequestBuilder existentes en `ExpectsJsonAddon` para añadir o
     * modificar la validación, por ejemplo.
     *
     * @param array|null $validationRules Reglas de validación a aplicar a los datos de la respuesta.
     * @param Closure|null $validatorCallback Callback que se pasará al Validator a través del método `after()` para hacer validación adicional.
     * @return $this
     * @see ExpectsJsonAddon
     */
    public function expectsJson(?array $validationRules = null, Closure $validatorCallback = null): RequestBuilder
    {
        $this->add_ons->push(new ExpectsJsonAddon($this, $validationRules, $validatorCallback));
        return $this;
    }

    /**
     * Agrega un BuilderAddon personalizado a la petición.
     *
     * @param BuilderAddon $item
     * @return $this
     */
    public function pushAddon(BuilderAddon $item): RequestBuilder
    {
        $this->add_ons->push($item);
        return $this;
    }

    /**
     * Añade un Sink personalizado a la petición.
     *
     * @param Sink $sink
     * @return $this
     */
    public function pushSink(Sink $sink): RequestBuilder
    {
        if (!$this->hasSink($sink->getName())) {
            $this->sinks->push($sink);
        }

        return $this;
    }

    /**
     * Añade una colección de Sinks personalizados a la petición.
     *
     * @param Collection $sinks
     * @return $this
     */
    public function pushSinks(Collection $sinks): RequestBuilder
    {
        $sinks->each(function (Sink $sink) {
            $this->pushSink($sink);
        });

        return $this;
    }

    /**
     * Elimina un Sink de la petición por nombre.
     *
     * @param string $sinkName
     * @return $this
     */
    public function withoutSink(string $sinkName): RequestBuilder
    {
        $this->sinks = $this->sinks
            ->filter(function (Sink $sink) use ($sinkName) {
                return $sink->getName() !== $sinkName;
            });

        return $this;
    }

    /**
     * Ejecuta la petición de forma asíncrona.
     *
     * @return PromiseInterface
     */
    public function executeAsync(): PromiseInterface
    {
        return $this->transfer($this->build_guzzle_options());
    }

    /**
     * Ejecuta la petición de forma síncrona. Si se produce un error, lanzará una excepción.
     *
     * Dependiendo de los addons incluidos en la petición, es posible que surjan peticiones
     * relativas a los datos devueltos aunque la petición haya sido exitosa.
     *
     * @return Response
     * @throws CommunicationException
     * @throws ClientException
     * @throws ServerException
     * @throws ConnectionException
     * @noinspection PhpDocRedundantThrowsInspection
     */
    public function execute(): Response
    {
        $options = $this->build_guzzle_options([GuzzleRequestOptions::SYNCHRONOUS => true]);
        return $this->transfer($options)->wait();
    }

    protected function transfer(array $options): PromiseInterface
    {
        return $this->client->getInternalClient()->requestAsync(
            $this->method, $this->uri, $options
        );
    }

    public function __call($name, $arguments)
    {
        foreach ($this->add_ons as $add_on) {
            /** @var BuilderAddon $add_on */
            if (method_exists($add_on, $name)) {
                return $add_on->$name(...$arguments);
            }
        }

        throw new BadMethodCallException(sprintf(
            "Método %s no encontrado en %s ni en ninguno de sus add-ons.",
            $name, get_class($this)
        ));
    }

    protected function build_guzzle_options(array $extra = []): array
    {
        $this->addHeader('User-Agent', $this->user_agent);

        $options = [
            'beat'                                => $this->build_request_options(),
            GuzzleRequestOptions::HEADERS         => $this->headers,
            GuzzleRequestOptions::CONNECT_TIMEOUT => $this->connect_timeout,
            GuzzleRequestOptions::TIMEOUT         => $this->timeout,
        ];

        // Aplicar add-ons a opciones
        foreach ($this->add_ons as $addon) {
            /** @var BuilderAddon $addon */
            $addon->buildOptions($options);
        }

        return array_merge($options, $extra);
    }

    /**
     * Construye un objeto con las opciones que espera GuzzleMiddleware para hacer su trabajo.
     *
     * @return RequestOptions
     * @see GuzzleMiddleware::get_beat_options_from_request_options()
     */
    protected function build_request_options(): RequestOptions
    {
        return (new RequestOptions())
            ->setLabel($this->label)
            ->withBuilderAddons($this->add_ons)
            ->withSinks($this->sinks);
    }

    /* ******* */
    /* GETTERS */
    /* ******* */

    /**
     * Devuelve la etiqueta o cadena descriptiva corta de la petición
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Devuelve el verbo HTTP de la petición.
     *
     * @return string
     * @see self::to()
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Devuelve la URI de la petición.
     *
     * @return string
     * @see self::to()
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Devuelve el array de cabeceras actual de la petición.
     *
     * @return array
     * @see self::addHeader()
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Devuelve el timeout de conexión (en segundos) que se usará en la petición.
     *
     * @return int
     * @see self::setTimeouts()
     */
    public function getConnectTimeout(): int
    {
        return $this->connect_timeout;
    }

    /**
     * Devuelve el timeout (en segundos) que se usará en la petición.
     *
     * @return int
     * @see self::setTimeouts()
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * Devuelve el valor de la cabecera `User-Agent` que se usará en la petición.
     *
     * @return string
     * @see self::userAgent()
     */
    public function getUserAgent(): string
    {
        return $this->user_agent;
    }

    /**
     * Devuelve la colección de addons actualmente en la petición.
     *
     * @return Collection
     */
    public function getAddons(): Collection
    {
        return $this->add_ons;
    }

    /**
     * Devuelve la colección de sinks que procesarán la petición y respuesta.
     *
     * @return Collection
     */
    public function getSinks(): Collection
    {
        return $this->sinks;
    }

    /**
     * Devuelve true o false si existe en la petición un sink con el nombre indicado.
     *
     * @param string $sinkName
     * @return bool
     */
    public function hasSink(string $sinkName): bool
    {
        return $this->sinks->filter(function (Sink $sink) use ($sinkName) {
            return $sink->getName() === $sinkName;
        })->isNotEmpty();
    }
}
