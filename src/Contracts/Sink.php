<?php

namespace Beat\HttpClient\Contracts;

use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\Exceptions\CommunicationException;

interface Sink
{
    /**
     * Construye una instancia del Sink.
     *
     * @param string                  $name
     * @param array                   $parameters
     * @param bool                    $registerSuccessfulRequest
     * @param bool                    $registerFailedRequest
     * @param MessageContentExtractor $requestExtractor
     * @param MessageContentExtractor $responseExtractor
     * @return Sink
     */
    public static function fromConfiguration(
        string                  $name,
        array                   $parameters,
        bool                    $registerSuccessfulRequest,
        bool                    $registerFailedRequest,
        MessageContentExtractor $requestExtractor,
        MessageContentExtractor $responseExtractor
    ): Sink;

    /**
     * Devuelve el nombre que identifica al Sink en la configuración.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Devuelve true si este Sink registra peticiones con éxito (estado < 400 y post-procesado sin errores).
     *
     * @return bool
     */
    public function registersSuccessfulRequests(): bool;

    /**
     * Devuelve true si este Sink registra peticiones fallidas (error de conexión, estado >= 400 o error en post-procesado).
     *
     * @return bool
     */
    public function registersFailedRequests(): bool;

    /**
     * Devuelve el extractor de contenido de las peticiones que utiliza el sink.
     *
     * @return MessageContentExtractor
     */
    public function getRequestContentExtractor(): MessageContentExtractor;

    /**
     * Devuelve el extractor de contenido de las respuestas que utiliza el sink.
     *
     * @return MessageContentExtractor
     */
    public function getResponseContentExtractor(): MessageContentExtractor;

    /**
     * Procesa el resultado de una comunicación (respuesta o error de comunicación).
     *
     * @param Response|CommunicationException $requestResult
     * @return void
     */
    public function processResult($requestResult): void;
}
