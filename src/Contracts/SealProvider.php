<?php

namespace Beat\HttpClient\Contracts;

use Psr\Http\Message\RequestInterface;

interface SealProvider
{
    public function addAuthToRequest(RequestInterface $requestSpec): RequestInterface;
}