<?php

namespace Beat\HttpClient\Contracts;

use Beat\HttpClient\DataTransferObjects\Response;
use Beat\HttpClient\RequestBuilder;

interface BuilderAddon
{
    public function getBuilder(): RequestBuilder;
    public function buildOptions(array &$options): void;
    public function applyToResponse(Response $response): void;
}