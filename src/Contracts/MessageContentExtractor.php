<?php

namespace Beat\HttpClient\Contracts;

use Psr\Http\Message\MessageInterface;

interface MessageContentExtractor
{
    public function extractContent(MessageInterface $message): string;
}