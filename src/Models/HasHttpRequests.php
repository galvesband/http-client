<?php

namespace Beat\HttpClient\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @property Collection|Request[] $requests
 */
trait HasHttpRequests
{
    public function requests(): MorphMany
    {
        return $this->morphMany(Request::class, 'authenticatable');
    }
}