<?php

namespace Beat\HttpClient\Models;

use ArrayObject;
use Beat\HttpClient\Database\Factories\RequestFactory;
use Beat\HttpClient\Jobs\SaveHttpRequestResultInDatabaseJob;
use Beat\HttpClient\Sinks\DatabaseSink;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Representa una petición HTTP y su respuesta registrada en base de datos por DatabaseSink.
 *
 * @see DatabaseSink
 *
 * @property int         $id
 * @property int|null    $authenticatable_id
 * @property string|null $authenticatable_type
 * @property Model|null  $authenticatable
 * @property string      $label
 * @property Carbon      $timestamp
 * @property string      $method
 * @property string      $scheme
 * @property string      $host
 * @property int         $port
 * @property string      $path
 * @property string      $parameters
 * @property ArrayObject $headers
 * @property int         $response_code
 * @property string      $response
 * @property ArrayObject $response_headers
 * @property float       $duration
 *
 * // Atributos virtuales
 * @property string      $url
 */
class Request extends Model
{
    use HasFactory;

    public static function newFactory(): RequestFactory
    {
        return RequestFactory::new();
    }

    public static function fromJobData(SaveHttpRequestResultInDatabaseJob $job): Request
    {
        $uri = new Uri($job->requestData->getUrl());
        $port = $uri->getPort();
        if (!$port) {
            switch ($uri->getScheme()) {
                case 'http':
                    $port = 80;
                    break;

                case 'https':
                    $port = 443;
                    break;

                default:
                    $port = 0;
            }
        }

        return new static([
            'authenticatable_id'   => $job->authenticatable_id,
            'authenticatable_type' => $job->authenticatable_class,
            'label'                => substr($job->label, 0, min(strlen($job->label), 255)),
            'timestamp'            => $job->timestamp,
            'method'               => $job->requestData->getMethod(),
            'scheme'               => $uri->getScheme(),
            'host'                 => $uri->getHost(),
            'port'                 => $port,
            'path'                 => $uri->getPath(),
            'parameters'           => $job->requestData->getContent(),
            'headers'              => $job->requestData->getHeaders(),
            'response_code'        => $job->responseData->getStatus(),
            'response'             => $job->responseData->getContent(),
            'response_headers'     => $job->responseData->getHeaders(),
            'duration'             => round($job->responseData->getTimeInSeconds(), 2),
        ]);
    }

    public $primaryKey = 'id';
    protected $table = 'requests';

    public $timestamps = false;

    protected $fillable = [
        'authenticatable_id', 'authenticatable_type', 'label', 'timestamp',
        'method', 'scheme', 'host', 'port', 'path', 'parameters', 'headers',
        'response_code', 'response', 'response_headers', 'duration',
    ];

    protected $casts = [
        'authenticatable_id' => 'integer',
        'timestamp'          => 'datetime:Y-m-d H:i:s',
        'headers'            => AsArrayObject::class,
        'response_code'      => 'integer',
        'response_headers'   => AsArrayObject::class,
        'duration'           => 'float:3',
    ];

    protected $attributes = [
        'authenticatable_id'   => null,
        'authenticatable_type' => null,
        'headers'              => '{}',
        'response'             => '',
        'response_headers'     => '{}',
        'duration'             => 0.0,
    ];

    public function authenticatable(): MorphTo
    {
        return $this->morphTo();
    }

    /** @noinspection PhpUnused */
    public function getUrlAttribute(): string
    {
        return sprintf(
            '%s://%s:%s%s',
            $this->scheme, $this->host, $this->port, $this->path
        );
    }
}