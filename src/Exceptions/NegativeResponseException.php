<?php

namespace Beat\HttpClient\Exceptions;

/**
 * La respuesta es un 4xx o un 5xx.
 */
class NegativeResponseException extends RequestException
{

}