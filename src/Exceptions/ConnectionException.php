<?php

namespace Beat\HttpClient\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Psr\Http\Message\RequestInterface;
use Throwable;

class ConnectionException extends CommunicationException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Error de conexión con sistema externo. Inténtelo más tarde o consulte con un administrador.';

    public function __construct(
        RequestInterface $request,
        RequestOptions $beatOptions,
        float $time_in_seconds,
        ?Throwable $previous,
        ?string $public_message = null,
        int $code = 504,
        ?string $message = null
    ) {
        parent::__construct(
            $request,
            $beatOptions,
            $time_in_seconds,
            $previous,
            $public_message,
            $code,
            $message
        );
    }


}