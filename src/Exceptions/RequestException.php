<?php

namespace Beat\HttpClient\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Tenemos una respuesta. Y es un error. Pero no indicamos nada más con esta excepción.
 */
class RequestException extends CommunicationException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Una petición a un sistema externo no ha podido ser completada. Inténtelo más tarde o consulte con un administrador.';

    protected ResponseInterface $response;

    public function __construct(
        RequestInterface  $request,
        ResponseInterface $response,
        ?Throwable        $previous,
        RequestOptions    $beatOptions,
        float             $elapsed_time_in_seconds,
        ?string           $public_message = null,
        int               $code = 502,
        ?string           $message = null
    ) {
        parent::__construct(
            $request,
            $beatOptions,
            $elapsed_time_in_seconds,
            $previous,
            $public_message,
            $code,
            $message
        );

        $this->response = $response;
    }

    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    public function context(): array
    {
        return array_merge(parent::context(), [
            'response' => $this->response_to_context(),
        ]);
    }

    protected function response_to_context(): array
    {
        $retVal = [
            'status' => $this->response->getStatusCode(),
        ];

        if (!empty($this->response->getHeaders())) {
            $retVal['headers'] = $this->response->getHeaders();
        }

        if (!empty($body = (string) $this->response->getBody())) {
            $retVal['body'] = $body;
        }

        return $retVal;
    }
}