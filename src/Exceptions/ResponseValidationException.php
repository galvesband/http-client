<?php

namespace Beat\HttpClient\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * La respuesta no ha pasado las reglas de validación.
 */
class ResponseValidationException extends RequestException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'La respuesta de un servicio externo no es válida. Consulte con un administrador.';

    // TODO Almacenar datos también!
    // TODO Quizás validador completo?

    protected array $validation_errors;

    public function __construct(
        RequestInterface  $request,
        ResponseInterface $response,
        RequestOptions    $options,
        array             $validationErrors,
        float             $timeInSeconds,
        ?string           $public_message = null,
        int               $code = 502,
        ?string           $message = null
    ) {
        parent::__construct(
            $request,
            $response,
            null,
            $options,
            $timeInSeconds,
            $public_message,
            $code,
            $message
        );

        $this->validation_errors = $validationErrors;
    }

    public function getValidationErrors(): array
    {
        return $this->validation_errors;
    }

    public function context(): array
    {
        return array_merge(parent::context(), [
            'validation_errors' => $this->validation_errors,
        ]);
    }
}