<?php

namespace Beat\HttpClient\Exceptions;

class ClientException extends NegativeResponseException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Un servicio remoto ha rechazado nuestra petición. Inténtelo más tarde o consulte con un administrador.';

    public function isIdentificationError(): bool
    {
        return $this->getStatusCode() === 401;
    }

    public function isAuthorizationError(): bool
    {
        return $this->getStatusCode() === 403;
    }

    public function isNotFound(): bool
    {
        return $this->getStatusCode() === 404;
    }

    public function isValidationError(): bool
    {
        return $this->getStatusCode() === 422;
    }

    public function isTooManyRequests(): bool
    {
        return $this->getStatusCode() === 429;
    }
}