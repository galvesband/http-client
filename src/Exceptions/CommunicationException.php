<?php

namespace Beat\HttpClient\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Exception;
use Illuminate\Http\JsonResponse;
use Psr\Http\Message\RequestInterface;
use Throwable;

class CommunicationException extends Exception
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Error de comunicación con sistema remoto. Vuelva a intentarlo más tarde o consulte con un administrador.';

    protected RequestOptions   $beat_options;
    protected RequestInterface $request;
    protected float            $time_in_seconds;
    protected string           $public_message;

    public function __construct(
        RequestInterface $request,
        RequestOptions   $beatOptions,
        float            $time_in_seconds,
        ?Throwable       $previous,
        ?string          $public_message = null,
        int              $code           = 502,
        ?string          $message        = null
    ) {
        parent::__construct(
            sprintf(
                "Error de comunicación durante operación '%s': '%s'.",
                $beatOptions->getLabel(),
                $message ?? optional($previous)->getMessage() ?? 'Error desconocido'
            ),
            $code,
            $previous
        );

        $this->beat_options    = $beatOptions;
        $this->request         = $request;
        $this->public_message  = $public_message ?? static::DEFAULT_PUBLIC_MESSAGE;
        $this->time_in_seconds = $time_in_seconds;
    }

    public function getRequestOptions(): RequestOptions
    {
        return $this->beat_options;
    }

    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    public function getTimeInSeconds(): float
    {
        return $this->time_in_seconds;
    }

    public function getPublicMessage(): string
    {
        return $this->public_message;
    }

    public function context(): array
    {
        return [
            'exception_type' => basename(str_replace('\\', '/', get_class($this))),
            'code'           => $this->getCode(),
            'request'        => $this->request_to_log_context(),
            'time'           => $this->time_in_seconds . "s",
        ];
    }

    protected function request_to_log_context(): array
    {
        $retVal = [
            'endpoint' => sprintf("%s %s", $this->request->getMethod(), $this->request->getUri()),
        ];

        if (!empty($this->request->getHeaders())) {
            $retVal['headers'] = $this->request->getHeaders();
        }

        if (!empty($body = (string) $this->request->getBody())) {
            $retVal['body'] = $body;
        }

        return $retVal;
    }

    public function render(): JsonResponse
    {
        $public_message = $this->public_message;

        return response()->json([
            'message' => $public_message,
        ], $this->getCode());
    }
}
