<?php

namespace Beat\HttpClient\Exceptions;

class ServerException extends NegativeResponseException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Un servicio remoto ha sufrido un error. Inténtelo más tarde o consulte con un administrador.';

    public function isGatewayTimeout(): bool
    {
        return $this->getStatusCode() === 504;
    }

    public function isTemporaryOutOfService(): bool
    {
        return $this->getStatusCode() === 503;
    }

    public function isBadGateway(): bool
    {
        return $this->getStatusCode() === 502;
    }

    public function isGenericServerError(): bool
    {
        return $this->getStatusCode() === 500;
    }
}