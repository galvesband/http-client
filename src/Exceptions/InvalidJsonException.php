<?php

namespace Beat\HttpClient\Exceptions;

use Beat\HttpClient\DataTransferObjects\RequestOptions;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * La respuesta ha sido correcta, pero el contenido, que debía ser un JSON, no ha podido ser decodificado.
 */
class InvalidJsonException extends RequestException
{
    protected const DEFAULT_PUBLIC_MESSAGE = 'Un sistema externo ha devuelto una respuesta inválida. Consulte con un administrador.';

    public function __construct(
        RequestInterface  $request,
        ResponseInterface $response,
        ?Throwable        $previous,
        RequestOptions    $options,
        float             $elapsed_time_in_seconds,
        ?string           $public_message          = null,
        int               $code                    = 502,
        ?string           $message                 = null
    ) {
        parent::__construct(
            $request,
            $response,
            $previous,
            $options,
            $elapsed_time_in_seconds,
            $public_message,
            $code,
            $message
        );
    }
}